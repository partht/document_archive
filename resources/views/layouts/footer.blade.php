 <footer class="footer footer-static footer-light navbar-border">
      <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="#" class="text-bold-800 grey darken-2">Zignuts</a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block hidden">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var base_url='{{ url('/') }}';
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            $(".loader-wrapper").fadeOut("slow");
        });
    </script>
    <script src="{{ url('/') }}/app-assets/vendors/js/extensions/dragula.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/forms/listbox/jquery.bootstrap-duallistbox.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    
    <script src="{{ url('/') }}/app-assets/vendors/js/pickers/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>

    <script src="{{ url('/') }}/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- <script src="{{ url('/') }}/app-assets/vendors/js/buttons/spin.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/buttons/ladda.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/js/scripts/buttons/button-ladda.js" type="text/javascript"></script> -->
    @include('layouts.scripts')

    @stack('scripts')
    
  </body>
</html>