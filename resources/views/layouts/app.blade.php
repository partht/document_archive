<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

  @include('layouts.header')
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar menu-collapsed" >
    
    
    <!-- navbar-fixed-top-->
    @include('layouts.navbar')
   

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    
    @include('layouts.menu')

    <!-- / main menu-->


    @yield('contentwithsidebar')

    
    <div class="app-content content container-fluid">
        <div class="loader-wrapper" id="loader-wrapper">
        <div class="loader-container">
            <div class="folding-cube loader-blue-grey">
                <div class="cube1 cube"></div>
                <div class="cube2 cube"></div>
                <div class="cube4 cube"></div>
                <div class="cube3 cube"></div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
    <div class="row">
    <div class="col-xs-12">
     @yield('content')
    </div>
    </div>
    </div>
    </div>
    </div>


    <!-- ////////////////////////////////////////////////////////////////////////////-->


   @include('layouts.footer')
