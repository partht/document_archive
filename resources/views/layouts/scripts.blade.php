<script>
//Write Custom scripts here
$(document).ready(function(){
	//Menu active
	$("ul#main-menu-navigation a").click(function(e) 
	{
		$("ul#main-menu-navigation a").removeClass('active');
		var link = $(this);
		var item = link.parent("li");
		if (item.hasClass("active")) 
		{
			item.removeClass("active").children("a").removeClass("active");
		} 
		else 
		{
			item.addClass("active").children("a").addClass("active");
		}
	}).each(function() 
	{
		var link = $(this);
		if (link.get(0).href === location.href) 
		{
			link.addClass("active").parents("li").addClass("active");
			return false;
		}
	});

	//Menu '+' signs
	$('li.nav-item span#add-category').on('click',function()
	{
		window.location.href = "{{route('category.add')}}";
		return false;
	});

	$('li.nav-item span#add-keyword').on('click',function()
	{
		window.location.href = "{{route('keyword.add')}}";
		return false;
	});

	$('li.nav-item span#add-document').on('click',function()
	{
		window.location.href = "{{route('document.add')}}";
		return false;
	});

	/*all index pages checkboxes*/
	$('input.input-chk').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
	});

	$(document).on('click',"li.paginate_button",function(){
		$('input.input-chk').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
		});
	});
});
</script>