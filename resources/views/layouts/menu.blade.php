<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content" id="navbar1">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="nav-item">
            <a href="{{ route('home') }}">
              <i class="icon-dashboard" id="menu_main_setting"></i>
              <span data-i18n="nav.dash.main" class="menu-title ">@lang('label.dashboard')</span>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="{{route('category.index')}}">
              <i class="icon-tag"></i>
              <span class="menu-title">@lang('label.categories')</span>
              <!--<span class="float-xs-right" id="add-category"><i class="icon-plus"></i></span>-->
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('keyword.index')}}">
              <i class="icon-key3"></i>
              <span class="menu-title">@lang('label.keyword')</span>
              <!--<span class="float-xs-right" id="add-keyword"><i class="icon-plus"></i></span>-->
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('document.index')}}">
              <i class="icon-document-text"></i>
              <span class="menu-title">@lang('label.document')</span>
              <!-- <span class="float-xs-right" id="add-document"><i class="icon-plus"></i></span> -->
            </a>
          </li>

        @if(checkPermission(['admin']))
          <li class="nav-item">
            <a href="{{route('user.index')}}">
              <i class="icon-users2"></i>
              <span class="menu-title">@lang('label.users')</span>
              <!--<span class="float-xs-right" id="add-user"><i class="icon-plus"></i></span>-->
            </a>
          </li>
        @endif  
        <li class="nav-item">
          <a href="{{url('/')}}/admin/reports">
            <i class="icon-file-text"></i>
            <span class="menu-title">@lang('label.audit_report')</span>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('page.index')}}">
            <i class="icon-copy2"></i>
            <span class="menu-title">@lang('label.page')</span>
          </a>
        </li>
        @if(checkPermission(['admin']))
          <li class="nav-item">
            <a href="{{route('timerange.index')}}">
              <i class="icon-clock"></i>
              <span class="menu-title">@lang('label.time_range')</span>
            </a>
          </li>
        @endif  
<!--           <li class="nav-item">
            <a href="{{url('/')}}/admin/sample-form">
              <i class="icon-document-text"></i>
              <span class="menu-title">Simple CRUD</span>
              <span class="float-xs-right" id="add-document"><i class="icon-plus"></i></span>
            </a>
          </li> -->
          

          {{-- <li class=" nav-item"><a href="#"><i class="icon-gift3" id="menu_main_setting"></i><span data-i18n="nav.navbars.main" class="menu-title">Level 0</span></a>
            <ul class="menu-content">
              <li><a href="{{url('/')}}/admin/module" data-i18n="nav.navbars.nav_light" class="menu-item selected">Module Master<span class="float-xs-right add_menu"><i class="icon-plus"></i></span></a>
              </li>
              <li><a href="{{url('/')}}/admin/device" data-i18n="nav.navbars.nav_dark" class="menu-item">Device master<span class="float-xs-right add_slider"><i class="icon-plus"></i></span></a>
              </li>
              
            </ul>
          </li> --}}
          
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <div class="main-menu-footer footer-close">
        <div class="header text-xs-center"><a href="#" class="col-xs-12 footer-toggle"><i class="icon-ios-arrow-up"></i></a></div>
        <div class="content">
          <div class="insights">
            <div class="col-xs-12">
              <p>Product Delivery</p>
              <progress value="25" max="100" class="progress progress-xs progress-success">25%</progress>
            </div>
            <div class="col-xs-12">
              <p>Targeted Sales</p>
              <progress value="70" max="100" class="progress progress-xs progress-info">70%</progress>
            </div>
          </div>
          <div class="actions"><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Settings"><span aria-hidden="true" class="icon-cog3"></span></a><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock"><span aria-hidden="true" class="icon-lock4"></span></a><a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout"><span aria-hidden="true" class="icon-power3"></span></a></div>
        </div>
      </div>
      <!-- main menu footer-->
    </div>
