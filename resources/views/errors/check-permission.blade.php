
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    
    <title>Error 401 - EZYFX4U</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{url('/')}}/app-assets/images/ico/apple-icon-60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('/')}}/app-assets/images/ico/apple-icon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('/')}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('/')}}/app-assets/images/ico/apple-icon-152.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="{{url('/')}}/app-assets/images/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap.css">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/sliders/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/pace.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/colors.css">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-multi-level-menu.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/pages/error.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="hover" data-menu="vertical-mmenu" data-col="1-column" class="vertical-layout vertical-mmenu 1-column  blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
    <div class="card-header no-border bg-transparent p-0">
        <h2 class="error-code text-xs-center mb-2">401</h2>
        <h3 class="text-uppercase text-xs-center">Error 401 - Unauthorized Access.</h3>
    </div>
    
    <div class="card-body collapse in">
        <fieldset class="row py-2">
            <div class="input-group col-xs-12">
                <input type="text" class="form-control form-control-lg input-lg  border-grey border-lighten-1" placeholder="Search..." aria-describedby="button-addon2">
                <span class="input-group-btn" id="button-addon2">
					<button class="btn btn-lg btn-secondary  border-grey border-lighten-1" type="button"><i class="icon-ios-search-strong"></i></button>
				</span>
            </div>
        </fieldset>
        <div class="row py-2">
            <div class="col-xs-6 offset-xs-3 text-xs-center">
                <a href="{{url('/')}}/admin/home" class="btn btn-primary btn-block font-small-3"><i class="icon-home3"></i> Back to Home</a>
            </div>

            </div>
        </div>
        <div class="card-footer bg-transparent pb-0">
            <div class="row">
                <p class="text-muted text-xs-center col-xs-12 py-1">© 2016 <a href="#">EZYFX4U</a>Crafted with <i class="icon-heart5 pink"> </i> By <a href="http://www.zignuts.com" target="_blank">ZIGNUTS </a> </p>
                <div class="text-xs-center">
                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="icon-facebook3"></span></a>
                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="icon-twitter3"></span></a>
                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="icon-linkedin3 font-medium-4"></span></a>
                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span class="icon-github font-medium-4"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{url('/')}}/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{url('/')}}/app-assets/vendors/js/menu/jquery.mmenu.all.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{url('/')}}/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{url('/')}}/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  </body>
</html>
