@extends('layouts.app')

@section('title'){{config('app.APP_NAME')." | Profile"}} @endsection

@section('content')
@if(Session::has('success_msg'))
<div class="alert alert-success">{{ Session::get('success_msg') }}</div>
@endif

<div class="card">
     <div class="card-header">
        <h4 class="card-title">Profil aktualisieren</h4>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
    </div>
    <div class="card-body">
        <div class="card-block">
           
            <div class="">
                <form class="form form-horizontal"  action="{{route('update_profile')}}" id="device_form" method="post" enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label class="col-md-2 label2-control required" for="projectinput1"> Name : </label>
                            <div class="col-md-6">
                            <input type="text" data-toggle="tolltip" maxlength="15" title="Name" placeholder="Name" required class="form-control" name="name" required autofocus value="{{Auth()->user()->name}}">
                            </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-2 label2-control required" for="projectinput1"> E-Mail Address :&nbsp; </label>
                            <div class="col-md-6">
                            <input type="text" data-toggle="tolltip" title="Email Address" placeholder="Email Address" required class="form-control" name="email"  value="{{Auth()->user()->email}}" disabled>
                            </div>
                    </div>
                {{--@if(Auth()->user()->user_type == 'HO')
                    <div class="form-group row">
                        <label class="col-md-2 label2-control required"> Phone Number :&nbsp; </label>
                        <div class="col-md-6">
                           <input type="phone" data-toggle="tolltip" title="Phone Number" placeholder="Phone Number" required class="form-control" name="phone_number"  value="{{Auth()->user()->phone_number}}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 label2-control required"> Address :&nbsp; </label>
                        <div class="col-md-6">
                           <textarea data-toggle="tolltip" name="address" class="form-control" title="address" placeholder="Address">{{Auth()->user()->address}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label2-control required"> Country :&nbsp; </label>
                        <div class="col-md-6">
                            <select class="form-control"  value="{{ old('country') }}" id="country" name="country">
                                <option value="">Select</option>
                                @foreach($countries as $country)
                                <option value="{{$country->id}}" @if($country->id == Auth()->user()->country) {{'selected'}} @endif>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 label2-control required"> State :&nbsp; </label>
                        <div class="col-md-6">
                            <div class="input-group">
                            <input type="text" required="" name="state" value="{{Auth()->user()->state_find->name}}" class="form-control state ui-autocomplete-input" id="state" autocomplete="off" required="">
                            <span class="input-group-addon"><i class="icon-angle-down"></i></span>
                            <input type="hidden" id="stateid" value="{{Auth()->user()->state}}">
                            
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group row">
                        <label class="col-md-2 label2-control required"> City :&nbsp; </label>
                        <div class="col-md-6">
                            <div class="input-group">
                            <input type="text" value="{{Auth()->user()->city_find->name}}" name="city" id="city"  class="form-control city ui-autocomplete-input" autocomplete="off" required="">
                            <span class="input-group-addon"><i class="icon-angle-down"></i></span>
                            </div>
                        </div>
                    </div>  
                @endif --}}
                    
                    {{-- <div class="form-group row">
                    <label class="col-md-2 label2-control required" for="projectinput1"> Confirm Password</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>
                        </div>
                    </div> --}}

                            
                    <!-- <div class="row">
                        <div class="col-md-6 ">
                            <a class="btn btn-theme-darkblue" href="{{ route('home') }}"><i class="icon-android-arrow-back"></i> @lang('general.back')</a>
                            <button type="submit" id="check_validation" class="btn btn-theme-orange"><i class="icon-check2"></i> @lang('general.save')</button>
                        </div>
                    </div> -->
                    <div class="form-actions">
                        <a class="btn btn-theme-darkblue" href="{{ route('home') }}"><i class="icon-android-arrow-back"></i> @lang('general.back')</a>
                        <button type="submit" id="check_validation" class="btn btn-theme-orange"><i class="icon-check2"></i> @lang('general.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">

    /*var countryID = $('#country option:selected').val();  
    $('#country').on('change',function () 
    {
        countryID = $(this).val();
        $("#state").autocomplete( "option", "source", base_url+'get-state-list/'+countryID );
    });


    $("#state")
    .bind('focus', function()
    { 
        countryID = $('#country option:selected').val(); 
        $(this).autocomplete("search");
    })
    .autocomplete({
       source: base_url+'get-state-list',
       minLength: 0,
       select: function( event, ui )
       {
            console.log(ui.item.id);
            $("#city").autocomplete( "option", "source", base_url+'get-city-list/'+ui.item.id );
            //$("#stateid").val(ui.item.id);
       }
  });
    
    $("#state").autocomplete( "option", "source", base_url+'get-state-list/'+countryID );

    $("#city")
    .bind('focus', function()
    { 
        countryID = $('#state option:selected').val(); 
        $(this).autocomplete("search");
    })
    .autocomplete({
       source: base_url+'get-city-list',
       minLength: 0,
       select: function( event, ui )
       {
          console.log(ui.item.id);
           // $("#cityid").val(ui.item.id);

       }
  });

    var stateid = $('input#stateid').val();
    $("#city").autocomplete( "option", "source", base_url+'get-city-list/'+stateid );*/
    
</script>
@endpush