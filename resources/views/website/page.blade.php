<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<meta name="keywords" content="key, words">
		<meta name="description" content="description">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>ArchiFOX</title>

		<link href="{{ url('/') }}/web/css/style.css" rel="stylesheet" type="text/css">		
		<link href="{{ url('/') }}/web/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" media="all" type="text/css" >		
		<link href="{{ url('/') }}/favicon.ico" rel="icon" type="image/x-icon">

	</head>

	<body id="wrapper">
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">              
                <li>
                    <a href="#">Alle</a>
                </li>
                <li>
                    <a href="#">Kategorie</a>
                </li>
                <li>
                    <a href="#">Kategorie</a>
                </li>
                <li>
                    <a href="#">Kategorie</a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <span class="fa fa-chevron-down"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Sub Kategorie</a></li>
                    <li><a href="#">Sub Kategorie</a></li>
                    <li><a href="#">Sub Kategorie</a></li>
                    <li><a href="#">Sub Kategorie</a></li>
                    <li><a href="#">Sub Kategorie</a></li>
                  </ul>
                </li>
                <hr>
                <li>
                    <a href="#">Tools</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <hr>
                <li>
                    <a href="#">Help</a>
                </li>
                <li>
                    <a href="#">Send Feedback</a>
                </li>
                <li>
                    <a href="#">Privacy</a>
                </li>
                <li>
                    <a href="#">Terms and Conditions</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->	
		<!--Content-->
		<section id="page-content-wrapper">
			<div class="container-fluid">		
				<div class="row">
					<div class="sidebar-nav">
						<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
			                <span class="hamb-top"></span>
			    			<span class="hamb-middle"></span>
							<span class="hamb-bottom"></span>
			            </button>
					</div>
				</div>
			</div>
			<div class="header-search">	
				<div class="container">							
					<div class="row">
						<div class="col-md-12">
							<div id="logo">
								<a href="{{ route('website.home') }}"><img src="{{ url('/') }}/web/images/logo.png" alt="Logo"></a>
							</div>					
							<div class="navbar-search">      
								<h3>{{$page->title}}</h3>
				            </div>
				            </div>
				        </div>
					</div>
				</div>
			</div>
			<div class="container">							
				<div class="row">
					<div class="col-md-12">
						<div id="search-content-box">
							{!! $pages->content !!}
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Content END-->
		
		<script src="{{ url('/') }}/web/js/jquery.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/web/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/web/js/main.js" type="text/javascript"></script>
	</body>
</html>
