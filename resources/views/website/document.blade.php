<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta name="keywords" content="key, words">
        <meta name="description" content="description">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ArchiFOX</title>

        <link href="{{ url('/') }}/web/css/style.css" rel="stylesheet" type="text/css">     
        <link href="{{ url('/') }}/web/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" media="all" type="text/css" >      
        <link href="{{ url('/') }}/favicon.ico" rel="icon" type="image/x-icon">
        <style>
            body {height: 100vh;}
            embed {min-height: 100%;height: 100%;width: 100%}
        </style>
    </head>
    <body>
        <h2 style="text-align: center;">{{$document->title}}</h2>
            <embed src="{{$document->public_url}}" type="application/pdf">
    </body>
</html>