	<!DOCTYPE html>
	<html lang="en-US">
	<head>
	<meta charset="utf-8">
	<meta name="keywords" content="key, words">
	<meta name="description" content="description">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ArchiFOX</title>

	<link href="{{ url('/') }}/web/css/style.css" rel="stylesheet" type="text/css">		
	<link href="{{ url('/') }}/web/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" media="all" type="text/css" >		
	<link href="{{ url('/') }}/favicon.ico" rel="icon" type="image/x-icon">
	
	<style>
	.modal-backdrop.in{opacity:0;}
	</style>

	</head>

	<body id="wrapper">
	<!-- Sidebar -->
	<nav class="navbar navbar-inverse navbar-fixed-top hidden" id="sidebar-wrapper" role="navigation">
	<ul class="nav sidebar-nav">              
	    <li>
	        <a href="#">Alle</a>
	    </li>
	    <li>
	        <a href="#">Kategorie</a>
	    </li>
	    <li>
	        <a href="#">Kategorie</a>
	    </li>
	    <li>
	        <a href="#">Kategorie</a>
	    </li>
	    <li class="dropdown">
	      <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <span class="fa fa-chevron-down"></span></a>
	      <ul class="dropdown-menu" role="menu">
	        <li><a href="#">Sub Kategorie</a></li>
	        <li><a href="#">Sub Kategorie</a></li>
	        <li><a href="#">Sub Kategorie</a></li>
	        <li><a href="#">Sub Kategorie</a></li>
	        <li><a href="#">Sub Kategorie</a></li>
	      </ul>
	    </li>
	    <hr>
	    <li>
	        <a href="#">Tools</a>
	    </li>
	    <li>
	        <a href="#">Settings</a>
	    </li>
	    <hr>
	    <li>
	        <a href="#">Help</a>
	    </li>
	    <li>
	        <a href="#">Send Feedback</a>
	    </li>
	    <li>
	        <a href="#">Privacy</a>
	    </li>
	    <li>
	        <a href="#">Terms and Conditions</a>
	    </li>
	</ul>
	</nav>
	<!-- /#sidebar-wrapper -->	
	<!--Content-->
	<section id="page-content-wrapper">
	<div class="container-fluid hidden">		
		<div class="row">
			<div class="sidebar-nav">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
	                <span class="hamb-top"></span>
	    			<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
	            </button>
			</div>
		</div>
	</div>
	<div class="header-search">	
		<div class="container">							
			<div class="row">
				<div class="col-md-12">
					<div id="logo">
						<a href="{{ route('website.home') }}"><img src="{{ url('/') }}/app-assets/images/logo.png" alt="Logo"></a>
					</div>					
					<div class="navbar-search">      
						<h3 class="text-center">Keywordsuche im Archiv</h3>
						<div class="inline-navbar">	
							<select class="select form-control" id="filter_options">
								<option value="">Alle</option>
								<option value="title">@lang('label.title')</option>
								<option value="keywords">@lang('label.keyword')</option>
								<option value="data">@lang('label.content')</option>
								<option value="address">@lang('label.location')</option>
							</select>
							<input id="search-input" class="navbar-input" type="text" placeholder="Suchworte bitte hier eingeben">
							<button class="navbar-button search-doc-icon" id="search">
								<i class="fa fa-search"></i>
								<span class="hidden-xs">Suchen</span> 
							</button>
						</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	{{-- </div> --}}
	<div class="navbar navbar-default" id="header-menu">
		<div class="container">							
			<div class="row">
				<div class="col-md-12">
					<nav class="header-menu">
						<div class="navbar-header">
							<h4>Filter Categories</h4>
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div id="navbar1" class="navbar-collapse collapse">
							<div class="visible-xs">
								<div class="xs-menu-title">
									<h4>Filter Categories									
										<span>
											<a href="javascript:;" class="close" id="close">X</a>
										</span>
									</h4>
								</div>
							</div>
							<ul class="nav navbar-nav pull-left">
								<li class="categorylist">
									<a href="#" data-index="allcat" class="abcd">Alle</a>
								</li>
								<?php $topfive = $categories->take(5); ?>
								@foreach($topfive as $category)
									<li class="categorylist" id="{{$category->id}}">
										<a href="#" class="abcd" data-index="{{$category->id}}">{{$category->name}}</a>
									</li>
								@endforeach	
								<input type="hidden" name="catname" id="catname">
								<?php $remaincat = $categories->slice(5)->all(); ?>
								<li class="dropdown">
									<a href="#" data-toggle="dropdown">Mehr <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										@foreach($remaincat as $category)
											<li class="categorylist" id="{{$category->id}}"><a href="#" data-index="{{$category->id}}">{{$category->name}}</a></li>
										@endforeach
									</ul>
								</li>
							</ul>								
							<ul class="nav navbar-nav pull-right">
								<li class="dropdown">
									<a href="#" data-toggle="dropdown">Typ <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li>
											<!-- <a href="#" data-value="all" tabIndex="-1" class="doctypeall"><input type="checkbox" id="all_select" name="doctype[]" value="-1"> All</a> -->
											<a href="#" data-value="all" tabIndex="-1" class="doctypeall">
												<label for="" class="mt-checkbox"> Alle
													<input type="checkbox" id="all_select" name="doctype[]" value="-1"> 
													<span class="checkmark"></span>
												</label>
											</a>
										</li>
										<li>
											<!-- <a href="#" data-value="document" tabIndex="-1" class="doctype"><input type="checkbox" name="doctype[]" value="docs" class="doctype"> Document</a> -->
											<a href="#" data-value="document" tabIndex="-1" class="doctype">
												<label for="" class="mt-checkbox"> @lang('label.document')
													<input type="checkbox" name="doctype[]" value="docs" class="doctype">
													<span class="checkmark"></span>
												</label>
											</a>
										</li>
										<li>
											<!-- <a href="#" data-value="audio" tabIndex="-1" class="doctype"><input type="checkbox" name="doctype[]" value="audiodoc" class="doctype"> Audio</a> -->
											<a href="#" data-value="audio" tabIndex="-1" class="doctype">
												<label for="" class="mt-checkbox"> Audio
													<input type="checkbox" name="doctype[]" value="audiodoc" class="doctype"> 
													<span class="checkmark"></span>
												</label>
											</a>
										</li>
										<li>
											<!-- <a href="#" data-value="video" tabIndex="-1" class="doctype"><input type="checkbox" name="doctype[]" value="videodoc" class="doctype"> Video</a> -->
											<a href="#" data-value="video" tabIndex="-1" class="doctype">
												<label for="" class="mt-checkbox"> Video
													<input type="checkbox" name="doctype[]" value="videodoc" class="doctype"> 
													<span class="checkmark"></span>
												</label>
											</a>
										</li>	
										<li><input type="button" class="doctypesearch navbar-button" style="float:right;width:100%;" value="Suchen"></li>	
										<input type="hidden" id="doctype" name="doctype">
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" data-toggle="dropdown">Jahre <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li>
											<!-- <a href="#" data-value="all" tabIndex="-1" class="yearallclass"><input type="checkbox" id="all_year" class="yearrange[]" id="checkall" value="-1"> All</a> -->
											<a href="#" data-value="all" tabIndex="-1" class="yearallclass">
												<label for="" class="mt-checkbox"> Alle
													<input type="checkbox" id="all_year" class="yearrange[]" id="checkall" value="-1"> 
													<span class="checkmark"></span>
												</label>
											</a>
										</li>
										@foreach($years as $year)
											<li id="{{$year->id}}">
												<!-- <a href="#" data-index="{{$year->id}}" tabIndex="-1" class="yearclass"><input type="checkbox" name="yearrange[]" class="yearrange" value="{{$year->id}}"> {{$year->yearrange}}</a> -->
												<a href="#" data-index="{{$year->id}}" tabIndex="-1" class="yearclass">
													<label for="" class="mt-checkbox"> {{$year->yearrange}}
														<input type="checkbox" name="yearrange[]" class="yearrange" value="{{$year->id}}"> 
														<span class="checkmark"></span>
													</label>
												</a>
											</li>											
										@endforeach	
										<input type="hidden" name="year" id="year">
										<li><input type="button" class="yearsearch navbar-button" style="float:right;width:100%;" value="Suchen"></li>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>						
		</div>
	</div>
	
	<div class="container">							
		<div class="row">
			<div class="col-md-12">
				<div class="loader"><img src="{{ url('/') }}/web/images/Loading_icon.gif"></div>												
				<div id="search-content-box">														
					
				</div>
			</div>
		</div>
	</div>
	</section>
	<!--Content END-->
	<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6 footer-links">
				<ul>
					@if(Request::is('page/*'))
						@if($pagespecific->slug=="help")
							<li><a href="#" data-toggle="modal" data-target="#help" id="specific-help-page">Help</a></li>
						@elseif($pagespecific->slug=="privacy")	
							<li><a href="#" data-toggle="modal" data-target="#privacy" id="specific-privacy-page">Privacy</a></li>
						@elseif($pagespecific->slug=="terms-and-conditions")	
							<li><a href="#" data-toggle="modal" data-target="#terms-and-conditions" id="specific-terms-page">Terms and Conditions</a></li>
						@endif	
					@endif	
					@foreach($pagecontent as$page)
						<li class="pagesearch"><input type="hidden" class="pagesearchinput" name="pagesearchid" value="{{$page->id}}" class="hidden"><a href="#" data-toggle="modal" data-target="#{{$page->slug}}">{{$page->title}}</a></li>
					@endforeach	
						{{-- <li><a href="#">Send Feedback</a></li>
						<li><a href="#" data-toggle="modal" data-target="#privacy">Privacy</a></li>
						<li><a href="#" data-toggle="modal" data-target="#terms-and-conditions">Terms and Conditions</a></li> --}}
				</ul>
			</div>
			<div class="col-md-6 copy-right">
				<p>
					© Copyright 2018 Archifox, All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
	</footer>

	@foreach($pagecontent as $pages)
	{{-- @if(($pages->slug == 'privacy') || ($pages->slug == 'help') || ($pages->slug == 'terms-and-conditions')) --}}
		<div class="modal fade full-modal" id="{{$pages->slug}}" data-easein="slideUpBigIn"  tabindex="-1" role="dialog" aria-labelledby="costumModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<h1 class="modal-title">{{$pages->title}}</h1>
							</div>
							<div class="modal-body">
								{!! $pages->content !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	{{-- @endif	 --}}
	@endforeach
	<div class="file-modal">
		<div class="modal fade" id="title-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog mymodal" role="document">
				<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title"><strong>Document Title</strong></h3>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12 descdoc">
								{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur finibus et sapien eget posuere. Sed fringilla metus nec mi venenatis ultricies. Vestibulum at est in leo posuere placerat nec at risus. In et nulla tempus, viverra leo et, suscipit lorem. Proin dignissim libero eu purus sollicitudin pellentesque. Ut eros nibh, luctus at nisl in, mollis dignissim turpis. Curabitur ultrices, leo ut luctus auctor, purus nisi euismod nisl, vel rhoncus turpis metus eu mauris. Suspendisse odio nulla, mollis at pulvinar et, pretium eget augue. </p> --}}
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pdfurlpopup">
								{{-- <div class="card card-border"> --}}
									{{-- <div class="card-block">
										<h5><strong>PDF</strong></h5>
										<hr class="mt-0">
										<div class="doc-content">
											<a href="https://archifox.s3.eu-central-1.amazonaws.com/add-parent.pdf" target="_blank">
												<embed src="https://archifox.s3.eu-central-1.amazonaws.com/add-parent.pdf" style="" type="application/pdf" width="100%" internalinstanceid="5">
											</a>
										</div>
										<a target="_blank" class="btn btn-theme-orange btn-sm" href="https://archifox.s3.eu-central-1.amazonaws.com/add-parent.pdf" download="">Download</a>
									</div>
								</div> --}}
							</div>

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 audiourlpopup">
								{{-- <div class="card card-border">
									<div class="card-block">
										<h5><strong>AUDIO</strong></h5>
										<hr class="mt-0">
										<div class="doc-content">
											<img src="app-assets/images/music-img.png" class="img-responsive" alt="">
											<div class="audio">
												<audio controls class="w-100">
													<source src="https://archifox.s3.eu-central-1.amazonaws.com/add-parent.mp3" type="audio/mp3">
												</audio>
											</div>
										</div>
									</div>
								</div> --}}
							</div>

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 videourlpopup">
							{{-- <div class="card card-border">
									<div class="card-block"> --}}
								{{-- <div class="card card-border">
									<div class="card-block ">
										<h5><strong>VIDEO</strong></h5>
										<hr class="mt-0">
										<div class="doc-content">
											<div id="video-preview"></div>
										</div>
									</div>
								</div> --}}
							</div>

						</div>
					</div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<script src="{{ url('/') }}/web/js/jquery.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/web/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/web/js/main.js" type="text/javascript"></script>
	<script>
	/*-------search add class----*/
	jQuery(document).ready(function($){
		$('#close').on('click', function(){
			// $("#close").hide();
			$("#navbar1").removeClass("in");
    	});
	});

	$(document).ready(function()
	{
		$("#search-content-box").hide();
		$("#header-menu").hide();

	/*Category Filter*/
		$('li.categorylist a').click(function(){
			$('a.abcd').removeClass('active');
			$(this).addClass('active');
			$("#navbar1").removeClass("in");
			$('input[class="yearrange"]').each(function() {
				this.checked = false;
			});
			$('input[class="doctype"]').each(function() {
				this.checked = false;
			});
			$('input#year').val("");
			$('input#doctype').val("");
			var catname = $(this).data("index");
			$('input#catname').val(catname);
			$('#search').trigger('click');
		});

	/*Doc Type Filter*/
		$("li a.doctypeall").on('click',function(){
			if($(this).find('input#all_select').prop("checked") == true)
			{
				$(this).find('input#all_select').prop("checked",true);
				$('li a.doctype :checkbox').prop('checked', true);
        	}
        	else
        	{
				$(this).find('input#all_select').prop("checked",false);
            	$('li a.doctype :checkbox').prop('checked', false);
            }
		});

		$('input.doctypesearch').click(function(){
			$('li a.abcd').removeClass('active');
			$("#navbar1").removeClass("in");
			$('input[class="yearrange"]').each(function() {
				this.checked = false;
			});
			$('input#year').val("");
			var val = [];
		    $(':checkbox:checked').each(function(i){
		      val[i] = $(this).val();
		    });
		    $('input#doctype').val(val);
			$('#search').trigger('click');
		});

	/*Year Range Filter*/	
		$("li a.yearallclass").on('click',function(){
			if($(this).find('input#all_year').prop("checked") == true){
				$('li a.yearclass :checkbox').prop('checked', true);
        	}
        	else {
            	$('li a.yearclass :checkbox').prop('checked', false);
            }
		});

		$('input.yearsearch').click(function(){
			$('li a.abcd').removeClass('active');
			$("#navbar1").removeClass("in");
			$('input[class="doctype"]').each(function() {
				this.checked = false;
			});
			$('input#doctype').val("");
			
			var valyear = [];
		    $(':checkbox:checked').each(function(i){
		      valyear[i] = $(this).val();
		    });
		    $('input#year').val(valyear);
			$('#search').trigger('click');
		});
		

		var search_parameter;
		var ajax_data, show_length, page = 0;
		//int: number of documents to be shown 
		var show_on_document = 10;

		$(window).scroll(function() 
		{
			//if($(window).scrollTop() == ($(document).height() - $(window).height())) 
			if($(window).scrollTop() > ($(document).height() - $(window).height() - 20)) 
			{
				if (ajax_data) 
				{
					page = page + 1;
					var start_index = (page*show_on_document)-show_on_document;
			
					var end_index =  (page*show_on_document);
					if (ajax_data.documents.length < (page*show_on_document)) 
					{
						end_index = ajax_data.documents.length;
					}
					show_documents(start_index,end_index);
				}
			}
				/*console.log("$(window).scrollTop()",$(window).scrollTop());
				console.log("$(document).height()",$(document).height());
				console.log("$(window).height()",$(window).height());*/
		});

		$("#search").click(function()
		{
			search_parameter = $('input#search-input').val();
			catname = $('input#catname').val();
			doctype = $('input#doctype').val();
			yearrange = $('input#year').val();
			
			the_end = false;
			var filter_options = $('select#filter_options option:selected').val();

			if (!search_parameter) 
			{
				$('input#search-input').focus();
			}
			else
			{
				$(".loader").show();
				$("#search-content-box").empty();
				$.ajax({
					url: "{{route('website.search.api')}}",
					data: {search_parameter:search_parameter,filter_options:filter_options,catname:catname,doctype:doctype,yearrange:yearrange},
					type: "post",
					dataType: "json",
					success: function(data)
					{
						if (data.documents.length) 
						{
							ajax_data = data;
							page = 1;
							var start_index = (page*show_on_document)-show_on_document;
			
							var end_index =  (page*show_on_document);
							if (ajax_data.documents.length < (page*show_on_document)) 
							{
								end_index = ajax_data.documents.length;
							}
							show_documents(start_index,end_index);
						}
						else
						{
							$("<p class='time-text'>"+data.documents.length+" ergibt "+parseFloat(data.time_taken).toFixed(4)+" Sekunden </p><h4 class='no-documents' style='text-align:center;'>Keine Daten gefunden...</h4>").appendTo("#search-content-box");
							$("#search-content-box").fadeIn(2000);

							$("body").addClass("search-page");
							$(".loader").fadeOut(500);
						}
					}
				});
			}
		});

		$('input#search-input').on('keypress',function(e)
		{
			if (e.which == '13') 
			{
				$("#search").trigger('click');
			}
		});

		var the_end = false,documents;
		function show_documents(start_index,end_index)
		{
			// console.log("Page: "+page);
			// console.log("Start Index: "+start_index);
			// console.log("End Index: "+(end_index));
			var html = "";
			documents = ajax_data.documents;

			if (start_index < end_index) 
			{
				
				$("#search-content-box").show();
				$("#header-menu").show();
				if (page == 1) 
				{							
					html = "<p class='time-text'>"+documents.length+" ergibt "+parseFloat(ajax_data.time_taken).toFixed(4)+" Sekunden </p>";
				}
				
				for (var i = start_index; i < end_index; i++)
				{
					var id = documents[i].id;
					var title = documents[i].title.replace(new RegExp(search_parameter, 'g'), '<strong>'+search_parameter+'</strong>');
					var public_url = documents[i].public_url;
					var audio_url = documents[i].audio_url;
					var video_url = documents[i].video_url;
					var docdata = documents[i].data;
					var address = documents[i].address;
					var city = documents[i].city;
					var country = documents[i].country;
					var description = documents[i].description.replace(new RegExp(search_parameter, 'g'), '<strong>'+search_parameter+'</strong>');

					html = html + "<div class='search-content-box clearfix'>";
					if(video_url!=null) {
						html = html + "<div class='col-md-9 col-sm-9'>";
					}
					else {
						html = html + "<div class='col-md-12 col-sm-12'>";
					}
					html = html + "<h4>"+title+"";
					if(public_url===null) {}
					else{	
						html = html + "<div class='view-document' style='margin-left:5px;margin-right:5px;'><a href='"+documents[i].slug+"' target='_blank' data-toggle='tooltip' data-placement='top'  title='Dokument anzeigen'><i class='fa fa-eye'></i><i class='fa fa-file-pdf-o'></i></a></div>";
					}
					
					if(audio_url!=null) {
						html = html + "<div class='search-audio' style='margin-left:5px;'><input text='hidden' name='audioid' value='"+id+"' class='hidden'/><audio class='audiourl' src='"+audio_url+"'></audio><a class='playaudio' href='#' data-toggle='tooltip' data-placement='top'  title='Audio abspielen'><i class='fa fa-music'></i><i class='fa fa-play'></i></a><a class='stopaudio hidden' href='#' data-toggle='tooltip' data-placement='top' title='Stop Audio'><i class='fa fa-stop'></i></a></div>";
					}
					html = html + "</h4><a href='javascript:;' class='title-popup open-modal' data-id="+id+"><p>"+description+"....<a href='javascript:;' class='open-modal read-more-link' data-id="+id+">Weiterlesen</a></p></a><ul class='tags'>";
					for (var j = 0; j < documents[i].keywords.length; j++) 
					{
						var keyword = documents[i].keywords[j].replace(new RegExp(search_parameter, 'g'), '<strong>'+search_parameter+'</strong>');
						if (keyword) 
						{
							html = html + "<li>"+keyword+"</li>"; 
						}
					}

					html = html + "</ul>";
					html = html + "<div class='row'><div class='col-lg-12 location-mark'>";
					if(address!=null) 
					{
						if(city==null){city="";}
						else {city=city;}
						html = html + "<div class='location'><i class='fa fa-map-marker'></i> "+city+" ("+country+")</div>";
					}
					html = html + "</div></div></div>";
					if(video_url!=null) {
						var visrc = (!video_url.includes('vimeo')) ? "//www.youtube.com/embed/"+ video_url.split("=")[1] : "//player.vimeo.com/video/"+ video_url.split("/")[3];
						html = html + "<div class='col-md-3 col-sm-3 video-clear'><div class='search-video'><embed src='"+visrc+"' scrolling='no' class='videoembed'></embed></div></div>";
					}
					html = html + "</div>";
					$(html).hide().appendTo("#search-content-box").fadeIn(2000);
					html = "";
				}

				//html = html.replace(new RegExp(search_parameter, 'g'), '<strong>'+search_parameter+'</strong>');
				$("body").addClass("search-page");
				$(".loader").fadeOut(500);
			}
			else
			{
				if (!the_end) 
				{
					the_end = true;
					$("<h4 class='no-documents' style='text-align:center;'> --- Ende der Ergebnisse --- </h4>").hide().appendTo("#search-content-box").fadeIn(2000);
				}
			}
		}

		$(document).on('click','ul.tags a',function(e){
			e.preventDefault();
		});
//col-md-offset-2 to add when only 2 - col-md-offset-4 to add when only 1
	/*Open Modal to view document content*/
		$(document).on('click',"a.open-modal",function(){
			/*console.log($(this).data('id'));
			console.log(documents);*/
			var modal_count = 0;

			var id = $(this).data('id');
			for(var i=0;i<documents.length;i++)
			{
				if(documents[i].id == id) 
				{
					$.ajax({
						url: "{{route('website.docaudiocount.api')}}",
						data: {audioid:id},
						type: "post",
						dataType: "json",
						success: function(data)
						{}
					});
					var description = documents[i].desc;
					var pdfurl = documents[i].public_url;
					var audiourl = documents[i].audio_url;
					var videourl = documents[i].video_url;

					$('#title-modal h3 strong').html(documents[i].title);

					if(description!=null)
					{
						$('#title-modal div.descdoc').html("<p>"+description+"</p>");
					}
					
					if(pdfurl != null)
					{
						modal_count = modal_count+1;
					}
					if(audiourl != null)
					{
						modal_count = modal_count+1;
					}
					if(videourl != null)
					{
						modal_count = modal_count+1;
					}
					
					if(modal_count == 3)
					{
						console.log('3');

						$('div.mymodal').removeClass('modal-lg').removeClass('modal-sm').addClass('modal-xl');
						$('#title-modal div.pdfurlpopup').removeClass('col-lg-6').addClass('col-lg-4');

						$('#title-modal div.audiourlpopup').removeClass('col-lg-6').addClass('col-lg-4');
						$('#title-modal div.videourlpopup').removeClass('col-lg-6').addClass('col-lg-4');
					}
					else if(modal_count == 2)
					{
						console.log('2');
						$('div.mymodal').removeClass('modal-xl').removeClass('modal-sm').addClass('modal-lg');
					}
					else if(modal_count == 1)
					{
						console.log('1');
						$('div.mymodal').removeClass('modal-xl').removeClass('modal-lg').addClass('modal-sm');
						
					}

					if(pdfurl===null)
					{
						$('#title-modal div.pdfurlpopup').empty();$('.pdfurlpopup').addClass('hidden');

						$('#title-modal div.audiourlpopup').removeClass('col-lg-4').addClass('col-lg-6');
						$('#title-modal div.videourlpopup').removeClass('col-lg-4').addClass('col-lg-6');
					}
					else	
					{
						$('.pdfurlpopup').removeClass('hidden');
						$('#title-modal div.pdfurlpopup').empty();
						$('#title-modal div.pdfurlpopup').html("<div class='card card-border'><div class='card-block'><h5><strong>PDF</strong></h5><hr class='mt-0'><div class='doc-content'><a href='"+pdfurl+"' target='_blank'><embed src='"+pdfurl+"' style='' type='application/pdf' width='100%'' internalinstanceid='5'></a></div><a target='_blank' class='btn btn-theme-orange btn-sm' href='"+pdfurl+"' download=''>Download</a></div></div>");
					}
					if(audiourl===null) {
						$('#title-modal div.audiourlpopup').empty();$('.audiourlpopup').addClass('hidden');

						$('#title-modal div.pdfurlpopup').removeClass('col-lg-4').addClass('col-lg-6');
						$('#title-modal div.videourlpopup').removeClass('col-lg-4').addClass('col-lg-6');
					}
					else
					{
						$('.audiourlpopup').removeClass('hidden');
						$('#title-modal div.audiourlpopup').empty();
						$('#title-modal div.audiourlpopup').html("<div class='card card-border'><div class='card-block'><h5><strong>AUDIO</strong></h5><hr class='mt-0'><div class='doc-content'><img src='app-assets/images/music-img.png' class='img-responsive' alt=''><div class='audio'><audio controls class='w-100'><source src='"+audiourl+"' type='audio/mp3'></audio></div></div></div></div>");
					}
					if(videourl===null) {
						$('#title-modal div.videourlpopup').empty();$('.videourlpopup').addClass('hidden');

						$('#title-modal div.pdfurlpopup').removeClass('col-lg-4').addClass('col-lg-6');
						$('#title-modal div.audiourlpopup').removeClass('col-lg-4').addClass('col-lg-6');
					}
					else
					{
						$('.videourlpopup').removeClass('hidden');
						$('#title-modal div.videourlpopup').empty();
					         
					    var src = (!videourl.includes('vimeo')) ? "//www.youtube.com/embed/"+ videourl.split("=")[1] : "//player.vimeo.com/video/"+ videourl.split("/")[3];     

						$('#title-modal div.videourlpopup').html("<div class='card card-border'><div class='card-block'><h5><strong>VIDEO</strong></h5><hr class='mt-0'><div class='doc-content'><div class='video-preview'><iframe class='embed-responsive-item w-100' src='"+src+"' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe></div></div></div></div>");					         
					}
					if(modal_count == 1)
					{
						$('#title-modal div.pdfurlpopup').removeClass('col-md-4').removeClass('col-lg-6').addClass('col-md-12');
						$('#title-modal div.audiourlpopup').removeClass('col-md-4').removeClass('col-lg-6').addClass('col-md-12');
						$('#title-modal div.videourlpopup').removeClass('col-md-4').removeClass('col-lg-6').addClass('col-md-12');
					}
				}
			}
			$('#title-modal').modal('show');
		});
	/*Open Modal to view document content*/	

	/*Page Routes*/	
		$("#specific-help-page").trigger('click');
		$("#specific-privacy-page").trigger('click');
		$("#specific-terms-page").trigger('click');
	/*Page Routes*/	
	

	/*Video Document Count*/
		/*$(document).on("load","iframe",function()
		{
			$(this).contents().on('click',function(){
            		console.log("get video");
			});
    	});

		$(document).on('click','span#abcde',function(e){
			console.log($(document).find("iframe#jigo"));
		});*/
		
	/*Page Search*/	
		$(document).on('click','li.pagesearch',function(event){
			event.preventDefault();
			var pageid = $(this).find('input[name=pagesearchid]').val();
			$.ajax({
				url: "{{route('website.pageviewcount.api')}}",
				data: {pageid:pageid},
				type: "post",
				dataType: "json",
				success: function(data)
				{}
			});
		});
		
	/*Run Audio Player*/	
		$('div.search-audio a.stopaudio').hide();
		$(document).on('click','div.search-audio a.playaudio',function(event) {	
			event.preventDefault();
			var audioid = $(this).siblings('input[name=audioid]').val();
			$.ajax({
				url: "{{route('website.docaudiocount.api')}}",
				data: {audioid:audioid},
				type: "post",
				dataType: "json",
				success: function(data)
				{}
			});
			
			//var audioElement = document.getElementsByClassName('audiourl');
			//console.log(audioElement);
			$(this).hide();
			$(this).siblings('div.search-audio a.stopaudio').removeClass('hidden');
			$(this).siblings('audio.audiourl').get(0).play();
			//console.log($(this).siblings('audio.audiourl'));
		});
		$(document).on('click','div.search-audio a.stopaudio',function(event) {
			event.preventDefault();
			/*var audioElement = document.getElementsByClassName('audiourl');
			console.log(audioElement);*/
			$(this).addClass('hidden');
			$(this).siblings('div.search-audio a.playaudio').show();
			$(this).siblings('audio.audiourl').get(0).pause();
			//console.log($(this).siblings('audio.audiourl'));
		});
		
	});
	</script>
	
	<script src='{{ url('/') }}/web/js/velocity.min.js'></script>
	<script src='{{ url('/') }}/web/js/velocity.ui.min.js'></script>
	<script src="{{ url('/') }}/web/js/index.js"></script>
	</body>
	</html>
