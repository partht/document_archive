@extends('layouts.app')
@section('title'){{config('app.name')." | Pages"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
@if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.page')</h4></h4>
            <div class="heading-elements">
                <a href="{{ route('page.add') }}" class="btn btn-theme-orange"><i class="icon-plus4 white"></i> @lang('label.create_page')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="table-responsive">
                <table id="pages_list" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>@lang('label.title')</th>
                            <th>@lang('label.slug')</th>
                            <th>@lang('label.page_views')</th>
                            <th>@lang('label.option')</th>
                        </tr>
                    </thead>
                    <tbody id="page_tbody">
                        @foreach($pagedata as $page)
                        <tr data-id="{{ $page }}">
                            <td>{{ $page['title'] }}</td>
                            <td>{{ $page['slug'] }}</td>
                            <td>{{ $page['page_views'] }}</td>
                            <td>
                                <div class="btn-group cursor-pointer pull-right">
                                    <a href="{{route('page.edit',['id' => $page['id']])}}" id="edit-page" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                    <a href="#" data-href="{{ route('page.delete',['id' => $page['id']]) }}" class="delete-page text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach                    
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
    <script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        var pageDataTable = $('#pages_list').DataTable({responsive: true,order: [],columnDefs: [ { orderable: false, targets: [0,1,2,3] } ],"bStateSave": true,
            "language": {
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "search": "Suche",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    
        });

        $('#page_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });

        $(document).on('click',"a.delete-page",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.page_delete')",
                text: "@lang('validation.page_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
</script>
@endpush