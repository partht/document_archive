@extends('layouts.app')
@section('title'){{config('app.name')." | Add Page"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
@if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif
@if ($errors->any())
    <input type="hidden" id="open-add">
@endif

<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.create_new_page')</h4></h4>
            <div class="heading-elements">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
        <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('page.store') }}" method="post" enctype="multipart/form-data" name="addpage">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="title">@lang('label.page_title')</label>
                            <div class="col-md-6">
                                <input class="form-control" maxlength="20" type="text" placeholder="@lang('label.enter_page_title')" id="title" required="" data-validation-required-message="Title is required." name="title" value="{{ old('title') }}">
                            <div class="help-block"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="tags">@lang('label.keyword')</label>
                            <div class="col-md-6">
                                {{-- <input class="form-control" type="text" placeholder="Enter keywords" id="keywords" name="keywords"> --}}
                                <select name="tags[]" id="tags" multiple="" style="width: 100%;" required class="form-control">
                                        @foreach ($keywords as $keyword)
                                            <option value="{{$keyword->name}}">{{$keyword->name}}</option>
                                        @endforeach
                                </select>
                            <div class="help-block"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12">
                                <textarea name="contentarea" id="contentarea" class="summernote"></textarea>
                                {{-- <textarea name=”content” id="content" class="summernote"></textarea> --}}
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-actions">

                            <a href="{{route('page.index')}}" class="btn btn-theme-darkblue">
                                <i class="icon-cross2"></i> @lang('label.cancel')
                            </a>

                            <button id="saveform" type="submit" class="btn btn-theme-orange">
                                <i class="icon-check2"></i> @lang('label.save')
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/editors/summernote.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/editors/codemirror.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/editors/theme/monokai.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
@endpush

@push('scripts')
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ url('/') }}/app-assets/vendors/js/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ url('/') }}/app-assets/js/scripts/editors/editor-summernote.js" type="text/javascript"></script>
    <script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script>
        $(document).ready(function() {
            $('textarea').summernote({
                callbacks: {
            onImageUpload: function(image) {
            var sizeKB = image[0]['size'] / 1000;
            var tmp_pr = 0;

            if(sizeKB > 500){
                tmp_pr = 1;

                alert("Wählen Sie weniger als 500kb Image.");
            }

            if(image[0]['type'] != 'image/jpeg' && image[0]['type'] != 'image/png'){

                tmp_pr = 1;

                alert("Wählen Sie png oder jpg Bild.");

            }

            if(tmp_pr == 0){

                var file = image[0];

                var reader = new FileReader();

                reader.onloadend = function() {

                    var image = $('<img>').attr('src', reader.result);

                    $('#contentarea').summernote("insertNode", image[0]);

                }

            reader.readAsDataURL(file);

            }

            }

        }
            });
        });
        $("select#tags").select2({
            tags: true
        });
    </script>
@endpush