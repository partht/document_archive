@extends('layouts.app')
@section('title'){{config('app.name')." | Keywords"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
{{-- @if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif --}}
@if ($errors->any())
    <input type="hidden" id="open-add">
@endif
@if ($errors->any())
    <input type="hidden" id="open-edit">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.keyword')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <!-- <button class="btn btn-theme-orange btn-sm" id="add-keyword"><i class="icon-plus4 white"></i> Add Keyword</button> -->
                <a href="#" class="btn btn-theme-orange" id="add-keyword"><i class="icon-plus4 white"></i> @lang('label.add_keyword')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="table-responsive">
                <table id="keywords" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>@lang('label.name')</th>
                            <th>@lang('label.slug')</th>
                            <th>@lang('label.option')</th>
                        </tr>
                    </thead>
                    <tbody id="key_tbody">
                    @foreach($keywords as $keyword)
                    <tr data-id="{{ $keyword['id'] }}">
                        <td>
                             <span class="name">{{ $keyword['name'] }}</span>
                        </td>
                        <td class="text-xs">
                                {{ $keyword['slug'] }}
                        </td>
                        <td>
                                <div class="btn-group cursor-pointer pull-right">
                                        <a href="#" id="edit-keyword" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                        <a href="#" data-href="{{ route('keyword.delete',['id' => $keyword['id']]) }}" id="delete-keyword" class="text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                    <!--</span>-->
                                </div>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
                   
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Keyword Modal -->
<div class="modal fade text-xs-left" id="add-keyword" tabindex="-1">
    <div class="modal-dialog modal-sm" role="keyword">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.add_keyword')</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('keyword.store')}}" method="post" id="add-keyword-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="keyword-add-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="keyword-name">@lang('label.keyword_name') : <span class="danger">*</span></label>
                            <input name="name" maxlength="15" type="text" id="keyword-name" class="form-control" placeholder="Schlüsselwortname" value="{{ old('name')}}">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-theme-darkblue closepopup" data-dismiss="modal">@lang('label.close')</button>
                <button type="button" class="btn btn-theme-orange" id="save-keyword">@lang('label.add_keyword')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Add Keyword Modal -->

<!-- Edit Keyword Modal -->
<div class="modal fade text-xs-left" id="edit-keyword" tabindex="-1">
    <div class="modal-dialog modal-sm" role="keyword">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">Keyword bearbeiten</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-action="{{route('keyword.update',['id'=>''])}}" method="post" id="edit-keyword-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="keyword-edit-error" style="display: none"></div>
                    <div class="row">
                        <!-- <div class="col-md-3">
                            <div class="help-block"></div>
                        </div> -->
                        <div class="col-md-12">
                            <label for="keyword-name">@lang('label.keyword_name') : <span class="danger">*</span></label>
                            <input name="name" maxlength="15" type="text" id="keyword-name" class="form-control" placeholder="Keyword Name">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-theme-darkblue closepopup" data-dismiss="modal">@lang('label.close')</button>
            <button type="button" class="btn btn-theme-orange" id="update-keyword">@lang('label.save_keyword')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Edit Keyword Modal -->
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var userDataTable = $('#keywords').DataTable({responsive: true,order: [],columnDefs: [ { orderable: false, targets: [0,1,2] } ],"bStateSave": true,
            "language": {
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "search": "Suche",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    });

        $('#key_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });
        
        $('a#add-keyword').on('click',function(){
            $("div#add-keyword").modal('show');
        });

        if ($("input#open-add").length) 
        {
            $("a#add-keyword").trigger('click');
        }

/*on close of popup, form data and validation message clear*/
        $(".closepopup, .close").click(function(){
            $("#add-keyword-form")[0].reset();
            $('div#keyword-add-error').hide();
            $('div#keyword-edit-error').hide();
        });
/*on close of popup, form data and validation message clear*/

        $("button#save-keyword").on('click',function(){
            $('div#keyword-add-error').hide();
            var name = $('input#keyword-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.key_required')');
            }
            if(!name.match(/^[0-9 a-z A-Z]+$/)) 
            {
                errors.push('@lang('validation.key_numeric')');    
            }

            if (errors.length) 
            {
                $('div#keyword-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#keyword-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#keyword-add-error').show().delay(5000).fadeOut(1000);
                }
            }
            else
            {
                $('form#add-keyword-form').submit();
            }
        });


        $(document).on('click','a#edit-keyword',function()
        {   
            var name = $(this).closest('tr').find('span.name').html();
            var id = $(this).closest('tr').data('id');
            
            $("div#edit-keyword input#keyword-name").val(name);

            var action = $("form#edit-keyword-form").data('action');
            action = action+"/"+id;
            $("form#edit-keyword-form").attr('action',action);
            $("div#edit-keyword").modal('show');
        });

        $("button#update-keyword").on('click',function(){
            $('div#keyword-edit-error').hide();

            var name = $('div#edit-keyword input#keyword-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.key_required')');
            }
            if(!name.match(/^[0-9 a-z A-Z]+$/)) 
            {
                errors.push('@lang('validation.key_numeric')');    
            }

            if (errors.length) 
            {
                $('div#keyword-edit-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#keyword-edit-error').append("<li>"+errors[i]+"</li>");
                    $('div#keyword-edit-error').show().delay(5000).fadeOut(1000);
                }
            }
            else
            {
                $('form#edit-keyword-form').submit();
            }
        });

        $(document).on('click',"a#delete-keyword",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.key_delete')",
                text: "@lang('validation.key_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
</script>
@endpush