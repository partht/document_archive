@extends('layouts.app')
@section('title'){{config('app.name')." | Time Range"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
{{-- @if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif
@if (Session::has('open_edit'))
    <input type="hidden" id="open-edit">
@endif --}}
@if ($errors->any())
    <input type="hidden" id="open-edit">
@endif
@if ($errors->any())
    <input type="hidden" id="open-add">
@endif

<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.time_range')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <a href="#" class="btn btn-theme-orange" id="add-range"><i class="icon-plus4 white"></i> @lang('label.add_timerange')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="table-responsive">
                <table id="timerange" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>@lang('label.year_range')</th>
                            <th>@lang('label.min_year')</th>
                            <th>@lang('label.max_year')</th>
                            <th>@lang('label.option')</th>
                        </tr>
                    </thead>
                    <tbody id="tmr_tbody">
                        @foreach($timerange as $timerange)
                        <tr data-id="{{ $timerange['id'] }}">
                            <td><span class="yearrange">{{$timerange['yearrange']}}</span></td>
                            <td><span class="minyear">{{$timerange['minyear']}}</span></td>
                            <td><span class="maxyear">{{$timerange['maxyear']}}</span></td>
                            <td>
                                <div class="btn-group cursor-pointer pull-right">
                                        <a href="#" id="edit-timerange" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                        <a href="#" data-href="{{ route('timerange.delete',['id' => $timerange['id']]) }}" id="delete-timerange" class="text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Time Range Modal -->
<div class="modal fade text-xs-left" id="add-range" tabindex="-1">
    <div class="modal-dialog modal-sm" role="range">
        <div class="modal-content">
            <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.add_timerange')</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('timerange.store')}}" method="post" id="add-timerange-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="timerange-add-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="yearrange">@lang('label.year_range') : <span class="danger">*</span></label>
                            <input name="yearrange" maxlength="25" type="text" id="yearrange" class="form-control" placeholder="@lang('label.enter_year_range')" value="{{old('yearrange')}}">
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-12">
                            <label for="minyear">@lang('label.min_year') : <span class="danger">*</span></label>
                            <input name="minyear" maxlength="10" type="text" id="minyear" class="form-control" placeholder="@lang('label.enter_min_year')" value="{{old('minyear')}}">
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-12">
                            <label for="maxyear">@lang('label.max_year') : <span class="danger">*</span></label>
                            <input name="maxyear" maxlength="10" type="text" id="maxyear" class="form-control" placeholder="@lang('label.enter_max_year')" value="{{old('maxyear')}}">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
                <button type="button" class="btn btn-theme-orange" id="save-range">@lang('label.add_timerange')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Add Time Rage Modal -->

<!--  -->

<!-- Edit Keyword Modal -->
<div class="modal fade text-xs-left" id="edit-range" tabindex="-1">
    <div class="modal-dialog modal-sm" role="range">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.edit_timerange')</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-action="{{route('timerange.update',['id'=>''])}}" method="post" id="edit-timerange-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="timerange-edit-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="yearrange">@lang('label.year_range') : <span class="danger">*</span></label>
                            <input name="yearrange" maxlength="25" type="text" id="yearrange" class="form-control" placeholder="Enter Year Range">
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-12">
                            <label for="minyear">@lang('label.min_year') : <span class="danger">*</span></label>
                            <input name="minyear" maxlength="10" type="text" id="minyear" class="form-control" placeholder="Enter Min Year">
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-12">
                            <label for="maxyear">@lang('label.max_year') : <span class="danger">*</span></label>
                            <input name="maxyear" maxlength="10" type="text" id="maxyear" class="form-control" placeholder="Enter Max Year">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
            <button type="button" class="btn btn-theme-orange" id="update-timerange">@lang('label.save_range')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Edit Keyword Modal -->

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var userDataTable = $('#timerange').DataTable({ordering: false,"bStateSave": true,
            "language": {
                "search": "Suche",
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    });

        $('#tmr_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });
        
        $('a#add-range').on('click',function(){
            $("div#add-range").modal('show');
        });
        if ($("input#open-add").length) 
        {
            $("a#add-range").trigger('click');
        }
        if ($("input#open-add").length) 
        {
            $("a#edit-range").trigger('click');
        }

/*on close of popup, form data and validation message clear*/
        $(".modal").on("hidden.bs.modal", function(){
            $("#add-timerange-form")[0].reset();
            $('div#timerange-add-error').hide();
            $('div#timerange-edit-error').hide();
            $('div.alert').empty();
            $('div.alert').hide();
        });
/*on close of popup, form data and validation message clear*/        

        $("#add-timerange-form #yearrange").change(function(){
            var yearrange = $('#add-timerange-form input#yearrange').val();
            if(yearrange.match(/[-]/)) {
                var y = yearrange.split("-");
            }
            else if(yearrange.match(/[ ]/)){
                var y = yearrange.split(" ");    
            }
            else {
                alert("Enter '-' or 'space' between year range");
                return false;
            }

            if(y[0]=='before ' || y[0]=='before' || y[0]=='Before' || y[0]=='Before '){
                var minyear = 0;
                $('#add-timerange-form input#minyear').val(minyear);
            }
            else {
                $('#add-timerange-form input#minyear').val(y[0]);    
            }
            var maxyear = y[1].replace(/\s+/g, '');
            $('#add-timerange-form input#maxyear').val(maxyear);
        });

        $("button#save-range").on('click',function(){
            $('div#timerange-add-error').hide();
            var yearrange = $('input#yearrange').val();
            var minyear = $('input#minyear').val();
            var maxyear = $('input#maxyear').val();
            var errors = [];
            if (!yearrange) 
            {
                errors.push('@lang('validation.year_range_required')');
            }
            if(!yearrange.match(/^[A-Z a-z 0-9 -]+$/)) 
            {
                errors.push('@lang('validation.year_range_numeric')');    
            }
            if (!minyear) 
            {
                errors.push('@lang('validation.min_year_required')');
            }
            if(!minyear.match(/^[0-9 ]+$/)) 
            {
                errors.push('@lang('validation.min_year_numeric')');    
            }
            if (!maxyear) 
            {
                errors.push('@lang('validation.max_year_required')');
            }
            if(!maxyear.match(/^[0-9 ]+$/)) 
            {
                errors.push('@lang('validation.max_year_numeric')');    
            }
            if(maxyear!="" && minyear!="" && (minyear >= maxyear))
            {
                errors.push('@lang('validation.min_less_max')');
            }
            if (errors.length) 
            {
                $('div#timerange-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#timerange-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#timerange-add-error').show().delay(5000).fadeOut(1000);
                }
            }
            else
            {
                $('form#add-timerange-form').submit();
            }
        });

        //Edit Time Range
        $(document).on('click','a#edit-timerange',function()
        {   
            var id = $(this).closest('tr').data('id');
            var yearrange = $(this).closest('tr').find('span.yearrange').html();
            var minyear = $(this).closest('tr').find('span.minyear').html();
            var maxyear = $(this).closest('tr').find('span.maxyear').html();
            
            $("div#edit-range input#yearrange").val(yearrange);
            $("div#edit-range input#minyear").val(minyear);
            $("div#edit-range input#maxyear").val(maxyear);

            var action = $("form#edit-timerange-form").data('action');
            action = action+"/"+id;
            $("form#edit-timerange-form").attr('action',action);
            $("div#edit-range").modal('show');
        });

        $("button#update-timerange").on('click',function(){
            $('div#timerange-edit-error').hide();
            var yearrange = $('div#edit-range input#yearrange').val();
            var minyear = $('div#edit-range input#minyear').val();
            var maxyear = $('div#edit-range input#maxyear').val();

            var errors = [];
            if (!yearrange) 
            {
                errors.push('@lang('validation.year_range_required')');
            }
            if(!yearrange.match(/^[A-Z a-z 0-9-<>]+$/)) 
            {
                errors.push('@lang('validation.year_range_numeric')');    
            }
            if (!minyear) 
            {
                errors.push('@lang('validation.min_year_required')');
            }
            if(!minyear.match(/^[0-9]+$/)) 
            {
                errors.push('@lang('validation.min_year_numeric')');    
            }
            if (!maxyear) 
            {
                errors.push('@lang('validation.max_year_required')');
            }
            if(!maxyear.match(/^[0-9]+$/)) 
            {
                errors.push('@lang('validation.max_year_numeric')');    
            }
            if(maxyear!="" && minyear!="" && minyear >= maxyear)
            {
                errors.push('@lang('validation.min_less_max')');
            }
            if (errors.length) 
            {
                $('div#timerange-edit-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#timerange-edit-error').append("<li>"+errors[i]+"</li>");
                    $('div#timerange-edit-error').show().delay(10000).fadeOut(1000);
                }
            }
            else
            {
                $('form#edit-timerange-form').submit();
            }
        });

        /*$('.modal').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
        });*/

        //Delete Year Range
        $(document).on('click',"a#delete-timerange",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.timerange_delete')",
                text: "@lang('validation.timerange_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
</script>
@endpush