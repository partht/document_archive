@extends('layouts.app')
@section('title'){{config('app.name')." | Categories"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
{{-- @if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif --}}
@if ($errors->any())
    <input type="hidden" id="open-add">
@endif
@if ($errors->any())
    <input type="hidden" id="open-edit">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.categories')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <!-- <button class="btn btn-theme-orange btn-sm" id="add-category"><i class="icon-plus"></i> Add Category</button> -->
                <a href="#" class="btn btn-theme-orange" id="add-category"><i class="icon-plus"></i> @lang('label.add_category')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="table-responsive">
                <table id="categories" class="table table-striped table-hover">
                    <thead>
                        <tr> 
                            <th width="0%"></th>
                            <th width="40%">@lang('label.name')</th>
                            <th width="40%">@lang('label.slug')</th>
                            <th width="15%">@lang('label.option')</th>
                        </tr>
                    </thead>
                    <tbody id="cat_tbody">
                        @foreach($categories as $category)
                        <tr data-id="{{ $category['id'] }}" data-parent-id="{{ $category['parent_id'] }}">
                            <td></td>
                            <td>
                                    @for ($i = 0; $i < $category['depth']; $i++){{"— "}}@endfor <span class="name">{{ $category['name'] }}</span>
                            </td>
                            <td>
                                {{ $category['slug'] }}
                            </td>
                            <td>
                                <div class="btn-group cursor-pointer pull-right">
                                        <a href="#" id="edit-category" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                        <a href="#" data-href="{{ route('category.delete',['id' => $category['id']]) }}" id="delete-category" class="text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Category Modal -->
<div class="modal fade text-xs-left" id="add-category" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.add_category')</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger" id="addalert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('category.store')}}" method="post" id="add-category-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="category-add-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="parent-category">@lang('label.parent_category') : </label>
                            <!-- <input type="text" id="parent-category" class="form-control" placeholder="First Name"> -->
                            <select id="parent-category" class="form-control select2" name="parent_id" style="width: 100%;">
                                <option value="0">--- Kein Elternteil ---</option>
                                @foreach ($categories as $category)
                                    <option data-depth="{{ $category['depth'] }}" value="{{ $category['id'] }}">@for ($i = 0; $i < $category['depth']; $i++){{"—"}}@endfor{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="category-name">@lang('label.category_name') : <span class="danger">*</span></label>
                            <input name="name" maxlength="15" type="text" id="category-name" class="form-control" placeholder="Kategorie Name" required="Category Name is Required." value="{{old('name')}}">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
                <button type="button" class="btn btn-theme-orange" id="save-category">@lang('label.add_category')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Add Category Modal -->
{{-- 
=============================== --}}

<!-- Edit Category Modal -->
<div class="modal fade text-xs-left" id="edit-category" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.edit_category')</h4>
            </div>
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger" id="editalert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form data-action="{{route('category.update',['id'=>''])}}" method="post" id="edit-category-form"> 
                    {{ csrf_field() }}
                    <div class="alert alert-danger" id="category-edit-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="parent-category">@lang('label.parent_category') : </label>
                            <select id="parent-category" class="form-control select2" name="parent_id" style="width: 100%;">
                                <option value="0">--- Kein Elternteil ---</option>
                                @foreach ($categories as $category)
                                    <option data-parent-id="{{ $category['parent_id'] }}" value="{{ $category['id'] }}">@for ($i = 0; $i < $category['depth']; $i++){{"—"}}@endfor{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="category-name">@lang('label.category_name') : <span class="danger">*</span></label>
                            <input name="name" maxlength="15" type="text" id="category-name" class="form-control" placeholder="Category Name">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
            <button type="button" class="btn btn-theme-orange" id="update-category">@lang('label.save_category')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Edit Category Modal -->
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var userDataTable = $('#categories').DataTable({responsive: true,order: [],columnDefs: [ { orderable: false, targets: [0,1,2,3] } ],"bStateSave": true,
            "language": {
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "search": "Suche",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    
    });

        $('#cat_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });
        
        $('a#add-category').on('click',function(){
            $("div#add-category").modal('show');
        });

        if ($("input#open-add").length) 
        {
            $("a#add-category").trigger('click');
        }


        $("select#parent-category").select2();

/*on close of popup, form data and validation message clear*/
        $(".modal").on("hidden.bs.modal", function(){
            $("#add-category-form")[0].reset();
            $('div#category-add-error').hide();
            $('div#category-edit-error').hide();
            $('div#addalert').empty();
            $('div#addalert').hide();
        });
/*on close of popup, form data and validation message clear*/        

        $("button#save-category").on('click',function(){
            $('div#category-add-error').hide();
            $('div#category-add-error').empty();
            var parent_id = $('select#parent-category option:selected').val();
            var name = $('input#category-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.cat_required')');
            }
            /*if(!name.match(/^[0-9a-zA-Z]+$/)) 
            {
                errors.push('@lang('validation.cat_numeric')');    
            }*/

            if (errors.length) 
            {
                $('div#category-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#category-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#category-add-error').show().delay(5000).fadeOut(1000);
                }
            }
            else
            {
                $('form#add-category-form').submit();
            }
        });

        function disable_it_and_its_subcategories(id) 
        {
            $("div#edit-category select#parent-category option").each(function(){
                if ($(this).val() == id) 
                {
                    $(this).prop('disabled',true);
                }

                if ($(this).data('parent-id') == id) 
                {
                    disable_it_and_its_subcategories($(this).val());
                }
            });
        }


        $(document).on('click','a#edit-category',function()
        {   
            $('div#editalert').empty();
            $('div#editalert').hide();
            var name = $(this).closest('tr').find('span.name').html();
            //alert(name);
            var id = $(this).closest('tr').data('id');
            var parent_id = $(this).closest('tr').data('parent-id');
            $("div#edit-category select#parent-category option").prop('disabled',false);
            disable_it_and_its_subcategories(id);
            
            $("div#edit-category select#parent-category").val(parent_id).trigger('change');
            $("div#edit-category input#category-name").val(name);

            var action = $("form#edit-category-form").data('action');
            action = action+"/"+id;
            $("form#edit-category-form").attr('action',action);
            $("div#edit-category").modal('show');
        });

        $("button#update-category").on('click',function(){
            $('div#category-edit-error').hide();
            var parent_id = $('div#edit-category select#parent-category option:selected').val();
            var name = $('div#edit-category input#category-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.cat_required')');
            }
           /* if(!name.match(/^[0-9a-zA-Z]+$/)) 
            {
                errors.push('@lang('validation.cat_numeric')');    
            }*/

            if (errors.length) 
            {
                $('div#category-edit-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#category-edit-error').append("<li>"+errors[i]+"</li>");
                    $('div#category-edit-error').show().delay(5000).fadeOut(1000);
                }
            }
            else
            {
                $('form#edit-category-form').submit();
            }
        });

        $(document).on('click',"a#delete-category",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.cat_delete')",
                text: "@lang('validation.cat_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
</script>
@endpush
