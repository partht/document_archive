@php
   // dd($user->document_audit->orderBy('id','desc')->take(10));
@endphp
@extends('layouts.app')
@section('title'){{config('app.name')." | View User"}}@endsection

@section('content')

<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.user_view')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <a href="{{ route('user.index') }}" class="btn btn-theme-orange"><i class="icon-arrow-left white"></i>  @lang('label.back')</a>
            </div>
        </div>
    </div>
    
    <div class="card-body">
        <div class="card-block">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.user_name'):</strong></div>
                            <div class="col-lg-10 col-lg-9">{{ $user['name'] }}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.role'):</strong></div>
                            <div class="col-lg-10 col-lg-9">@if($user['role'] == 'A')
                                        {{ 'Admin' }}
                                        @else   
                                            {{ 'Sub Admin' }}
                                        @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.user_emailid'):</strong></div>
                            <div class="col-lg-10 col-lg-9">{{ $user['email'] }}</div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('label.recent_act')</h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <th>Datum</th>
                                    <!-- <th>User Name</th>
                                    <th>Role</th> -->
                                    <th>Dokumente</th>
                                    <th>@lang('label.doc_action')</th>
                                    <th>@lang('label.user_ipaddress')</th>
                                </thead>
                               
                                <tbody>
                                @if(count($user->document_audit) != 0)
                                    @foreach($user->document_audit->sortByDesc('id',true)->take(10) as $document_audit)
                                       {{--  @if($document_audit['action'] == 'D')
                                        @elseif($document_audit->document['title'] == '')
                                        @else --}}
                                        <tr>
                                            <td>{{ $document_audit['action_time'] }}</td>
                                            <!-- <td>{{ $user['name'] }}</td>
                                            <td>
                                                @if($user['role'] == 'A')
                                                {{ 'Admin' }}
                                                @else   
                                                    {{ 'Sub Admin' }}
                                                @endif
                                            </td> -->
                                            
                                            <td>
                                                <!-- @if($document_audit->document['public_url']!= "")
                                                    <a href="{{$document_audit->document['public_url']}}" class="tag tag-theme-orange round" target="_blank">
                                                @elseif($document_audit->document['audio_url']!= "")   
                                                    <a href="{{$document_audit->document['audio_url']}}" class="tag tag-theme-orange round" target="_blank">     
                                                @endif        
                                                        {{ $document_audit->document['title'] }} -->
                                            <!-- Static -->
                                            <!-- <a href="#" class="tag tag-theme-border-orange round custom-user-tag text-uppercase">
                                                <i class="icon-file-pdf-o icon-tag"></i> PDF
                                            </a>
                                            <a href="#" class="tag tag-theme-border-darkblue round custom-user-tag text-uppercase">
                                                <i class="icon-file-audio-o icon-tag"></i> Audio
                                            </a>
                                            <a href="#" class="tag tag-theme-border-pink round custom-user-tag text-uppercase">
                                                <i class="icon-file-movie-o icon-tag"></i> Video
                                            </a> -->

                                            <!--Static-option-2-->
                                            {{-- <a href="{{ route('document.showaudit/$document_audit->document["id"]]') }}" target="_blank" class="link-orange user-upload-items">{{ $document_audit->document['title'] }}</a> --}}

                                            @if($document_audit['action'] == 'D')
                                                {{ $document_audit->document['title'] }}
                                            @else    
                                                <a class="link-orange" href="{{ route('document.showaudit',['id' => $document_audit->document['id']]) }}">{{ $document_audit->document['title'] }}</a>

                                                @if($document_audit->document['public_url']!= "")
                                                    <a href="{{$document_audit->document['public_url']}}" target="_blank" class="link-orange user-upload-items">
                                                     <i class="icon-file-pdf"></i>     
                                                    </a>
                                                @endif
                                                @if($document_audit->document['audio_url']!= "")    
                                                    <a href="{{$document_audit->document['audio_url']}}" target="_blank" class="link-darkblue user-upload-items">
                                                    <i class="icon-music2"></i>     
                                                    </a>
                                                @endif
                                                @if($document_audit->document['video_url']!= "")    
                                                    <a href="{{$document_audit->document['video_url']}}" target="_blank" class="link-pink user-upload-items">
                                                    <i class="icon-video2"></i>     
                                                    </a>
                                                @endif    
                                            @endif    
                                            </td>
                                            <td>
                                            @if($document_audit['action'] == 'U')
                                                {{'Updated'}}
                                            @elseif($document_audit['action'] == 'C')
                                                {{'Created'}}
                                            @elseif($document_audit['action'] == 'D')
                                                {{'Deleted'}}    
                                            @endif                                    
                                            </td>
                                            <td>
                                                {{ $document_audit['user_ipaddress'] }}             
                                            </td>
                                        </tr>
                                        {{-- @endif --}}
                                    @endforeach
                                    @else
                                    <tr><td colspan="5" style="text-align: center;">{{ "Keine Daten gefunden..." }}</td></tr>
                                @endif
                                </tbody>
                                
                            </table>
                        </div>
                        @if(count($user->document_audit) != 0)
                        <div class="float-xs-right"><a class="btn btn-theme-orange" href="{{ route('report.index',['userid' => $user['id']]) }}">@lang('label.view_more')</a></div>
                        @endif
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>   
@endsection