@extends('layouts.app')
@section('title'){{config('app.name')." | Users"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
{{-- @if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif --}}
@if ($errors->any())
    <input type="hidden" id="open-add">
@endif
@if ($errors->any())
    <input type="hidden" id="open-edit">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.users')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <!-- <button class="btn btn-theme-orange btn-sm" id="add-user"><i class="icon-plus"></i> Add User</button> -->
                <a href="#" class="btn btn-theme-orange" id="add-user"><i class="icon-plus4 white"></i> @lang('label.add_user')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table id="users" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>@lang('label.name')</th>
                        <th>@lang('label.email')</th>
                        <th>@lang('label.role')</th>
                        <th>@lang('label.docs_created')</th>
                        <th>@lang('label.last_login')</th>
                        <th class="th-custom-width">@lang('label.option')</th>
                    </tr>
                </thead>
                <tbody id="user_tbody">
                    @foreach($usersdata as $user)
                    <tr data-id="{{ $user['id'] }}">
                        <td>
                            <!--<div class="media">
                                <div class="media-body media-middle">
                                    <a target="_blank" href="" class="media-heading"> <span class="name">--><span class="name">{{ $user['name'] }}</span>@if($user['block'] == 'B')<span class="text-warning"><i style="margin-left: 5%;" class="icon-lock"></i></span>@endif<!--</span></a>
                                </div>
                            </div>-->
                        </td>
                        <td>
                            <span class="email">{{ $user['email'] }}</span>
                        </td>
                        <td>
                            @if($user['role']=='A')
                                <span class="role">{{ 'Admin' }}</span>
                            @else
                                <span class="role">{{ 'Sub Admin' }}</span>
                            @endif  
                        </td>
                        <td>
                            <?php 
                            $i = 0;                             
                            foreach($userdocs as $userdoc) {
                                if($userdoc['created_by'] == $user['id'])
                                    { $i++; }
                            }
                            echo $i;
                            ?>  
                        </td>
                        <td>
                            {{ $user['last_login'] }}
                        </td>
                        <td>
                            <div class="btn-group cursor-pointer pull-right">
                                    <a href="{{route('user.show',['id' => $user['id']])}}" id="show-user" class="text-info btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Ansehen"><i class="icon-eye3"></i></a>
                                    

                                    <a href="#" id="edit-user" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                    <a href="#" data-href="{{ route('user.delete',['id' => $user['id']]) }}" class="delete-user text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                    @if(Auth::user()->id == $user['id']) 
                                    @else 
                                        @if($user['block'] == 'U')
                                            <a href="#" data-href="{{ route('user.editblock',['id' => $user['id'],$user['block']]) }}" class="block-user text-warning btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Blocken"><i class="icon-lock"></i></a>
                                        @else
                                            <a href="#" data-href="{{ route('user.editblock',['id' => $user['id'],$user['block']]) }}" class="unblock-user text-success btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Freischalten"><i class="icon-unlocked2"></i></a>
                                        @endif
                                    @endif      
                                <!--</span>-->
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Add user Modal -->

<div class="modal fade text-xs-left" id="add-user" tabindex="-1">
    <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1"> @lang('label.add_user')</h4>
            </div>
            <div class="modal-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
                <form name="adduser" id="adduser" action="{{ route('user.store') }}" method="post">
                        {{csrf_field()}}
                        <div class="alert alert-danger" id="user-add-error" style="display: none"></div>
                        
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="title">@lang('label.name')</label>
                            <div class="col-md-9">
                                <input class="form-control" maxlength="15" type="text" placeholder="@lang('label.enter_name')" id="name" name="name" required="Name required" value="{{ old('name') }}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="Email">@lang('label.email')</label>
                            <div class="col-md-9">
                                <input class="form-control" maxlength="30" type="email" placeholder="@lang('label.enter_email')" id="email" name="email" required email-validation-required-message="Valid Email required." value="{{ old('email') }}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="Email">@lang('label.password')</label>
                            <div class="col-md-9">
                                <input type="hidden" name="length" value="10">
                                <fieldset>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="password" name="password" required data-validation-required-message="Password is required." aria-describedby="button-addon2" pattern="().{6,}" title="password must be atleast 6 characters">
                                        <span class="input-group-btn" id="button-addon2">
                                            <button class="btn btn-theme-orange" type="button" onClick="generate();">@lang('label.generate_password')</button>
                                        </span>
                                    </div>
                                </fieldset>
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="email">@lang('label.select_role')</label>
                            <div class="col-md-9">
                                <select name="role" id="role" style="width: 100%;" required>
                                    <option value="A">@lang('label.select_role')</option>
                                    <option value="A">Admin</option>
                                    <option value="S">Sub-Admin</option>
                                </select>
                            <div class="help-block"></div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" id="closepopup" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
                            <button type="button" class="btn btn-theme-orange" id="save-user"> @lang('label.add_user')</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<!-- END Add User Modal -->

<!-- Edit User Modal -->
<div class="modal fade text-xs-left" id="edit-user" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.edit_user')</h4>
            </div>
            <div class="modal-body">
                <form data-action="{{ route('user.update',['id'=>'']) }}" method="post" id="edituser">
                        {{csrf_field()}}
                        <div class="alert alert-danger" id="user-edit-error" style="display: none"></div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="name">@lang('label.name')<span class="danger">*</span></label>
                            <div class="col-md-6">
                                <input class="form-control" maxlength="15" type="text" placeholder="Enter name" id="username" name="name" value="{{$user['name']}}">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="email">@lang('label.email')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" readonly value="{{$user['email']}}" id="email">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="role">@lang('label.role')</label>
                            <div class="col-md-6">
                                <select name="role" id="role" class="form-control" style="width: 100%;">
                                        <option value="A">Admin</option>
                                        <option value="S">Sub Admin</option>
                                </select>
                            </div>
                            <div class="help-block"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="closepopupedit" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
                            <button type="button" class="btn btn-theme-orange" id="update-user">@lang('label.save_user')</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<!-- END Edit Category Modal -->
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/css/plugins/forms/validation/form-validation.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var userDataTable = $('#users').DataTable({responsive: true,order: [],columnDefs: [ { orderable: false, targets: [0,1,2,3,4,5] } ],"bStateSave": true,
            "language": {
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "search": "Suche",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    });

        $('#user_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });

        $("input,textarea").not("[type=submit]").jqBootstrapValidation(); 

        $('a#add-user').on('click',function(){
            $("div#add-user").modal('show');
        });

        if ($("input#open-add").length) 
        {
            $("a#add-user").trigger('click');
        }

        $("select#role").select2();

/*on close of popup, form data and validation message clear*/
        $("#closepopup, .close").click(function(){
            $("#adduser")[0].reset();
            $('div#user-add-error').hide();
        });

        $("#closepopupedit, .close").click(function(){
            $('div#user-edit-error').hide();
        });
/*on close of popup, form data and validation message clear*/

        $("button#save-user").on('click',function(){
            $('div#user-add-error').hide();
            
            var name = $('input#name').val();
            var email = $('input#email').val();
            var password = $('input#password').val();
            var errors = [];
            if (!name && !email && !password) 
            {
                errors.push('@lang('validation.user_required')');
                errors.push('@lang('validation.email_required')');
                errors.push('@lang('validation.password_required')');
            }
            if(!name.match(/^[0-9 a-z A-Z]+$/))
            {
                errors.push('@lang('validation.user_numeric')');
            }

            if (errors.length) 
            {
                $('div#user-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#user-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#user-add-error').show().delay(10000).fadeOut(3000);
                }
            }
            else
            {
                $('form#adduser').submit();
            }
        });

        $(document).on('click','a#edit-user',function()
        {   
            var name = $(this).closest('tr').find('span.name').html();
            var email = $(this).closest('tr').find('span.email').html();
            var role = $(this).closest('tr').find('span.role').html();
            var id = $(this).closest('tr').data('id');

            if (role == "Admin") 
            {
                //alert(role);
                $("div#edit-user select#role").val('A').trigger('change');
            }
            else if (role == "Sub Admin") 
            {
                //alert(role);
                $("div#edit-user select#role").val('S').trigger('change');
            }
            
            $("div#edit-user input#username").val(name);
            $("div#edit-user input#email").val(email);

            var action = $("form#edituser").data('action');
            action = action+"/"+id;
            $("form#edituser").attr('action',action);
            $("div#edit-user").modal('show');
        });

        $("button#update-user").on('click',function(){
            $('div#user-edit-error').hide();
            
            var name = $('div#edit-user input#username').val();

            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.user_required')');
            }
            if(!name.match(/^[0-9 a-z A-Z]+$/))
            {
                errors.push('@lang('validation.user_numeric')');
            }

            if (errors.length) 
            {
                $('div#user-edit-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#user-edit-error').append("<li>"+errors[i]+"</li>");
                    $('div#user-edit-error').show().delay(10000).fadeOut(3000);
                }
            }
            else
            {
                $('form#edituser').submit();
            }
        });


        $(users).on('click',"a.delete-user",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.user_delete')",
                text: "@lang('validation.user_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
        $(users).on('click',"a.block-user",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.user_block')",
                text: "@lang('validation.user_block_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.block_yes')",
                cancelButtonText: "@lang('validation.block_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
        $(users).on('click',"a.unblock-user",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.user_unblock')",
                text: "@lang('validation.user_unblock_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.unblock_yes')",
                cancelButtonText: "@lang('validation.unblock_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
        }
        return pass;
    }

    function generate() {
        adduser.password.value = randomPassword(adduser.length.value);
    }
</script>
@endpush