<?php
    // dd(request()->userid);
?>
@extends('layouts.app')
@section('title'){{config('app.name')." | Documents"}}@endsection

@section('content')

<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.all_reports')</h4></h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">    
                    <div class="filter">
                        <div class="row">
                            {{-- <form name="searchdoc" action="{{ route('report.search') }}"> --}}
                            <div class="alert alert-danger" id="search-error" style="display: none"></div>
                            <div class="offset-md-3 col-md-2">
                                <div class="form-group">
                                    <label for="userid">@lang('label.select_user')</label>
                                    <select name="userid" id="userid" class="form-control input-sm">
                                        <option value="">@lang('label.select_user')</option>
                                        @foreach($users as $user)
                                            @if(isset(request()->userid))
                                                @if(request()->userid == $user['id'])
                                                    <option value="{{ $user['id'] }}" selected="">{{ $user['name'] }}</option>
                                                @else
                                                    <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>
                                                @endif    
                                            @else
                                                <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>
                                            @endif
                                        @endforeach    
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 p-0">
                                <div class="form-group">
                                    <label for="search_text">@lang('label.search_doc')</label>
                                    <input type="text" name="search_text" id="search_text" class="form-control input-sm" placeholder="@lang('label.search_doc')" value="{{isset($document)?$document->title:""}}">
                                    @if(isset(request()->docid))    
                                        <input type="hidden" id="searchdocid" value="{{ request()->docid }}">
                                    @else
                                        <input type="hidden" id="searchdocid" value="">
                                    @endif        
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="startdate">@lang('label.start_date')</label>
                                <div class='input-group'>
                                    <input type="date" class="form-control input-sm" name="startdate" id="startdate" placeholder="Start Date">
                                    {{-- <input type='text' class="form-control daterange input-sm" value="" placeholder="MM/DD/YYYY" id="searchtime" /> --}}
                                </div>
                            </div>  
                            <div class="col-md-2">
                                <label for="enddate">@lang('label.end_date')</label>
                                <div class='input-group'>
                                    <input type="date" class="form-control input-sm" name="enddate" id="enddate" placeholder="End Date">
                                </div>
                            </div>  
                            <div class="col-md-1">
                                 <label for="searchform">&nbsp;</label>
                                <div class='input-group'>
                                    <button id="searchform" class="btn btn-theme-orange btn-sm">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                            </div>
                            {{-- </form>  --}}         
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="loader" style="display: none;"><img src="{{ url('/') }}/web/images/Loading_icon.gif"></div> --}}
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="auditreport">
                    <thead>
                        <tr>
                            <th>Datum</th>
                            <th>@lang('label.user_name')</th>
                            <th>@lang('label.user_emailid')</th>
                            <th>@lang('label.document')</th>
                            <th>@lang('label.document_action')</th>
                        </tr>
                    </thead>
                    <tbody id="searchdata">
                        
                    </tbody>
                    <tr><td colspan="5" style="text-align: center;"><div class="loader" style="display: none;"><img src="{{ url('/') }}/web/images/Loading_icon.gif"></div></td></tr>
                   {{--  
                     <tbody>
                        <tr>
                            <td>07/05/2018</td>
                            <td>Micky Arthur</td>
                            <td>micky@gmail.com</td>
                            <td><a href="https://archifox.s3.eu-central-1.amazonaws.com/one.pdf" class="link-orange" target="_blank">One</a></td>
                            <td>Document Created</td>
                        </tr>
                    </tbody> --}} 
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/css/plugins/pickers/daterange/daterange.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
<!-- <script src="{{ url('/') }}/app-assets/js/scripts/pickers/dateTime/picker-date-time.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script> -->
<script src="{{ url('/') }}/app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
        //var userDataTable = $('#auditreport').DataTable({bFilter:false, bInfo: false, ordering: false, bLengthChange: false});

        $( '#searchtime' ).daterangepicker();
        //search autocomplete 
        src = "{{ route('report.autocomplete') }}";
        $("#search_text").autocomplete({
            source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
            minLength: 1,
            select: function( event, ui ) {
                    //console.log("rachna");
                var searchtext=$('input#search_text').val();
                //alert(searchtext);
                if(!searchtext){
                  $( "#searchdocid" ).empty();
                }
                  $(this).val( ui.item.value );
                  $( "#searchdocid" ).val( ui.item.id );
                  return false;
               },
               change: function(event, ui) {
                    console.log(this.value);
                    if (ui.item == null) {
                        // console.log("new item: " + this.value);
                        $( "#searchdocid" ).val("");

                    } else {
                        // console.log("selected item: " + ui.item.value);
                    }
                }

        });

        $("#searchform").click(function() {
            var searchtext=$('input#search_text').val();
            
            $("#searchdata").empty();
            var searchdocid = $('input#searchdocid').val();
            var usersearch = $('select#userid').val();
            var startdate = $('input#startdate').val();
            var enddate = $('input#enddate').val();

            var errors = [];
            if(!startdate && enddate!="")
            {
                errors.push('@lang('validation.start_date_required')');
            }
            if(startdate!="" && !enddate)
            {
                errors.push('@lang('validation.end_date_required')');
            }
            if(startdate!="" && enddate!="" && startdate > enddate)
            {
                errors.push('@lang('validation.start_less_end')');
            }
            if (!searchdocid && !usersearch && !startdate && !enddate) 
            {
                errors.push('@lang('validation.onefield_required')');
            }
            if (errors.length) 
            {
                $('div#search-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#search-error').append("<li>"+errors[i]+"</li>");
                    $('div#search-error').show().delay(10000).fadeOut(3000);
                }
            }
            else
            {
                $(".loader").show();
                $.ajax({
                    url: "{{route('report.searchuserdoc')}}",
                    dataType: "json",
                    data: {
                        searchdocid:searchdocid,usersearch:usersearch,startdate:startdate,enddate:enddate
                    },
                    type: "post",
                    success: function(data) {
                        $('.loader').hide();

                        console.log('Success!', data);
                        $("#searchdata").empty();
                        //$('#searchdata').append(data);
                        var len = data.length;
                        if(len!=0) {
                            for(var i=0; i<len; i++){                       
                                var user = data[i].user;
                                var doc = data[i].document;
                                
                                var action_time = data[i].action_time;
                                var action = data[i].action;
                                if(action == 'U'){
                                    var action = 'Updated';
                                }
                                if(action == 'C'){
                                    var action = 'Created';
                                }
                                if(action == 'D'){
                                    var action = 'Deleted';
                                }
                                var tr_str = "<tr>" +
                                    "<td>" + action_time + "</td>" +
                                    "<td>" + user.name + "</td>" +
                                    "<td>" + user.email + "</td>" +
                                    "<td>" + doc.title + "</td>" +
                                    "<td>" + action + "</td>" +
                                    "</tr>";

                                $("#searchdata").append(tr_str);
                            }
                        }
                        else {
                            var no_str = "<td colspan=5 style='text-align:center;font-size:25px;'><strong>"+'Keine Daten gefunden...'+"</strong></td>";
                            $("#searchdata").append(no_str);
                        }
                    }
                });
            }
            });
        


        @if(isset(request()->userid))
            $("#searchform").trigger('click');
        @endif
        @if(isset(request()->docid))
            $("#searchform").trigger('click');
        @endif
    });
</script>
@endpush