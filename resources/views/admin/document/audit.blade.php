@extends('layouts.app')
@section('title'){{config('app.name')." | Document Audit"}}@endsection

@section('content')

<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.doc_audit')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <a href="{{ route('document.index') }}" class="btn btn-theme-orange"><i class="icon-arrow-left white"></i>  @lang('label.back')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.doc_name'):</strong></div>
                            <div class="col-lg-10 col-lg-9">{{ $documentdata['title'] }}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.description'):</strong></div>
                            <div class="col-lg-10 col-lg-9">{{ str_limit($documentdata['description'],50) }}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.doc_cat'):</strong></div>
                            <div class="col-lg-10 col-lg-9">
                                @foreach($categories as $category)
                                @if($category['id'] == $documentdata['categories'])
                                    {{ $category['name'] }}
                                @endif    
                            @endforeach</span></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-3"><strong>@lang('label.keyword'):</strong></div>
                            <div class="col-lg-10 col-lg-9">
                                @foreach ($keywords as $keyword)
                                    @if (strpos($documentdata->keywords, $keyword->name) !== false)
                                        <span class="tag tag-theme-orange round">{{$keyword['name']}}</span>
                                    @endif
                                @endforeach 
                            </div>
                        </div>
                        <hr>
                        <div class="document-uploaded-view pt-1">
                            <div class="row">
                                @if($documentdata['public_url']!="")
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card border-grey border-lighten-2">
                                            <div class="card-block p-1 card-custom-height">
                                                <h5 class="link-darkblue text-bold-500">PDF</h5>
                                                <hr class="mt-0">
                                                <div class="mb-1">
                                                    <a href="{{ $documentdata['public_url'] }}" target="_blank">
                                                        <embed src="{{ $documentdata['public_url'] }}" style="" type="application/pdf" width="100%" />
                                                    </a>
                                                </div>
                                                {{-- <button class="btn btn-theme-orange btn-sm" onClick="download('{{ $documentdata['public_url'] }}')">Download</button> --}}
                                                 <a target="_blank" class="btn btn-theme-orange btn-sm" href="{{ $documentdata['public_url'] }}" download>Download</a> 
                                            </div>
                                        </div>
                                    </div>
                                @endif    
                                @if($documentdata['audio_url']!="")    
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card border-grey border-lighten-2">
                                            <div class="card-block p-1 card-custom-height">
                                                <h5 class="link-darkblue text-bold-500">AUDIO</h5>
                                                <hr clas="mt-0">
                                                <img src="../../../app-assets/images/music-img.png" class="img-fluid mb-1" alt="">
                                                <div class="audio">
                                                    <audio controls class="w-100">
                                                        <source src="{{ $documentdata['audio_url'] }}" type="audio/mp3">
                                                    </audio>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif    
                                <!--</div>

                                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="card border-grey border-lighten-2">
                                        <div class="card-block p-1 card-custom-height">
                                            <h5 class="link-darkblue text-bold-500">VIDEO</h5>
                                            <hr clas="mt-0">
                                            <div class="video">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/XQFb12N0Arc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
                                    
                                @if($documentdata['video_url']!="")    
                                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card border-grey border-lighten-2">
                                            <div class="card-block p-1 card-custom-height">
                                                <h5 class="link-darkblue text-bold-500">VIDEO</h5>
                                                <hr clas="mt-0">
                                                <div class="video">
                                                    
                                                    <input type="hidden" name="videobox" id="videobox" value="{{$documentdata['video_url']}}">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                         <div id="video-preview"></div>
                                                        {{-- <iframe class="embed-responsive-item" src="http://www.dailymotion.com/409772c0-9540-4760-a762-ca1397fbad96" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            @if($documentdata['public_url']!="")
                                <th><a href="{{ $documentdata['public_url'] }}" class="link-orange" target="_blank">Open PDF</a></th>
                            @endif    
                            @if($documentdata['audio_url']!="")    
                                <th><a href="{{ $documentdata['audio_url'] }}" class="link-orange" target="_blank">Run Audio</a></th>
                            @endif    
                            @if($documentdata['video_url']!="")    
                                <th><a href="{{ $documentdata['video_url'] }}" class="link-orange" target="_blank">View Video</a></th>
                            @endif    
                        </tr>
                    </thead>
                    <tbody> -->
                        <!-- <tr>
                            <td>{{ $documentdata['title'] }}</td>
                            <td>
                                <?php foreach($userdocs as $userdoc){
                                        if($userdoc['id'] == $documentdata['created_by']){
                                                echo $userdoc['name'];
                                            }
                                        }
                                ?>
                            </td>
                            <td>{{ $documentdata['created_at'] }}</td>
                        </tr> -->
                    <!-- </tbody>
                </table>
            </div> -->
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('label.recent_act')</h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @if($docaudit != NULL)
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>@lang('label.doc_action')</th>
                                        <th>@lang('label.doc_time')</th>
                                        <th>@lang('label.doc_actionby')</th>
                                        <th>@lang('label.user_ipaddress')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($docaudit as $docaudits)
                                    <tr>
                                        <td>
                                            @if($docaudits['action'] == 'U')
                                                {{'Updated'}}
                                            @elseif($docaudits['action'] == 'C')    
                                                {{'Created'}}
                                            @endif    
                                        </td>
                                    
                                        <td>
                                            {{$docaudits['action_time']}}
                                        </td>
                                        <td>
                                            @foreach($userdocs as $userdoc)
                                                @if($userdoc['id'] == $docaudits['user_id'])
                                                    <a class="link-orange" href="{{ route('user.show',['id' => $userdoc['id']]) }}">{{ $userdoc['name'] }}</a>
                                                @endif
                                            @endforeach
                                            
                                        </td>
                                    
                                        <td>
                                            {{$docaudits['user_ipaddress']}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                        <div class="float-xs-right">
                            <a class="btn btn-theme-orange" href="{{ route('report.index',['docid' => $documentdata['id']]) }}">@lang('label.view_more')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/css/plugins/forms/validation/form-validation.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
         var url =$('input#videobox').val(); 
         //alert(url);
         var ifrm = document.createElement('iframe');
         ifrm.src = (!url.includes('vimeo')) ? "//www.youtube.com/embed/"+ url.split("=")[1] : "//player.vimeo.com/video/"+ url.split("/")[3];
         ifrm.width= "560";
         ifrm.height = "560";
         ifrm.frameborder="0";
         ifrm.scrolling="no";
         $('#video-preview').html(ifrm);
         videobox.style.display = "block";
    });
    /*function download(file)
{
        alert(file);
 window.location=file;
}*/
</script>
@endpush
    