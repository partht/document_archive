@extends('layouts.app')
@section('title'){{config('app.name')." | Documents"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
@if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.document')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <a href="{{ route('document.add') }}" class="btn btn-theme-orange"><i class="icon-plus4 white"></i> @lang('label.add_document')</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table id="documents" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th width="20%">@lang('label.name')</th>
                        <th width="10%">@lang('label.search_count')</th>
                        <th width="20%">@lang('label.description')</th>
                        <th width="15%">@lang('label.categories')</th>
                        <th width="15%">@lang('label.keyword')</th>
                        <th>@lang('label.option')</th>
                    </tr>
                </thead>
                <tbody id="doc_tbody">
                    @foreach($documents as $document)
                    <tr data-id="{{ $document }}">
                        <td>
                            <div class="media">
                                <div class="media-body media-middle">
                                    <!--<a target="_blank" href="{{$document->public_url}}" class="media-heading link-orange">--> <span class="name">{{ $document['title'] }}</span><!--</a>-->
                                </div>
                            </div>
                        </td>
                        <td>
                            {{ $document['search_count'] }}
                        </td>
                        <td>
                            {{ str_limit($document['description'],30) }}
                        </td>
                        <td>
                            @foreach($categorydata as $category)
                                @if($category['id'] == $document['categories'])
                                    {{ $category['name'] }}
                                @endif    
                            @endforeach    
                        </td>
                        <td>
                            {{ $document['keywords'] }}
                        </td>
                        <td>
                            <div class="btn-group cursor-pointer pull-right">
                                <!--<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-theme-darkblue dropdown-toggle dropdown-menu-right"><i class="icon-cog3"></i></button>
                                <span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">-->
                                    
                                    <a href="{{route('document.showaudit',['id' => $document['id']])}}" id="audit-document" class="text-info btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Ansehen"><i class="icon-eye3"></i></i></a>
                                    <a href="{{route('document.edit',['id' => $document['id']])}}" id="edit-document" class="link-orange btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Bearbeiten"><i class="icon-pencil2"></i></a>
                                    <a href="#" data-href="{{ route('document.delete',['id' => $document['id']]) }}" class="delete-document text-danger btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" data-original-title="Löschen"><i class="icon-trash-b"></i></a>
                                <!--</span>-->
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js" type="text/javascript"></script> 
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var userDataTable = $('#documents').DataTable({responsive: true, order: [],columnDefs: [ { orderable: false, targets: [0,1,2,3,4,5] } ],"bStateSave": true,
            "language": {
                "info": "Zeige _START_ bis _END_ von _TOTAL_ Einträge",
                "infoEmpty": "Zeige 0 bis _END_ von _TOTAL_ Einträge",
                "emptyTable": "Keine Daten in der Tabelle verfügbar",
                "lengthMenu": "Einträge _MENU_ anzeigen",
                "search": "Suche",
                "paginate": {
                  "previous": "zurück",
                  "next": "weiter"
                }
            }    });

        /*$('button#add-category').on('click',function(){
            $("div#add-category").modal('show');
        });

        if ($("input#open-add").length) 
        {
            $("button#add-category").trigger('click');
        }


        $("select#parent-category").select2();

        $("button#save-category").on('click',function(){
            $('div#category-add-error').hide();
            var parent_id = $('select#parent-category option:selected').val();
            var name = $('input#category-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('Category Name is a required field.');
            }

            if (errors.length) 
            {
                $('div#category-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#category-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#category-add-error').show();
                }
            }
            else
            {
                $('form#add-category-form').submit();
            }
        });

        $(document).on('click','a#edit-category',function()
        {   
            var name = $(this).closest('tr').find('span.name').html();
            var id = $(this).closest('tr').data('id');
            var parent_id = $(this).closest('tr').data('parent-id');
            $("div#edit-category select#parent-category").val(parent_id).trigger('change');
            $("div#edit-category input#category-name").val(name);

            var action = $("form#edit-category-form").data('action');
            action = action+"/"+id;
            $("form#edit-category-form").attr('action',action);
            $("div#edit-category").modal('show');
        });

        $("button#update-category").on('click',function(){
            $('div#category-edit-error').hide();
            var parent_id = $('div#edit-category select#parent-category option:selected').val();
            var name = $('div#edit-category input#category-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('Category Name is a required field.');
            }

            if (errors.length) 
            {
                $('div#category-edit-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#category-edit-error').append("<li>"+errors[i]+"</li>");
                    $('div#category-edit-error').show();
                }
            }
            else
            {
                $('form#edit-category-form').submit();
            }
        });
        */

        $('#doc_tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });

        $(document).on('click',"a.delete-document",function(){
            var link = $(this).data('href');
            swal({
                title: "@lang('validation.doc_delete')",
                text: "@lang('validation.doc_delete_confirm')",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "@lang('validation.delete_yes')",
                cancelButtonText: "@lang('validation.delete_no')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } 
            });
        });
    });
</script>
@endpush