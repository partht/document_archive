<?php 

    //dd(request()->route()->getName());
 ?>
@extends('layouts.app')
@section('title'){{config('app.name')." | Documents Add"}}@endsection

@section('content')
@if (Session::has('success_msg'))
    <div class="alert alert-success">
        <li>{{ Session::get('success_msg') }}</li>
    </div> 
@endif
@if (Session::has('delete_msg'))
    <div class="alert alert-danger">
        <li>{{ Session::get('delete_msg') }}</li>
    </div> 
@endif
@if (Session::has('open_add'))
    <input type="hidden" id="open-add">
@endif
@if ($errors->any())
    <input type="hidden">
@endif
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.add_document')</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                {{-- <a href="{{ route('document.add') }}" class="btn btn-primary btn-sm"><i class="icon-plus4 white"></i> Add Document</a> --}}
            </div>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('document.store') }}" method="post" enctype="multipart/form-data" id="docformadd">
                        {{csrf_field()}}
                        @if ($errors->get('title') || $errors->get('pdf') ||$errors->get('keywords'))
                            <div class="alert alert-danger" id="docerror">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="alert alert-danger" id="add-error" style="display: none"></div>

                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="title">@lang('label.title')</label>
                            <div class="col-md-6">
                                <input class="form-control" maxlength="30" type="text" placeholder="Titel eingeben" id="title" name="title" required data-validation-required-message="Title is required." value="{{old('title')}}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="description">@lang('label.description')</label>
                            <div class="col-md-6">
                                <textarea class="form-control" maxlength="300" placeholder="@lang('label.enter_description_here')" id="description" name="description" value="{{old('description')}}"></textarea>
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="categories">@lang('label.categories')</label>
                            <div class="col-md-6">
                                <select name="categories" id="categories" style="width: 100%;" class="select2" required>
                                    <option value="">@lang('label.select_cat')</option>
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            <div class="help-block"></div>
                            </div>
                            <a href="#" id="add-category" class="btn btn-theme-orange"><i class="icon-plus"></i></a>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="keywords">@lang('label.keyword')</label>
                            <div class="col-md-6">
                                <select name="keywords[]" id="keywords" multiple="" style="width: 100%;" required>
                                    @foreach ($keywords as $keyword)
                                        <option value="{{$keyword->name}}">{{$keyword->name}}</option>
                                    @endforeach
                                </select>
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="video_url">@lang('label.video_url')</label>
                            <div class="col-md-6">
                                <input class="form-control" type="url" pattern="https?://.+" placeholder="Enter Video URL" id="video_url" name="video_url" value="{{old('video_url')}}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="audio">@lang('label.audio_url')</label>
                            <div class="col-md-6">
                                <input class="form-control" type="file" id="audio" name="audio" accept=".mp3,.wav,.ogg,.flac,.mpeg" value="{{old('audio')}}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="pdf">@lang('label.upload_pdf')</label>
                            <div class="col-md-6">
                                <input class="form-control" type="file" id="pdf" name="pdf" accept=".pdf" value="{{old('pdf')}}">
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="timerange">@lang('label.doc_timerange')</label>
                            <div class="col-md-6">
                                <select class="select2" name="timerange" id="timerange" style="width: 100%;" required>
                                    <option value="0">@lang('label.select_time_range')</option>
                                    @foreach ($timerange as $timerange)
                                        <option value="{{$timerange->id}}">{{$timerange->yearrange}}</option>
                                    @endforeach
                                </select>
                            <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="docaddress">@lang('label.doc_address')</label>
                            <div class="col-md-6">
                                <input id="autocomplete" placeholder="Type your address" onFocus="geolocate()" name="docaddress" class="form-control" type="text" value="{{ old('google_address') }}">
                                <input type="hidden" name="address" id="address" class="form-control" value="">
                                <input type="hidden" name="lat" id="lat" class="form-control" value="">
                                <input type="hidden" name="lng" id="lng" class="form-control" value="">
                                <input type="hidden" name="city" id="city" class="form-control" value="">
                                <input type="hidden" name="state" id="state" class="form-control" value="">
                                <input type="hidden" name="country" id="country" class="form-control" value="">
                                <div class="help-block"></div>
                                <div id="map-canvas"></div>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <a href="{{route('document.index')}}" class="btn btn-theme-darkblue">
                                <i class="icon-cross2"></i> @lang('label.cancel')
                            </a>
                            <button id="saveform" type="button" class="btn btn-theme-orange">
                                <i class="icon-check2"></i> @lang('label.save')
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Category Modal -->

@if ($errors->get('name'))
    <input type="hidden" id="open-add">
@endif
<div class="modal fade text-xs-left" id="add-category" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">@lang('label.add_category')</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('category.store')}}" method="post" id="add-category-form"> 
                    {{ csrf_field() }}
                    @if ($errors->any())
                        <div class="alert alert-danger" id="caterror">
                            <ul>
                                @foreach ($errors->get('name') as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="routename" value={{request()->route()->getName()}}>
                    <div class="alert alert-danger" id="category-add-error" style="display: none"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="parent-category">@lang('label.parent_category') : <span class="danger">*</span></label>
                            <!-- <input type="text" id="parent-category" class="form-control" placeholder="First Name"> -->
                            <select id="parent-category" class="select2" name="parent_id" style="width: 100%;">
                                <option value="0">--- Kein Elternteil ---</option>
                                @foreach ($categories as $category)
                                    <option data-depth="{{ $category['depth'] }}" value="{{ $category['id'] }}">@for ($i = 0; $i < $category['depth']; $i++){{"—"}}@endfor{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="category-name">@lang('label.category_name') : <span class="danger">*</span></label>
                            <input name="name" maxlength="15" type="text" id="category-name" class="form-control" placeholder="Category Name" value="{{old('name')}}">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-theme-darkblue" data-dismiss="modal">@lang('label.close')</button>
                <button type="button" class="btn btn-theme-orange" id="save-category">@lang('label.add_category')</button>
            </div>
        </div>
    </div>
</div>
<!-- END Add Category Modal -->

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/app-assets/css/plugins/forms/validation/form-validation.css">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/forms/selects/select2.min.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
<!--Google Map Script-->
<script type="text/javascript">
  var placeSearch, autocomplete;
  var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
  };

  function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    autocomplete.addListener('place_changed', fillInAddress);
    // console.log(autocomplete);
  }

  function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();

    $('input#lat').val(lat);
    $('input#lng').val(lng);
    codeLatLng(lat, lng);
  }

  function codeLatLng(lat, lng) {
    var geocoder= new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
         //formatted address
         // alert(results[0].formatted_address)
        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

                if (results[0].address_components[i].types[0] == "locality") {
                        //this is the object you are looking for
                        city = results[0].address_components[i];
                }
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    region= results[0].address_components[i];
                }
                if (results[0].address_components[i].types[0] == "country") {
                        //this is the object you are looking for
                        country = results[0].address_components[i];
                }
            }
        }

        $('input#city').val(city.long_name);
        $('input#state').val(region.long_name);
        $('input#country').val(country.long_name);
        } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
    var address = $('input#autocomplete').val();
    codeAddress(address);
  }

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        console.log(position);
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }
</script>

<script type="text/javascript">
    // function to geocode an address and plot it on a map
    function codeAddress(address) {
        geocoder = new google.maps.Geocoder();                          // create geocoder object
        var latlng = new google.maps.LatLng(40.6700, -73.9400);         // set default lat/long (new york city)
        var mapOptions = {                                              // options for map
            zoom: 12,
            center: latlng
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);   // create new map in the map-canvas div
        $('#map-canvas').addClass('height-300 width-100-per map-height');
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);            // center the map on address
                var marker = new google.maps.Marker({                   // place a marker on the map at the address
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("input,textarea").not("[type=submit]").jqBootstrapValidation(); 
        
        $("select#keywords").select2({
            tags: true
        });
        $("select#categories").select2();
        $("select#timerange").select2();
        
    /*on close of popup, form data and validation message clear*/
        $(".modal").on("hidden.bs.modal", function(){
            $("#add-category-form")[0].reset();
            $('div#category-add-error').hide();
            $('div#caterror').hide();
            $('div#caterror').empty();
        });
    /*on close of popup, form data and validation message clear*/

        $("#saveform").mousedown(function(e){
            var title = $('input#title').val();
            var categories = $('select#categories').val();
            var video_url = $('input#video_url').val();
            var audio = $('input#audio').val();
            var pdf = $('input#pdf').val();
            var address = $('input#autocomplete').val();
            var lat = $('input#lat').val();
            var lng = $('input#lng').val();
            var city = $('input#city').val()
            var state = $('input#state').val()
            var country = $('input#country').val()
            
            var errors = [];
            if(address!="" && lat=="")
            {
                errors.push('@lang('validation.doc_address_valid')');
            }
            if (!video_url && !audio && !pdf) 
            {
                errors.push('@lang('validation.video_audio_pdf_req')');
            }

            if (!title) 
            {
                errors.push('@lang('validation.doc_title_req')');
            }
            if (!categories) 
            {
                errors.push('@lang('validation.cat_required')');
            }
            if (audio) 
            {
                var audio_extentions = [".mp3",".wav",".ogg",".flac",".mpeg"];  
                var blnValid = false;
                for (var j = 0; j < audio_extentions.length; j++) 
                {
                    var sCurExtension = audio_extentions[j];
                    if (audio.substr(audio.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }    

                if (!blnValid) 
                {
                    errors.push('Audio format incorrect. Please upload any one of ('+audio_extentions.join(', ')+") files.");
                }
            }

            if (pdf) 
            {
                var pdf_extentions = [".pdf"];  
                var pdfValid = false;
                for (var j = 0; j < pdf_extentions.length; j++) 
                {
                    var sCurExtension = pdf_extentions[j];
                    if (pdf.substr(pdf.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        pdfValid = true;
                        break;
                    }
                }    

                if (!pdfValid) 
                {
                    errors.push('Pdf format incorrect. Please upload any one of ('+pdf_extentions.join(', ')+") files.");
                }
            }

            if (errors.length) 
            {
                $('div#add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#add-error').append("<li>"+errors[i]+"</li>");
                    $('div#add-error').show().delay(10000).fadeOut(3000);
                }
            }
            else
            {
                $('form#docformadd').submit();
                $(".loader-wrapper").show();
            }
        });

        /*var $inputs = $('input[name=video_url],input[name=pdf]');
        $inputs.on('input', function () {
        // Set the required property of the other input to false if this input is not empty.
            $inputs.not(this).prop('required', !$(this).val().length);
        });*/

        $('a#add-category').on('click',function(){
            $("div#add-category").modal('show');
        });

        if ($("input#open-add").length) 
        {
            $("a#add-category").trigger('click');
        }


        $("select#parent-category").select2();

        $("button#save-category").on('click',function(){
            $('div#category-add-error').hide();
            var parent_id = $('select#parent-category option:selected').val();
            var name = $('input#category-name').val();
            var errors = [];
            if (!name) 
            {
                errors.push('@lang('validation.cat_required')');
            }
            if(!name.match(/^[0-9a-zA-Z]+$/)) 
            {
                errors.push('@lang('validation.cat_numeric')');    
            }

            if (errors.length) 
            {
                $('div#category-add-error').empty();
                for (var i = 0; i < errors.length; i++) 
                {
                    $('div#category-add-error').append("<li>"+errors[i]+"</li>");
                    $('div#category-add-error').show();
                }
            }
            else
            {
                $('form#add-category-form').submit();
            }
        });
    });
</script>
<!--Google Map API-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYZm76EEQ2ve_b580TZ-3mW6mn043qnMo&libraries=places&callback=initAutocomplete" async defer></script>
@endpush