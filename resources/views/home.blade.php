@extends('layouts.app')
@section('title')
{{config('app.name')." | Dashboard"}}
@endsection
@php
    //dd(route('home'));
@endphp 
@section('content')
@if(Session::has('success_msg'))
<div class="alert alert-success">{{ Session::get('success_msg') }}</div>
@endif
<div class="card">
    <div class="card-body">
        <div class="card-block">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                    <div class="media px-1">
                        <div class="media-left media-middle">
                            <i class="icon-search4 font-large-1 blue-grey"></i>
                        </div>
                        <div class="media-body text-xs-right">
                            <span class="font-large-2 text-bold-300 red">{{ $documents->sum('search_count') }}</span>
                        </div>
                        <p class="text-muted">@lang('label.total_search')<span class="info float-xs-right"><!-- <i class="icon-arrow-up4 info"></i> 16.89% --></span></p>
                        <progress class="progress progress-sm progress-red" value="80" max="100"></progress>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                    <div class="media px-1">
                        <div class="media-left media-middle">
                            <i class="icon-tag font-large-1 blue-grey"></i>
                        </div>
                        <div class="media-body text-xs-right">
                            <span class="font-large-2 text-bold-300 info">{{ $categories->count() }}</span>
                        </div>
                        <p class="text-muted">@lang('label.total_categories') <span class="info float-xs-right"><!-- <i class="icon-arrow-up4 info"></i> 16.89% --></span></p>
                        <progress class="progress progress-sm progress-info" value="80" max="100"></progress>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                    <div class="media px-1">
                        <div class="media-left media-middle">
                            <i class="icon-document-text font-large-1 blue-grey"></i>
                        </div>
                        <div class="media-body text-xs-right">
                            <span class="font-large-2 text-bold-300 deep-orange">{{ $documents->count() }}</span>
                        </div>
                        <p class="text-muted">@lang('label.total_documents')<span class="deep-orange float-xs-right"><!-- <i class="icon-arrow-up4 deep-orange"></i> 5.14% --></span></p>
                        <progress class="progress progress-sm progress-deep-orange" value="45" max="100"></progress>
                    </div>
                </div>
                @if(checkPermission(['admin']))
                <div class="col-xl-3 col-lg-6 col-sm-12">
                    <div class="media px-1">
                        <div class="media-left media-middle">
                            <i class="icon-users2 font-large-1 blue-grey"></i>
                        </div>
                        <div class="media-body text-xs-right">
                            <span class="font-large-2 text-bold-300 success">{{ $users->count() }}</span>
                        </div>
                        <p class="text-muted">@lang('label.total_users')<span class="success float-xs-right"><!-- <i class="icon-arrow-up4 success"></i> 43.84% --></span></p>
                        <progress class="progress progress-sm progress-success" value="60" max="100"></progress>
                    </div>
                </div>
                @endif
            </div>
            
        </div>
    </div>
</div>
<div class="card">
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.cat_numbers')</h4>                        
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div style="overflow-x: auto;">
                <div id="chart-container">
                    <canvas id="mycanvas"></canvas>            
                </div>
            </div>
        </div>
    </div>
</div>  
<div class="card">
    <div class="alert alert-danger" id="search-error" style="display: none"></div>
    <div class="card-head">
        <div class="card-header">
            <h4 class="card-title">@lang('label.top_search_docs')
                <div class="datepicker-filter">                
                    <div id="reportrange">
                        <i class="icon-calendar4"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </h4>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>@lang('label.doc_name')</th>
                            <th>@lang('label.search_count')</th>
                        </tr>
                    </thead>
                    <tbody id="searchdocs">
                        {{-- @foreach($searched_docs as $docs)
                        <tr>
                            <td><a class="link-orange" href="{{ route('document.showaudit',['id' => $docs['id']]) }}">{{$docs['title']}}</a></td>
                            <td>{{$docs['search_count']}}</td>
                        </tr>
                        @endforeach --}}
                    </tbody>    
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/app-assets/js/scripts/Chart.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
    $.ajax({
        url: "{{route('home.catdoc_count')}}",
        type: "get",
        success: function(data) {
            console.log('Success!',data);
            var name = [];
            var docs_count = [];

            for(var i in data) {
                name.push(data[i].name);
                docs_count.push(data[i].docs_count);
            }

            var chartdata = {
                labels: name,
                datasets : [
                    {
                        label: 'Kategorie Dokumentanzahl',
                        backgroundColor: '#dd5a21',
                        borderColor: '#dd5a21',
                        hoverBackgroundColor: '#dd5a21',
                        hoverBorderColor: '#dd5a21',
                        data: docs_count
                    }
                ]
            };

            var ctx = $("#mycanvas");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata,
                options: {
                    scaleShowValues: true,
                    scales: {
                    yAxes: [{
                    ticks: {
                    beginAtZero: true
                    }
                    }],
                    xAxes: [{
                    ticks: {
                    autoSkip: false
                    }
                    }]
                    }
                }
            });
        },
        error: function(data) {
            console.log(data);
        }
    });

    $("div#reportrange span").bind('DOMSubtreeModified', function() {
        $("#searchdocs").empty();
        var timefilter = $('#reportrange span').html();
        var timerange = timefilter.split("-");
        var startdate = timerange[0];
        var enddate = timerange[1];
        
        $.ajax({
            url: "{{route('home.searchdoc_count')}}",
            dataType: "json",
            data: {
                startdate:startdate,enddate:enddate
            },
            type: "post",
            success: function(data) {
                console.log(data);
                $("#searchdocs").empty();
                var len = data.length;
                if(len!=0) {
                    for(var i=0; i<len; i++){
                        var id = data[i].id;                       
                        var title = data[i].title;
                        var search_count = data[i].search_count;
                        var tr_str = "<tr>" +
                            "<td>" + title + "</td>" +
                            "<td>" + search_count + "</td>" +
                            "</tr>";

                        $("#searchdocs").append(tr_str);
                    }
                }
                else {
                    var no_str = "<td colspan=5 style='text-align:center;font-size:25px;'><strong>"+'Keine Daten gefunden...'+"</strong></td>";
                    $("#searchdocs").append(no_str);
                }
            }
        });
    });
});
</script>

<!-- datepicker filter -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        locale: {"customRangeLabel": "Benutzerdefiniertes Datum",},
        ranges: {
           'Heute': [moment(), moment()],
           'Gestern': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Letzten 7 Tage': [moment().subtract(6, 'days'), moment()],
           'Letzten 30 Tage': [moment().subtract(29, 'days'), moment()],
           'Diesen Monat': [moment().startOf('month'), moment().endOf('month')],
           'Im vergangenen Monat': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
@endpush
