@extends('layouts.app-default')

@section('content')
<?php //dd("password".$pass);?>
<div class="alert alert-danger" style="display: none;" id="validation_msg"></div>
       
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class='form animated bounceIn'>
            <img class="img-responsive login-logo" src="{{ url('/') }}/app-assets/images/logo-archifox.png">    
            <!-- <h2>Login</h2> -->
            
<!--<div class="card">
    <div class="card-header">-->
        <h2 class="login-title text-uppercase"><span>@lang('general.change_password')</span></h2> 
    <!--</div>-->
    <div class="card-body">
        <div class="card-block">
           
            <div class="row">
                <div class="col-md-12">
                     <form method="post" action="{{ route('updatenew_password') }}">
                       <div class="form-group">
                       <input type="text" name="email" hidden="" value="{{$user->email}}" class="email">
                            <!--<label class="col-md-2 label2-control required" for="projectinput1"> @lang('label.oldpassword') : </label>
                                <div class="col-md-12">-->
                                <input type="password" data-toggle="tolltip" title="password" placeholder="@lang('auth.old_password')" required class="form-control old" name="old_password" class="input-field">
                                <!--</div>-->
                        </div>
                        <div class="form-group">
                            <!--<label class="col-md-2 label2-control required" for="projectinput1"> @lang('label.newpassword') :&nbsp; </label>
                                <div class="col-md-12">-->
                                <input type="password" data-toggle="tolltip" title="New Password" placeholder="@lang('auth.new_password')" required class="form-control new" name="new_password"  required>
                                
                        </div>
                        
                        <div class="form-group">
                        <!--<label class="col-md-2 label2-control required" for="projectinput1"> @lang('label.conpassword') :&nbsp;</label>
                            <div class="col-md-12">-->
                                <input id="password" type="password" class="form-control con" name="con_password" placeholder="@lang('auth.cpassword')" required>
                        </div>

                        <div class="login-forgot">
                            <button type="button" id="check_validation" class="btn btn-primary"><i class="icon-check2"></i>@lang('general.save')</button>
                        </div>
                    </form> 

                    <!--<form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='Old Password' name="password" type='password' id="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='New Password' name="password" type='password' id="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='Change Password' name="password" type='password' id="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            
                <div class="login-forgot">
                    <button style="width: 100%" type="submit">Change Password</button>
                </div>                
            </form>-->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>

<script type="text/javascript">
 $(document).ready(function() {
    $('button#check_validation').on('click',function(e){
        e.preventDefault();

         var error_msg = $('div#validation_msg');
            $(error_msg).hide().empty();

            var old = $('input.old').val();
            var newpass = $('input.new').val();
            var conpass = $('input.con').val(); 
            var email = $('input.email').val();
            
            if(old == '' || newpass == '' || conpass == '')
            {
                $(error_msg).show().append('<li>Bitte füllen Sie alle Felder im Formular aus.</li>');
                        $("html, body").animate({scrollTop:0},"slow");
            }

            if(newpass != '')
            {
                if(newpass != conpass)
                {
                    $(error_msg).show().append('<li>Neues Passwort und Passwort bestätigen sind nicht gleich.</li>');
                            $("html, body").animate({scrollTop:0},"slow");
                }
                else
                {
                    $.ajax({  
                        url:"{{route('updatenew_password')}}",  
                        method:"POST",
                        data:{old_password:old,new_password:newpass,email:email},
                        dataType:"json",
                        success:function(data)
                        {
                            if(data.status == 0)
                            {
                                 $(error_msg).show().append('<li>Altes Passwort ist falsch, bitte passendes Passwort eingeben</li>');
                            $("html, body").animate({scrollTop:0},"slow");
                            }
                            else
                            {
                                window.location.href = "{{route('home')}}";
                            }
                        }  
                    });
                }
            }

    });
 });
    
</script>
@endpush