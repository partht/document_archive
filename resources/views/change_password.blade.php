@extends('layouts.app')
@section('title')
{{config('app.name')." | Change Password"}}
@endsection
@section('content')

@if(Session::has('success_msg'))
    <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
@endif
<div class="alert alert-danger" style="display: none;" id="validation_msg"></div>
       
<div class="card">
     <div class="card-header">
        <h4 class="card-title">@lang('general.change_password')</h4>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
    </div>
    <div class="card-body">
        <div class="card-block">
           
            <div class="row">
                <div class="col-md-12">
                    <form class="form form-horizontal" id="device_form" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-2 label2-control required" for="projectinput1"> @lang('auth.old_password') : </label>
                                <div class="col-md-6">
                                <input type="password" data-toggle="tolltip" title="password" placeholder="@lang('auth.old_password')" required class="form-control old" name="old_password">
                                </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-2 label2-control required" for="projectinput1"> @lang('auth.new_password') :&nbsp; </label>
                                <div class="col-md-6">
                                <input type="password" data-toggle="tolltip" title="New Password" placeholder="@lang('auth.new_password')" required class="form-control new" name="new_password"  required>
                                </div>
                        </div>
                        
                        <div class="form-group row">
                        <label class="col-md-2 label2-control required" for="projectinput1"> @lang('auth.cpassword') :&nbsp;</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control con" name="con_password" placeholder="@lang('auth.cpassword')" required>
                            </div>
                        </div>

                        <div class="form-actions">
                            
                            <a class="btn btn-theme-darkblue" href="{{ route('home') }}"><i class="icon-android-arrow-back"></i> @lang('general.back')</a>
                            <button type="button" id="check_validation" class="btn btn-theme-orange"><i class="icon-check2"></i> @lang('general.save')</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/vendors/css/extensions/sweetalert.css">
@endpush

@push('scripts')
<script src="{{url('/')}}/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>

<script type="text/javascript">
 $(document).ready(function() {
    $('button#check_validation').on('click',function(e){
        e.preventDefault();
            var error_msg = $('div#validation_msg');
            $(error_msg).hide().empty();

            var old = $('input.old').val();
            var newpass = $('input.new').val();
            var conpass = $('input.con').val(); 
            
            if(old == '' || newpass == '' || conpass == '')
            {
                $(error_msg).show().append('<li>Bitte füllen Sie alle Felder im Formular aus.</li>');
                        $("html, body").animate({scrollTop:0},"slow");
            }

            if(newpass != '')
            {
                if(newpass != conpass)
                {
                    $(error_msg).show().append('<li>Neues Passwort und Passwort bestätigen sind nicht gleich.</li>');
                            $("html, body").animate({scrollTop:0},"slow");
                }
                else
                {
                    $.ajax({  
                        url:"{{route('update_password')}}",  
                        method:"POST",
                        data:{old_password:old,new_password:newpass},
                        dataType:"json",
                        success:function(data)
                        {
                            if(data.status == 0)
                            {
                                swal("Altes Passwort ist falsch, bitte passendes Passwort eingeben");
                            }
                            else
                            {
                                window.location.href = "{{route('home')}}";
                            }
                        }  
                    });
                }
            }

    });
 });
    
</script>
@endpush