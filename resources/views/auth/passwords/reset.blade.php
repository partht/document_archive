@extends('layouts.app-default')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class='form animated bounceIn'>
            <img class="img-responsive login-logo" src="{{ url('/') }}/app-assets/images/logo-archifox.png">    
            <!-- <h2>Login</h2> -->
            <h2 class="login-title text-uppercase"><span>@lang('label.reset_password')</span></h2>
                <form method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            
                                <input id="email" placeholder='E-mail id' type="email" class="input-field email" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input placeholder='Password' id="password" type="password" class="input-field pass" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input placeholder='Confirm Password' id="password-confirm" type="password" class="input-field con" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="login-forgot">
                            <button style="width: 100%" type="submit" id="check_valid">
                                    @lang('label.reset_password')
                                </button>
                        </div>
                    </form> 
                
            </div>
        </div>
    </div>
</div>
@endsection
