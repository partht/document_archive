@extends('layouts.app-default')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class='form animated bounceIn'>
            <img class="img-responsive login-logo" src="{{ url('/') }}/app-assets/images/logo-archifox.png">    
            <!-- <h2>Login</h2> -->
            <h2 class="login-title text-uppercase"><span>@lang('label.reset_password')</span></h2>
                
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input placeholder='E-mail id' id="email" type="email" class="input-field" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="login-forgot">
                                <button style="width: 100%" type="submit">
                                    @lang('label.password_reset_link')
                                </button>
                        </div>
                    </form> 

                    <!--<form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='E-mail id' name="email" type='email' id="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="login-forgot">
                    <button style="width: 100%" type="submit">Send Password Reset Link</button>
                </div>                
            </form>-->
                
            </div>
        </div>
    </div>
</div>
@endsection
