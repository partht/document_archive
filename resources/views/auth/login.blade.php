@extends('layouts.app-default')

@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class='form animated bounceIn'>
            <img class="img-responsive login-logo" src="{{ url('/') }}/app-assets/images/logo.png">    
            <!-- <h2>Login</h2> -->
            <h2 class="login-title text-uppercase"><span>Sign In</span></h2>
            <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='E-mail id' name="email" type='email' id="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="input-field" placeholder='Password' name="password" type='password' id="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me</label>
                    </div>
                </div>

                <div class="login-forgot">
                    <button class='animated infinite pulse' type="submit">Signin</button>
                    <a href="{{ route('password.request') }}">
                        @lang('label.password_forgoted')
                    </a>
                </div>                
            </form>
            </div>
        </div>
    </div>
</div>

@endsection
