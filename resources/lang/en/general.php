<?php

return [
    /*navbar.blade.php*/
    'edit_profile' => 'Profil bearbeiten',
    'logout' => 'Ausloggen',
    'change_password' => 'Passwort ändern',
    'view' => 'Aussicht',
    'yes'=>'Yes',
    'no'=>'No',
    'action' =>'Action',
    'edit' =>'Edit',
    'delete' =>'Delete',
    'inactive' =>'Inactive',
    'active' =>'Active',
    'save' =>'SPAREN',
    'back' => 'ZURÜCK',
    'select' => 'Select',
    'password' => 'Passwort',
    'cpassword' => 'Conform Password',
];