<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ihre eingaben sind ungültig.',
    'throttle' => 'Sie haben zu oft Ihr Passwort falsch eingegeben. Bitte versuchen Sie es erneut in :seconds seconds.',
    'blocked' => 'Ihre E-Mail Adresse wurde geblockt.',

    'old_password' => 'Altes Passwort',
    'new_password' => 'Neues Kennwort',
    'cpassword' => 'Passwort bestätigen',

];
