<?php

return [

/*Login Page*/
	'credentials_not_matched'=>'Diese Anmeldeinformationen stimmen nicht mit unseren Datensätzen überein.',
	'remenber_me'=>'Remember Me',
	'signin'=>'Sign In',	

/*Menu*/	
	'edit_profile'=>'Profil editieren',
	'change_password'=>'Change Password',
	'dashboard' => 'Dashboard',
	'categories' => 'Kategorien',
	'keyword' => 'Stichworte',
	'document' => 'Dokumente',
	'users' => 'Benutzer',
	'audit_report' => 'Audit Reports',
	'page' => 'Seiten',
	'time_range' => 'Zeitraum',
	'back' => 'Zurück',

/*admin dashboard */
	'total_search' => 'Anzahl der Suchen gesamt',
	'total_categories' => 'Anzahl Kategorien',
	'total_documents' => 'Anzahl Dokumente',
	'total_users' => 'Anzahl  Benutzer',
	'cat_numbers'=>'Anzahl Kategorien',
	'cat_doc_count'=>'Kategorie Dokumentanzahl',
	'top_search_docs'=>'Top 10 der gesuchten Dokumente',
	'no_data_found'=>'Keine Daten gefunden...',
	'doc_name'=>'Name Dokument',
	'search_count'=>'Anzahl der Suchen',

/*Categories blade*/
	'add_category' => 'Kategorie hinzufügen',
	'name' => 'Name',
	'slug' => 'Slug',
	'option' => 'Optionen',
	'parent_category' => 'Mutterkategorie',
	'category_name' => 'Kategorie Name',
	'close' => 'Schliessen',
	'edit_category' => 'Kategorie bearbeiten',
	'save_category' => 'Kategorie speichern',
	'showing_entries'=>'Zeige Kategorie 1 bis 10 von 39 Kategorien',
	

/*Keyword blade*/	
	'add_keyword' => 'Keyqword hinzufügen',
	'keyword_name' => 'Keyqword Name',
	'save_keyword' => 'Keyqword speichern',
	

/*Document Blade*/	
	'add_document' => 'Dokument hinzufügen',
	'description' => 'Beschreibung',
	'title' => 'Titel',
	'video_url' => 'Video Url',
	'audio_url' => 'Audio hochladen',
	'upload_pdf' => 'PDF hochladen',
	'doc_timerange' => 'Zeitschiene Dokument',
	'doc_address' => 'Adresse Dokument',
	'cancel' => 'Abrechen',
	'save' => 'Speichern',
	'select_time_range' => 'Wählen Sie Zeitbereich',
	'select_cat' => 'Kategorie wählen',
	'enter_description_here' => 'Gib hier eine Beschreibung ein ...',

	'edit_document' => 'Dokument bearbeiten',	
	'upload_audio' => 'Audio-Datei hochladen und überschreiben',
	'upload_overwrite_pdf' => 'PDF hochladen und überschreiben',

	'doc_audit' => 'Dokument anzeigen',	
	'back' => 'Zurück',
	'doc_name' => 'Dokument Name',
	'doc_cat' => 'Kategorie',
	'recent_act' => 'Letzte Aktivitäten',
	'doc_action' => 'Aktivität',
	'doc_time' => 'Zeit',
	'doc_actionby' => 'Aktivität von',
	'user_ipaddress' => 'IP Addresse',
	'view_more' => 'Mehr anzeigen',
	'download' => 'Download',

/*User Blade*/	
	'add_user' => 'Benutzer hinzufügen',
	'role' => 'Rolle',
	'email' => 'Email',
	'docs_created' => 'Erstellte Dokumente',
	'last_login' => 'Letzter Login',
	'password' => 'Passwort',
	'generate_password' => 'Passwort generieren',
	'select_role' => 'Wähle Rolle',
	'edit_user' => 'Benutzer bearbeiten',
	'save_user' => 'Benutzer speichern',
	'block' => 'Blocken',
	'unblock' => 'Freischalten',
	'view' => 'Ansehen',
	'enter_name' => 'Name eingeben',
	'enter_email' => 'Email eingeben',

	'user_view' => 'Benutzer anzeigen',
	'user_name' => 'Benutzer Name',
	'user_emailid' => 'E-Mail id',

/*Time Range Blade*/	
	'add_timerange' => 'Zeitschiene hinzufügen',
	'year_range' => 'Zeitschiene Jahr',
	'min_year' => 'Startjahr',
	'max_year' => 'Endjahr',
	'edit_timerange' => 'Zeitschiene bearbeiten',
	'save_range' => 'Zeitschiene speichern',
	'enter_year_range' => 'Jahresbereich eingeben',
	'enter_min_year' => 'Geben Sie Min. Jahr ein',
	'enter_max_year' => 'Geben Sie Max. Jahr ein',
	

/*All Reports Index Blade*/	
	'all_reports' => 'Alle Berichte',
	'select_user' => 'Benutzer wählen',
	'search_doc' => 'Dokument suchen',
	'start_date' => 'Start Datum',
	'end_date' => 'End Datum',
	'document_action' => 'Aktivität Dokument',
	'select_user' => 'Nutzer wählen',
	

/*Pages Index Page*/	
	'create_page' => 'Seite erstellen',
	'page_views' => 'Seite ansehen',
	'create_new_page' => 'Neue Seite erstellen',
	'page_title' => 'Seitentitel',
	'enter_page_title' => 'Geben Sie den Seitentitel ein',
	'edit_page' => 'Seite bearbeiten',
	

/*Forget Password*/	
	'password_forgoted'=>'Passwort vergessen?',
	'reset_password'=>'Passwort zurücksetzen',
	'password_reset_link'=>'Password senden und Passwortlink zurücksetzen',
	'no_user_found'=>'Kein Benutzer für diese E-Mail Adresse hinterlegt.',
	'send_password_reset_link'=>'Wir haben Ihnen eine E-Mail gesendet zum Passwort zurücksetzen. ',

/*Frontside*/
	'content' => 'Inhalt',
	'location' => 'Lage',
	'read_more' => 'Weiterlesen',

/*Category Success Messages*/	
	'cat_add_success' => 'Kategorie erfolgreich hinzugefügt!',
	'cat_update_success' => 'Kategorie erfolgreich aktualisiert!',
	'cat_delete_success' => 'Kategorie erfolgreich gelöscht!',

/*Document Success Messages*/	
	'doc_add_success' => 'Dokument erfolgreich hinzugefügt!',
	'doc_update_success' => 'Dokument erfolgreich aktualisiert!',
	'doc_delete_success' => 'Dokument erfolgreich gelöscht!',	

/*Keyword Success Messages*/	
	'key_add_success' => 'Keywort erfolgreich hinzugefügt!',
	'key_update_success' => 'Keyword erfolgreich aktualisiert!',
	'key_delete_success' => 'Keyword erfolgreich gelöscht!',	

/*User Success Messages*/	
	'user_add_success' => 'Benutzer erfolgreich hinzugefügt!',
	'user_update_success' => 'Benutzer erfolgreich aktualisiert!',
	'user_delete_success' => 'Benutzer erfolgreich gelöscht!',		


];