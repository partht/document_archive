<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Ihr Passwort sollte mindestens 6 Buchstaben haben.',
    'reset' => 'Ihr Passwort wurde erfolgreich zurückgesetzt.!',
    'sent' => 'Wir haben Ihnen einen Link gesendet!',
    'token' => 'Ihr token ist nicht gültig.',
    'user' => "Kein Benutzer für diese E-Mail Adresse hinterlegt.",



];
