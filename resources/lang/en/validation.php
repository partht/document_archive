<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    /*Category Validations*/
    'cat_required' => 'Der Kategoriename ist ein Pflichtfeld. Bitte vergeben Sie einen Kategorienamen.',
    'cat_numeric' => 'Category Name must be characters or numeric, in between space not allowed.',
    'cat_delete' => 'Kategorie löschen?',
    'cat_delete_confirm' => 'Sind Sie sicher, dass Sie die Kategorie löschen möchten?',
    'delete_yes' => 'Ja, löschen.',
    'delete_no' => 'Nein, abrechen',


    /*Keyword Validations*/
    'key_required' => 'Der Keywordname ist ein Pflichtfeld.d',
    'key_numeric' => 'Der Keyword Name muss aus Bichstaben oder numerischen Zeichen bestehen.',
    'key_delete' => 'Keyword löschen?',
    'key_delete_confirm' => 'Sind Sie sicher, dass Sie das Keyword löschen möchten?',


    /*Document Validations*/
    'doc_delete' => 'Möchten Sie das Dokument löschen?',
    'doc_delete_confirm' => 'Sind Sie sicher, dass Sie das Dokument löschen möchten?',
    'video_audio_pdf_req' => 'Bitte laden Sie entweder eine Video, Audio oder eine PDF Datei hoch.',
    'doc_title_req' => 'Bitte vergeben Sie einen Titel. Maximal 50 Zeichen',
    'doc_address_valid' => 'Geben Sie eine valide Adresse ein.',
    'upload_limit' => 'Das ausgewählte PDF überschreitet die maximale Dateigröße.',


    /*Users Validations*/
    'user_required' => 'Der Benutzername ist ein Pflichtfeld.',
    'email_required' => 'E-Mail ist ein Pflichtfeld.',
    'password_required' => 'Das Passwort ist ein Pflichtfeld. Dieses muss mindestens aus 6 Buchstaben bestehen.',
    'user_numeric' => 'Der Benutzername muss aus Buchstaben oder Zahlen bestehen.',
    'user_delete' => 'Möchten Sie diesen Benutzer löschen?',
    'user_delete_confirm' => 'Sind Sie sicher, dass Sie den Benutzer löschen möchten?',
    'user_block' => 'Benutzer blockieren?',
    'user_block_confirm' => 'Sind Sie sicher, dass Sie diesen Benutzer blockieren möchten?',
    'block_yes' => 'Ja blockieren.',
    'block_no' => 'Nein, abrechen',
    'user_unblock' => 'Nutzer wieder freigeben?',
    'user_unblock_confirm' => 'Sind Sie sicher, dass Sie den Benutzer wieder freigeben möchten?',
    'unblock_yes' => 'Ja, freigeben.',
    'unblock_no' => 'Nein, abrechen',

    /*Time Range Validations*/
    'year_range_required' => 'Bitte geben Sie einen Zeitraum an.',
    'year_range_numeric' => 'Der Zeitraum darf nur aus Zahlenwerten bestehen.',
    'min_year_required' => 'Bitte geben Sie ein Anfangsjahr ein.',
    'min_year_numeric' => 'Das Anfangsjahr muss aus Buchstaben oder Zahlen bestehen',
    'max_year_required' => 'Bitte geben Sie das Endjahr ein.',
    'max_year_numeric' => 'Das Endjahr darf nur aus Zahlenwerten bestehen.',
    'min_less_max' => 'Das Startjahr darf nicht hinter dem Endjahr liegen.',
    'timerange_delete' => 'Zeitraum löschen?',
    'timerange_delete_confirm' => 'Sind Sie sicher, dass Sie den Zeitraum löschen möchten?',

    /*All Reports Validations*/
    'start_date_required' => 'Bitte geben Sie ebenfalls ein Startjahr an.',
    'end_date_required' => 'Bitte geben Sie ebenfalls ein Endjahr an.',
    'start_less_end' => 'Das Startdatum muss kleiner als das Enddatum sein.',
    'onefield_required' => 'Ein Feld von Benutzer / Dokument / Datum ist erforderlich.',

    /*Page Validations*/
    'page_delete' => 'Seite löschen?',
    'page_delete_confirm' => 'Sind Sie sicher, dass Sie die Seite löschen möchten?',
    'req_page_title'=>'Seitentitel ist erforderlich',
    'img_upload_limit' => 'Wählen Sie ein Bild mit weniger als 500 KB',


    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'Das ausgewählte PDF überschreitet die maximale Dateigröße 10MB.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];