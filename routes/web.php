<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('/setnewpassword', 'HomeController@setnewpassword')->name('setnewpassword');


Route::get('/', 'WebsiteController@index')->name('website.home');
Route::get('/{slug}', 'WebsiteController@document')->name('website.test');
Route::get('/page/{slug}', 'WebsiteController@page')->name('website.page');

Route::prefix(config('system_parameters.admin_slug'))->group(function(){
	
	Route::post('/resetpassword', 'ResetPasswordController@showResetForm')->name('resetpassword');
	Route::post('/updatenew_password', 'HomeController@updatenew_password')->name('updatenew_password');
	Auth::routes();
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/catdoc_count','HomeController@document_category_count')->name('home.catdoc_count');
	Route::post('/searchdoc_count','HomeController@searched_document_count')->name('home.searchdoc_count');
	Route::get('/profile', 'HomeController@profile')->name('user_profile');
	Route::post('/update_profile', 'HomeController@update_profile')->name('update_profile');
	Route::post('/update_password', 'HomeController@update_password')->name('update_password');
	Route::get('/change_password', 'HomeController@change_password')->name('change_password');
	

	Route::namespace('admin')->prefix('category')->group(function(){
		Route::get('/','CategoryController@index')->name('category.index');
		Route::get('/add','CategoryController@add')->name('category.add');
		Route::post('/store','CategoryController@store')->name('category.store');
		Route::post('/update/{id}','CategoryController@update')->name('category.update');
		Route::get('/delete/{id}','CategoryController@delete')->name('category.delete');
	});


	Route::namespace('admin')->prefix('document')->group(function(){
		Route::get('/','DocumentController@index')->name('document.index');
		Route::get('/add','DocumentController@add')->name('document.add');
		Route::post('/store','DocumentController@store')->name('document.store');
		Route::get('/edit/{id}','DocumentController@edit')->name('document.edit');
		Route::post('/update/{id}','DocumentController@update')->name('document.update');
		Route::get('/delete/{id}','DocumentController@delete')->name('document.delete');
		Route::get('/audit/{id}','DocumentController@showaudit')->name('document.showaudit');
	});

	Route::namespace('admin')->prefix('keyword')->group(function(){
		Route::get('/','KeywordController@index')->name('keyword.index');
		Route::get('/add','KeywordController@add')->name('keyword.add');
		Route::post('/store','KeywordController@store')->name('keyword.store');
		Route::post('/update/{id}','KeywordController@update')->name('keyword.update');
		Route::get('/delete/{id}','KeywordController@delete')->name('keyword.delete');
	});

	Route::namespace('admin')->prefix('timerange')->middleware('check-permission:admin')->group(function(){
		Route::get('/','TimerangeController@index')->name('timerange.index');
		Route::post('/store','TimerangeController@store')->name('timerange.store');
		Route::post('/update/{id}','TimerangeController@update')->name('timerange.update');
		Route::get('/delete/{id}','TimerangeController@delete')->name('timerange.delete');
	});

	Route::namespace('admin')->prefix('user')->middleware('check-permission:admin')->group(function(){
		Route::get('/','UserController@index')->name('user.index');
		Route::get('/add','UserController@add')->name('user.add');
		Route::post('/store','UserController@store')->name('user.store');
		Route::get('/editblock/{id}{block}','UserController@editblock')->name('user.editblock');
		Route::get('/show/{id}','UserController@show')->name('user.show');
		//Route::get('/audit/{id}','UserController@showaudit')->name('user.showaudit');
		Route::get('/edit/{id}','UserController@edit')->name('user.edit');
		Route::post('/update/{id}','UserController@update')->name('user.update');
		Route::get('/delete/{id}','UserController@delete')->name('user.delete');
	});

	Route::namespace('admin')->prefix('page')->group(function(){
		Route::get('/','PageController@index')->name('page.index');
		Route::get('/add','PageController@add')->name('page.add');
		Route::post('/store','PageController@store')->name('page.store');
		Route::get('/edit/{id}','PageController@edit')->name('page.edit');
		Route::post('/update/{id}','PageController@update')->name('page.update');
		Route::get('/delete/{id}','PageController@delete')->name('page.delete');
	});

	Route::namespace('admin')->prefix('reports')->group(function(){
		Route::get('/','AllReportsController@index')->name('report.index');
		// Route::get('/{?u_id}/{?d_id}','AllReportsController@index')->name('report.index');
		Route::get('/searchdoc','AllReportsController@autoComplete')->name('report.autocomplete');
		Route::post('/searchuserdoc','AllReportsController@searchuserdoc')->name('report.searchuserdoc');
	});
});

Route::group(['middleware'=>'auth'], function () {

	Route::get('permissions-admin-subadmin',['middleware'=>'check-permission:admin|subadmin','uses'=>'HomeController@subadmin']);

	Route::get('permissions-admin',['middleware'=>'check-permission:admin','uses'=>'HomeController@admin']);
});