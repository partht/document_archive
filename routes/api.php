<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/search', 'WebsiteController@search')->name('website.search.api');
/*Route::post('/search_scout', 'WebsiteController@search_scout')->name('website.search_scout.api');*/

Route::post('/audiocount', 'WebsiteController@docaudiocount')->name('website.docaudiocount.api');
Route::post('/pageviewcount', 'WebsiteController@pageviewcount')->name('website.pageviewcount.api');

