{{route('user.edit',['id' => $user['id']])}}
SQL

ALTER TABLE `documents` ADD `video_url` VARCHAR(512) NOT NULL AFTER `url`; 

ALTER TABLE `documents` ADD `status_flag` INT(11) NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `documents` ADD `audio_name` VARCHAR(512) NULL DEFAULT NULL AFTER `video_url`, ADD `audio_url` VARCHAR(512) NULL DEFAULT NULL AFTER `audio_name`;

CREATE TABLE `document_archive_db`.`time_range` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `timerange` VARCHAR(255) NULL DEFAULT NULL , `min_time` VARCHAR(255) NULL DEFAULT NULL , `max_time` VARCHAR(255) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; 

ALTER TABLE `time_range` ADD `created_at` TIMESTAMP NULL AFTER `max_time`, ADD `updated_at` TIMESTAMP NULL AFTER `created_at`; 

ALTER TABLE `category` ADD `docs_count` INT(20) NOT NULL DEFAULT '0' AFTER `slug`, ADD `audio_count` INT(20) NOT NULL DEFAULT '0' AFTER `docs_count`, ADD `video_count` INT(20) NOT NULL DEFAULT '0' AFTER `audio_count`;

ALTER TABLE `documents` ADD `search_time` TIMESTAMP NULL AFTER `timerange_id`; 

/*Address Field*/
ALTER TABLE `documents` ADD `lat_long` VARCHAR(255) NULL DEFAULT NULL AFTER `search_time`, ADD `city` VARCHAR(255) NULL DEFAULT NULL AFTER `lat_long`, ADD `state` VARCHAR(255) NULL DEFAULT NULL AFTER `city`, ADD `country` VARCHAR(255) NULL DEFAULT NULL AFTER `state`; 