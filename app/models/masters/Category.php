<?php

namespace App\models\masters;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'category';
	public $timestamps = true;
	protected $primaryKey = "id";

	protected $fillable = ['parent_id', //Default NULL
								'name',
								'slug',
								'docs_count',
								'audio_count',
								'video_count',
								'created_by',
								'updated_by'
							];

	public static $categories_array = array();
	public static function get_list($categories,$depth) 
	{
		foreach($categories as $category) 
		{
			//array_push(Category::$categories_array, array('depth'=>$depth,'category'=>$category));
			$cat = $category->toArray();
			//$cat = array_merge($cat, array('depth'=>$depth));
			array_push(Category::$categories_array, array_merge($category->toArray(), array('depth'=>$depth,'object'=>$category)));
			if($category->children->count()) 
			{
				$children_array = Category::get_list($category->children->sortBy('name'),$depth+1);
			}
		}
		return Category::$categories_array;
	}

	public function children()
    {
    	return $this->hasMany('App\models\masters\Category','parent_id','id');
    }

    public function parent()
    {
    	return $this->hasOne('App\models\masters\Category','id','parent_id');    	
    }

    public function cargo_types_map()
    {
    	return $this->hasMany('App\models\masters\CategoryCargoTypeMap','category_id','id');
    }

    public function root_category()
    {
    	if ($this->parent_id == 0) 
    	{
    		return $this;
    	}
    	else
    	{
    		return $this->parent->root_category();	
    	}
    }
}

?>