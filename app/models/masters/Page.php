<?php

namespace App\models\masters;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	//use Searchable;
	
	protected $table = 'pages';
	public $timestamps = true;
	protected $primaryKey = "id";

	protected $fillable = ['title',
							'slug', 
							'content',	//Default NULL
							'tags',
							'page_views',
							'created_at',		//Default NULL
							'updated_at',
							'created_by',
							'updated_by'
							];
}