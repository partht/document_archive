<?php

namespace App\models\masters;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'keyword';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $fillable = ['name',
							'slug',
							'created_by',
							'updated_by'
							];

}
