<?php

namespace App\models\masters;

use Illuminate\Database\Eloquent\Model;

class DocumentAudit extends Model
{
	//use Searchable;
	
	protected $table = 'document_audit';
	protected $primaryKey = "id";

	protected $fillable = ['doc_id',
							'user_id', 
							'action',
							'user_ipaddress',
							'action_time',
							];

	public function document()
    {
        return $this->hasOne('App\models\Document','id','doc_id')->select('id','title','public_url','audio_url','video_url');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id')->select('id','name','email');
    }
}
