<?php

namespace App\models\masters;

use Illuminate\Database\Eloquent\Model;

class Timerange extends Model
{
    protected $table = 'time_range';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $fillable = ['yearrange',
							'maxyear',
							'minyear',
							];

}
