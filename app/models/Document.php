<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Document extends Model
{
	//use Searchable;
	
	protected $table = 'documents';
	public $timestamps = true;
	protected $primaryKey = "id";

	protected $fillable = ['title',
							'slug', 
							'description',	//Default NULL
							'categories',
							'parent_cat',
							'keywords',		//Default NULL
							'data',
							'url',
							'video_url',
							'audio_name',
							'audio_url',
							'public_url',
							'search_count',
							'created_by',
							'updated_by',
							'status_flag',
							'timerange_id',
							'search_time',
							'address',
							'lat_long',
							'city',
							'state',
							'country',
							];

	public function document_audit()
    {
        return $this->hasMany('App\models\masters\DocumentAudit','doc_id','id');
    }
}
