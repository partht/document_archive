<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class welcomemail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->password = $password;
        //dd($this->password);
    }

    /**
     * Build the message.   
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.welcome')->with(['password'=>$this->password,'user'=>$this->user]);
    }
}
