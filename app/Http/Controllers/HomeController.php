<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\models\masters\Category as category;
use App\models\Document as document;

use Session;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['updatenew_password']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->headers);
        // dd($request->ip());
        $users = User::select('id')->where('status_flag','0')->get();
        $categories = category::select('id')->get();
        $documents = document::select('id','search_count')->where('status_flag','0')->get();
        return view('home',compact('categories','documents','users'));
    }

    public function document_category_count()
    {
        $cate_doc_count = category::select('id','name','docs_count')->where('parent_id','0')->get();
        //dd($cate_doc_count);
        return $cate_doc_count;
    }

    public function searched_document_count(Request $request)
    {
        $startdate = Date('Y-m-d 00:00:00', strtotime($request->get('startdate')));
        $enddate = Date('Y-m-d 23:23:23', strtotime($request->get('enddate')));
        
        $searched_docs = document::select('id','title','search_count','search_time')->where('search_time','>=',$startdate)->where('search_time','<=',$enddate)->where('status_flag','0')->orderBy('search_count','desc')->limit(10)->get(); 
        //dd($searched_docs);
        /*$currentDate = \Carbon\Carbon::now();
        if($request->get('timefilter') == 'week') {
            $agoDate = $currentDate->subDays(7);
            $searched_docs = document::select('id','title','search_count','search_time')->where('search_time','>=',$currentDate->toDateTimeString())->where('status_flag','0')->orderBy('search_count','desc')->limit(10)->get();   
        }
        if($request->get('timefilter') == 'month') {
            $agoDate = $currentDate->subDays(30);
            $searched_docs = document::select('id','title','search_count','search_time')->where('search_time','>=',$currentDate->toDateTimeString())->where('status_flag','0')->orderBy('search_count','desc')->limit(10)->get();   
        }
        if($request->get('timefilter') == 'quater') {
            $agoDate = $currentDate->subDays(120);
            $searched_docs = document::select('id','title','search_count','search_time')->where('search_time','>=',$currentDate->toDateTimeString())->where('status_flag','0')->orderBy('search_count','desc')->limit(10)->get();   
        }
        if($request->get('startdate') && $request->get('enddate')) {
            //dd($request->get('startdate'));
            $startdate = $request->get('startdate');
            $enddate = $request->get('enddate');
            $searched_docs = document::select('id','title','search_count','search_time')->where('search_time','>=',$startdate)->where('search_time','<=',$enddate)->where('status_flag','0')->orderBy('search_count','desc')->limit(10)->get(); 
            //dd($searched_docs);  
        }*/
        return $searched_docs;
    }

    public function admin()
    {
        dd('Acess to all modules');
    }

    public function subadmin()
    {
        dd('Acess to all modules except user module and timerange module');
    }    

    public function profile()
    {
        return view('profile');
    }

    public function update_profile(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|string|max:255',
            ]);

        User::find(auth()->user()->id)->update([
                'name' => $request['name'],
                ]);
        Session::flash('success_msg','Profile changed successfully!');
        return redirect('admin/home');

        /*if(Auth()->user()->user_type == 'A')
        {
            
        }
        else
        {
            $statefind = State::where('name',$request->state)->count();
            if ($statefind == 0)
            {
                $state = State::create(['country_id'=>$request['country'] ,'name' => $request->state ]);
            }
            else
            {
                $state = State::where('name',$request->state)->first();
            }

            $cityfind = City::where('name',$request->city)->count();
            if ($cityfind == 0)
            {
                $city = City::create(['state_id'=>$state->id, 'name' => $request->city]);
            }
            else
            {
                $city = City::where('name',$request->city)->first();
            }

            $this->validate($request,[
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone_number' =>'required',
                'address' =>'required',
                'country' =>'required',
                'state' =>'required',
                'city' =>'required',
            ]);

            $houseowner = User::find($id)->update([
                        'name' => $request['name'],
                        'email' =>$request['email'],
                        'phone_number' => $request['phone_number'],
                        'address' => $request['address'],
                        'country' => $request['country'],
                        'state' => $state->id,
                        'city' => $city->id,
                    ]);

            Session::flash('success_msg','Profile Updated SuccessFully!!!');
            return redirect()->route('/home');
        }*/
    }

    public function change_password()
    {
        return view('change_password');
    }

    public function update_password(Request $request)
    {
        $curPassword = auth()->user()->password;
        if(!Hash::check($request['old_password'], $curPassword))
        {
            echo json_encode(['status'=>0]);
        }
        else
        {
            $user = User::find(Auth()->user()->id)->update([
                    'password' => bcrypt($request['new_password']),
                    ]);
            Session::flash('success_msg','Password updated successfully!');
            echo json_encode(['status'=>1]);
        }
    }


    public function setnewpassword()
    {
        return view('setnewpassword');
    }

    public function updatenew_password(Request $request)
    {

        $user = User::where('email',$request['email'])->first();
        $curPassword = $user->password;
        //dd($curPassword);
        if(!Hash::check($request['old_password'], $curPassword))
        {
            echo json_encode(['status'=>0]);
        }
        else
        {
            //dd("password changed");
            $user = User::find($user->id)->update([
                    'password' => bcrypt($request['new_password']), 'status' => 1,
                    ]);
            Session::flash('success_msg','Password updated successfully!');
            echo json_encode(['status'=>1]);
        }
    }


    /*
    public function change_password()
    {
        
        return view('change_password');
    }
    

    public function set_password()
    {
        if(Auth()->user()->is_first_login == 'Y')
        {
            return view('first_password');
        }
        else
        {
            return redirect('/home');
        }
    }
    public function first_password(Request $request)
    {
        // dd($request->all());
        $request->validate([
        'password' => 'required|confirmed|min:6',
        'password_confirmation' => 'required|min:6',
         ]);

        $user = User::find(Auth()->user()->id)->update([
                'password' => bcrypt($request['password']),
                'is_first_login' => 'N',
                ]);

        return redirect('/home');
    }*/
}
