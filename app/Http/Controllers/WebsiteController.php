<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\models\Document as document;
use App\models\masters\Page as page;
use App\models\masters\Category as category;
use App\models\masters\Timerange as timerange;

use Session;
use Hash;

class WebsiteController extends Controller
{
	public function index()
	{
		$categories = category::where('parent_id','0')->where('docs_count','>','0')->orderBy('docs_count','desc')->get();
		$pagecontent = page::all();
		$years = timerange::all();
		return view('website.index',compact('categories','pagecontent','years'));
	}

/*Increase Pages View count*/
	public function page($slug)
	{
		/*$page = page::where('slug',$slug)->first();
		if(!$page){
			abort(404);
		}
		$page->update(['page_views',($page->page_views++)]);
		return view('website.page',compact('page'));*/

		$categories = category::where('parent_id','0')->where('docs_count','>','0')->orderBy('docs_count','desc')->get();
		$pagecontent = page::all();
		$years = timerange::all();
		$pagespecific = page::where('slug',$slug)->first();
		return view('website.index',compact('categories','pagecontent','years','pagespecific'));
	}

/*Increase Document search count*/
	public function document($slug)
	{
		$document = document::where('slug',$slug)->first();
		if(!$document){
			abort(404);
		}
		$document->update(['search_count'=>($document->search_count+1), 'search_time'=>now()]);
		return view('website.document',compact('document'));
	}

/*Document Search API*/
	public function search(Request $request)
	{
		//dd($request->get('doctype'));
		$search_parameter = $request->get('search_parameter');
		$filter_option = $request->get('filter_options');
		$catname = $request->get('catname');
		//dd($catname);
		$doctype = $request->get('doctype');
		if (strpos($doctype, '-1') !== false) {
		    $doctype="";
		}
		else {
			$doctype=$request->get('doctype');
		}
		$year_range = $request->get('yearrange');
		//dd($doctype);
		$time_start = microtime(true);
		if($catname == 'allcat')
		{
			$catname="";
		}
		//Search Filter for year range
		if($year_range!="")
		{
			$year = explode(',',$year_range);
			
			if($filter_option!="")
			{
				$documents = document::where('status_flag','0')->whereIn('timerange_id',$year)->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
			}
			else
			{	//dd("all data from search");
				$documents = document::where('status_flag','0')->whereIn('timerange_id',$year)->where(
					function($query) use ($search_parameter)
					{
						$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
					})
				->get();
			}
		}
		//End of year range filter
		//Search Filter for Document type 
		elseif($doctype!="") 
		{
			$dt = explode(',', $doctype);
			//Filter for Document only
			if(in_array('docs',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->whereNotNull('public_url')->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('status_flag','0')->whereNotNull('public_url')->where(
						function($query) use ($search_parameter)
						{
							$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
						})
					->get();
				}
				
			}
			//Filter for Audio only
			if(in_array('audiodoc',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->whereNotNull('audio_url')->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('status_flag','0')->whereNotNull('audio_url')->where(
						function($query) use ($search_parameter)
						{
							$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
						})
					->get();
				}
				
				//dd($documents);		
			}
			//Filter for Video only
			if(in_array('videodoc',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->whereNotNull('video_url')->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('status_flag','0')->whereNotNull('video_url')->where(
						function($query) use ($search_parameter)
						{
							$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
						})
					->get();
				}
				
				//dd($documents);		
			}
			//Filter for Video and Audio
			if(in_array('videodoc',$dt) && in_array('audiodoc',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->where("".$filter_option."",'like',"%".$search_parameter."%")->where(
						function($query) use ($search_parameter)
						{
							$query->whereNotNull('video_url')->orwhereNotNull('audio_url');
						})
					->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('title','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->orwhere('slug','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->orwhere('description','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->orwhere('keywords','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->orwhere('data','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->orwhere('address','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('video_url')->orwhereNotNull('audio_url')->get();
				}
			}
			//Filter for Document and Audio
			if(in_array('docs',$dt) && in_array('audiodoc',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->where("".$filter_option."",'like',"%".$search_parameter."%")->where(
						function($query) use ($search_parameter)
						{
							$query->whereNotNull('public_url')->orwhereNotNull('audio_url');
						})
					->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('title','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->orwhere('slug','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->orwhere('description','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->orwhere('keywords','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->orwhere('data','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->orwhere('address','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('audio_url')->get();
				}
			}
			//Filter for Document and Video
			if(in_array('docs',$dt) && in_array('videodoc',$dt))
			{
				if($filter_option!="")
				{
					$documents = document::where('status_flag','0')->where("".$filter_option."",'like',"%".$search_parameter."%")->where(
						function($query)
						{
							$query->whereNotNull('public_url')->orwhereNotNull('video_url');
						})
					->get();
				}
				else
				{	//dd("all data from search");
					$documents = document::where('title','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->orwhere('slug','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->orwhere('description','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->orwhere('keywords','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->orwhere('data','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->orwhere('address','like',"%".$search_parameter."%")->where('status_flag','0')->whereNotNull('public_url')->orwhereNotNull('video_url')->get();
					/*$documents = document::where('status_flag','0')->*/
				}
			}
		}
		//End of Document type filter
		//Search Filter by Category
		elseif($catname!="")
		{
			if($filter_option!="")
			{
				$documents = document::where('status_flag','0')->where('parent_cat',$catname)->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
			}
			else
			{	//dd("all data from search");
				$documents = document::where('status_flag','0')->where('parent_cat',$catname)->where(
					function($query) use ($search_parameter)
					{
						$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
					})
				->get();
			}
		}
		//End of Category Filter
		//Else
		else
		{	
			if($filter_option!="")
			{
				$documents = document::where('status_flag','0')->where("".$filter_option."",'like',"%".$search_parameter."%")->get();
			}
			else
			{	//dd("all data from search");
				$documents = document::where('status_flag','0')->where(
					function($query) use ($search_parameter)
					{
						$query->where('title','like',"%".$search_parameter."%")->orwhere('slug','like',"%".$search_parameter."%")->orwhere('description','like',"%".$search_parameter."%")->orwhere('keywords','like',"%".$search_parameter."%")->orwhere('data','like',"%".$search_parameter."%")->orwhere('address','like',"%".$search_parameter."%");
					})
				->get();
			}
		}
		//dd($documents);
		$time_end = microtime(true);
		
		$time_taken = $time_end - $time_start;
		
		$result = array();
		
		foreach ($documents as $document) 
		{
			if($document->data!="")
			{
				$index = strpos($document->data, $search_parameter);
			}
			else 
			{
				$index = strpos($document->description, $search_parameter);
			}
			$start = 0;
			if ($index > 50) 
			{
				$start = $index-50;
			}
			if($document->data=="")
			{
				$docdata = $document->description;
			}
			else
			{
				$docdata = $document->data;
			}
			//dd($document->address);
			array_push($result, array(
				'id'=>$document->id,
				'title'=>$document->title,
				'slug'=>$document->slug,
				'public_url'=>$document->public_url,
				'audio_url'=>$document->audio_url,
				'video_url'=>$document->video_url,
				'keywords'=>explode(',', $document->keywords),
				'address'=>$document->address,
				'city'=>$document->city,
				'country'=>$document->country,
				'desc'=>$document->description,
				'data'=>$document->data,
				'description'=>htmlspecialchars(substr($docdata, $start,250)),
			));
		}
		// print_r(array('total_documents'=>document::count(),'time_taken'=>$time_taken,'documents'=>$result));
		// die();
		echo json_encode(array('total_documents'=>document::count(),'time_taken'=>$time_taken,'documents'=>$result));
	}

/*Increase Document Search Count on click of audio play*/
	public function docaudiocount(Request $request)
	{
		//dd($request->audioid);
		$docaudio = document::where('id',$request->audioid)->first();
		if(!$docaudio){
			abort(404);
		}
		$docaudio->update(['search_count'=>($docaudio->search_count+1), 'search_time'=>now()]);
		return json_encode(['success'=>1]);
	}

/*Increase Page view count*/
	public function pageviewcount(Request $request)
	{
		//dd($request->pageid);
		$pageviews = page::where('id',$request->pageid)->first();
		if(!$pageviews){
			abort(404);
		}
		$pageviews->update(['page_views'=>($pageviews->page_views+1)]);
		//return view('website.index');
		return json_encode(['success'=>1]);
	}

	public function search_scout(Request $request)
	{
		$search_parameter = $request->get('search_parameter');
		
		$time_start = microtime(true);
		$documents = document::search($search_parameter)->get();
		$time_end = microtime(true);
		
		$time_taken = $time_end - $time_start;
		
		$result = array();
		foreach ($documents as $document) 
		{
			if($document->data!="")
			{
				$index = strpos($document->data, $search_parameter);
			}
			else 
			{
				$index = strpos($document->description, $search_parameter);
			}
			$start = 0;
			if ($index > 50) 
			{
				$start = $index-50;
			}
			if($document->data=="")
			{
				$docdata = $document->description;
			}
			else
			{
				$docdata = $document->data;
			}
			array_push($result, array(
				'title'=>$document->title,
				'link'=>$document->public_url,
				'keywords'=>explode(',', $document->keywords),
				'description'=>substr($docdata, $start,150),
			));
		}
		return json_encode(array('time_taken'=>$time_taken,'documents'=>$result));

	}
}
