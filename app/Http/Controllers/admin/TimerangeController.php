<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\masters\Timerange as timerange;

use Session;

class TimerangeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $timerange = timerange::orderBy('created_at','desc')->get();
        return view('admin.timerange.index',compact('timerange'));
    }

    public function add()
    {
        Session::flash('open_add','true');
        return redirect()->route('timerange.index');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
        'yearrange' => 'regex:/^[A-Za-z0-9- ]+$/u|unique:time_range',
        'minyear' => 'regex:/^[0-9]+$/u|unique:time_range',
        'maxyear' => 'regex:/^[0-9]+$/u|unique:time_range'
        ]);

        if($validatedData!=""){
            Session::flash('open_add','true');
        }

        $timerange = timerange::create(['yearrange'=>$request->get('yearrange'),'minyear'=>$request->get('minyear'),'maxyear'=>$request->get('maxyear'),'created_by'=>auth()->user()->id]);
        Session::flash('success_msg','Zeitbereich erfolgreich hinzugefügt!');
        return redirect()->route('timerange.index');
    }

    public function edit()
    {
        Session::flash('open_edit','true');
        return redirect()->route('timerange.index');
    }

    public function update($id,Request $request)
    {
        $timerange = timerange::find($id);

        $validatedData = $request->validate([
        'yearrange' => 'regex:/^[A-Za-z0-9- ]+$/u',
        'minyear' => 'regex:/^[0-9]+$/u',
        'maxyear' => 'regex:/^[0-9]+$/u'
        ]);

        if($validatedData!=""){
            Session::flash('open_edit','true');
        }

        $timerange->update(['yearrange'=>$request->get('yearrange'), 'minyear'=>$request->get('minyear'), 'maxyear'=>$request->get('maxyear'), 'updated_by'=>auth()->user()->id]);
        
        Session::flash('success_msg','Zeitbereich erfolgreich aktualisiert!');
        
        return redirect()->route('timerange.index');
    }

    public function delete($id)
    {
        timerange::find($id)->delete();
        Session::flash('success_msg','Zeitbereich erfolgreich gelöscht!');
        return redirect()->route('timerange.index');
    }
}
