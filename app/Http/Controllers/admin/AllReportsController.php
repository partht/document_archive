<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Document as document;
use App\models\masters\DocumentAudit as document_audit;
use App\models\masters\Category as category;
use App\models\masters\Keyword as keyword;
use App\User;

class AllReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $users = User::where('status_flag','0')->get();
        $document = document::select('title')->find($request->docid);
        // dd($document);
        // $document_audit = document_audit::all();
        return view('admin.all-reports.index', compact('users','document'));
    }   

/*Get autocomplete suggession list on document search*/
    public function autoComplete(Request $request) {
        $query = $request->get('term','');
        
        $documents=document::select('id','title')->where('title','LIKE','%'.$query.'%')->where('status_flag','0')->get();
        
        $data=array();
        foreach ($documents as $document) {
                $data[]=array('value'=>$document->title,'id'=>$document->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    } 

/*Search Filter with User/Document/Date*/
    public function searchuserdoc(Request $request){
        //dd("test");
        //dd($request->get('searchtime'));

        //$user = User::find($request->get('usersearch'),['id','name','email']);
        $user = User::select(['id','name','email'])->find($request->get('usersearch'));
        //dd($user);
        //$doc = document::find($request->get('searchdocid'),['id','title']);
        $doc = document::select(['id','title'])->find($request->get('searchdocid'));

        $startdate = Date('Y-m-d 00:00:00', strtotime($request->get('startdate')));
        $enddate = Date('Y-m-d 23:23:23', strtotime($request->get('enddate')));
        
        //Only Search By User
        if($request->get('usersearch') != "" && $request->get('searchdocid') == "" && $request->get('startdate') == "" && $request->get('enddate') == "") {
            $audits =$user->document_audit->where('user_id',$request->get('usersearch'))->sortByDesc('id',true);    
        }
        
        //Only Search By Document
        if($request->get('searchdocid') != "" && $request->get('usersearch') == "" && $request->get('startdate') == "" && $request->get('enddate') == "") {
            $audits = $doc->document_audit->where('doc_id',$request->get('searchdocid'))->sortByDesc('id',true);
        }

        //Only Search By Date
        if($request->get('startdate') != "" && $request->get('enddate') != "" && $request->get('searchdocid') == "" && $request->get('usersearch') == "") {
            $audits = document_audit::whereBetween('action_time',[$startdate,$enddate])->orderBy('id','desc')->get();
            //dd($audits);
        }

        //Search By User & Document
        if($request->get('searchdocid') != "" && $request->get('usersearch') != ""  && $request->get('startdate') == "" && $request->get('enddate') == "") {
            $audits = $user->document_audit->where('doc_id',$request->get('searchdocid'))->sortByDesc('id',true);
            //dd($audits);
        }

        //Search By Date & Document
        if($request->get('searchdocid') != "" && $request->get('usersearch') == ""  && $request->get('startdate') != "" && $request->get('enddate') != "") {
            //dd($request->get('searchdocid'));
            $audits = $doc->document_audit->where('action_time','>=',$startdate)->where('action_time','<=',$enddate)->sortByDesc('id',true);
            // dd($audits);
        }

        //Search By Date & User
        if($request->get('searchdocid') == "" && $request->get('usersearch') != ""  && $request->get('startdate') != "" && $request->get('enddate') != "") {
            $audits =$user->document_audit->where('action_time','>=',$startdate)->where('action_time','<=',$enddate)->sortByDesc('id',true);
            // dd($audits);
        }

        //Search By Date & User & Document
        if($request->get('searchdocid') != "" && $request->get('usersearch') != ""  && $request->get('startdate') != "" && $request->get('enddate') != "") {
            $audits = $user->document_audit->where('doc_id',$request->get('searchdocid'))->where('action_time','>=',$startdate)->where('action_time','<=',$enddate)->sortByDesc('id',true);
        }
        
        return $audits->load('user','document')->values();
    }
}
