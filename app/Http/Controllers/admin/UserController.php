<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User as users;
use App\models\Document as document;
use App\models\masters\DocumentAudit as document_audit;

use Session;
use App\Http\Controllers\utilities\FunctionsController as fun;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\welcomemail;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {    
        //List Admin Users
        $usersdata = users::where('status_flag','0')->get();
        $userdocs = document::select('created_by')->where('status_flag','0')->get();
        return view('admin.user.index', compact('usersdata','userdocs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //Add User
        Session::flash('open_add','true');
        return redirect()->route('user.index');
        //return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd("test");
        //Validation
        $validatedData = $request->validate([
        'name' => 'required|regex:/^[a-zA-Z0-9]+$/u|max:100',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6'
        ]);

        //Send Mail and Add User
        try{
            $user = users::create(['name'=>$request->get('name'), 'email'=>$request->get('email'), 'password'=>bcrypt($request->get('password')), 'role'=>$request->get('role'), 'block'=>'U','created_by'=>auth()->user()->id]);
            Mail::to($request['email'])->send(new welcomemail($user, $request['password']));
            Session::flash('success_msg','Benutzer erfolgreich hinzugefügt!');
            return redirect()->route('user.index');
        }catch(\Exception $e){
            Session::flash('delete_msg','Woops, Mail wurde nicht an den Benutzer gesendet.');
            return redirect()->route('user.index');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = users::find($id);
        if(!$user){
            abort(404);
        }
        // dd($user);
        // $docaudit = document_audit::select('action','action_time')->where('user_id',$id)->orderBy('id', 'desc')->limit(5)->get();
        // $user->document_audit
        // dd($user->document_audit);
        //$userdocs = array();
        /*foreach($docaudit as $docs) 
        {
            $userdocs[] = document::select('id','title','public_url')->find($docs['doc_id']);
        }
        dd($docaudit);*/
        return view('admin.user.view', compact('user'));
    }

    /*public function showaudit($id)
    {
        $userdata = users::find($id);
        $userdocs = document::select('title','public_url','created_at','updated_at','created_by','updated_by')->orderBy('created_at', 'desc')->limit(5)->get();
        return view('admin.user.audit', compact('userdata','userdocs'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editblock($id, $block)
    {
        //Edit Block/Unblock User
        $user = users::find($id);
        if($block == 'U'){$role='B';}
        else {$role='U';}
        $user->update(['block'=>$role,'updated_by'=>auth()->user()->id]);
        if($block == 'B'){
            Session::flash('success_msg','Benutzer entsperren erfolgreich!');
        }
        elseif ($block == 'U') {
            Session::flash('success_msg','Benutzer blockieren erfolgreich!');
        }
        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user = users::find($id);
        Session::flash('open_add','true');
        return redirect()->route('user.index');
        
        //return view('admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
        'name' => 'required|regex:/^[a-zA-Z0-9]+$/u|max:100'
        ]);
        //Update User
        $user = users::find($id);
        
        $user->update(['name'=>$request->get('name'),'role'=>$request->get('role'),'updated_by'=>auth()->user()->id]);
        
        Session::flash('success_msg','Benutzer erfolgreich aktualisiert!');
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //Delete User

        //users::find($id)->delete();
        users::find($id)->update(['status_flag'=>'1']);
        Session::flash('success_msg','Benutzer erfolgreich gelöscht!');
        return redirect()->route('user.index');
    }
}
