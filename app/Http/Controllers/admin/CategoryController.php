<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\masters\Category as category;
use App\models\Document as document;
use App\User;
use Session;
use App\Http\Controllers\utilities\FunctionsController as fun;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $root_categories = category::where('parent_id','0')->orderBy('name','ASC')->get();
        $categories = category::get_list($root_categories,0);
        return view('admin.category.index',compact('categories'));
    }

/*Category Add*/
    public function add()
    {
        Session::flash('open_add','true');
        return redirect()->route('category.index');
    }

/*Category Store in Database*/
    public function store(Request $request)
    {
        //dd($request->get('name'));
        $validatedData = $request->validate([
        'name' => 'required|max:100|unique:category'
        ]);

        $slug = fun::getSlug($request->get('name'),'C');
        $category = category::create(['parent_id'=>$request->get('parent_id'),'name'=>$request->get('name'),'slug'=>$slug,'created_by'=>auth()->user()->id]);
        Session::flash('success_msg','Kategorie erfolgreich hinzugefügt!');
        if ($request->routename && $request->docid) 
        {
            //dd($request->routename,$request->docid);
            return redirect()->route($request->routename,[$request->docid]);
        }
        elseif($request->routename)
        {
            return redirect()->route($request->routename);
        }
        else
        {
            return redirect()->route('category.index');
        }
    }

/*Category Update in Database*/
    public function update($id,Request $request)
    {

        $validatedData = $request->validate([
        'name' => 'required|max:100'
        ]);

        $category = category::find($id);
        /*$database_ids = $category->cargo_types_map->pluck('id')->toArray();
        print_r($database_ids);
        echo "<br>";
        print_r(array_diff($database_ids, $request->get('cargo_types')));
        echo "<br>";
        print_r($request->get('cargo_types'));*/
        /*$to_be_deleted = array_diff($category->cargo_types_map->pluck('cargo_type_id')->toArray(), $request->get('cargo_types'));*/

        /*$validatedData = $request->validate([
        'name' => 'unique:category'
        ]);*/

        $slug = fun::getSlug($request->get('name'),'C',$id);
        $category->update(['parent_id'=>$request->get('parent_id'),'name'=>$request->get('name'),'slug'=>$slug,'updated_by'=>auth()->user()->id]);
        /*if ($request->get('cargo_types')) 
        {
            foreach ($request->get('cargo_types') as $cargo_type_id) 
            {
                category_cargo_type_map::updateOrCreate(['category_id'=>$category->id,'cargo_type_id'=>$cargo_type_id],['updated_by' => auth()->user()->id]);
            }
        }*/
        /*foreach ($to_be_deleted as $id) 
        {
            category_cargo_type_map::where('category_id',$category->id)->where('cargo_type_id',$id)->delete();
        }*/
        Session::flash('success_msg','Kategorie erfolgreich aktualisiert!');
        return redirect()->route('category.index');
    }

/*Category Delete*/
    public function delete($id)
    {
        category::where('parent_id',$id)->update(['parent_id'=>'0']);
        document::where('categories',$id)->update(['categories'=>'0']);
        category::find($id)->delete();
        Session::flash('success_msg','Kategorie erfolgreich gelöscht!');
        return redirect()->route('category.index');
    }
}
