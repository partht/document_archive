<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\ModuleMst;

class ModuleController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$modules = ModuleMst::get();
    	return view('admin.module.index',compact('modules'));
    }

    public function activeInactive($id)
    {	

    	$module = ModuleMst::find($id);

    	if($module->is_active == 'Y')
    	{
    		$module->update([
    			'is_active' => 'N',
    			'updated_by' => auth()->user()->id,
    			]);
    	}
    	else
    	{
    		$module->update([
    			'is_active' => 'Y',
    			'updated_by' => auth()->user()->id,
    			]);
    	}
    	return redirect('admin/module');
    	
    }
}
