<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User as users;
use App\models\Document as document;
use App\models\masters\DocumentAudit as document_audit;
use App\models\masters\Page as page;
use App\models\masters\Keyword as keyword;

use Session;
use App\Http\Controllers\utilities\FunctionsController as fun;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\welcomemail;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {    
        //List Admin Users
        $pagedata = page::all();
        return view('admin.page.index', compact('pagedata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //Add Page
        $keywords = keyword::all();
        return view('admin.page.add', compact('keywords'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
        'title' => 'required|max:100|unique:pages'
        ]);

        $title_slug = fun::getSlug($request->get('title'),'P');
        
        if ($request->get('tags')) 
        {
            foreach ($request->get('tags') as $key => $keyword) {
                if (!keyword::where('name', '=', $keyword)->first()) 
                {
                    $slug = fun::getSlug($keyword,'K');
                    keyword::create(['name'=>$keyword,'slug'=>$slug,'created_by'=>auth()->user()->id]);
                }
            }
            $tags = implode(', ', $request->get('tags'));
        }
        $page = page::create(['title'=>$request->get('title'),'slug'=>$title_slug,'tags'=>$tags,'content'=>$request->get('contentarea'),'created_by'=>auth()->user()->id]);

        Session::flash('success_msg','Seite wurde erfolgreich hinzugefügt!');
        return redirect()->route('page.index');
    }

    public function edit($id)
    {
        $page = page::find($id);
        if(!$page){
            abort(404);
        }
        $keywords = keyword::all();
        return view('admin.page.edit', compact('page', 'keywords'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Update User
        /*$validatedData = $request->validate([
        'title' => 'required|regex:/^[a-zA-Z0-9]+$/u|max:100'
        ]);*/
       $page = page::find($id);

        
        if ($request->get('tags')) 
        {
            foreach ($request->get('tags') as $key => $keyword) {
                if (!keyword::where('name', '=', $keyword)->first()) 
                {
                    $slug = fun::getSlug($keyword,'K');
                    keyword::create(['name'=>$keyword,'slug'=>$slug,'created_by'=>auth()->user()->id]);
                }
            }
            $tags = implode(', ', $request->get('tags'));
        }
        $page->update(['title'=>$request->get('title'),'content'=>$request->get('contentarea'),'tags'=>$tags,'updated_by'=>auth()->user()->id]);
        
        Session::flash('success_msg','Seite wurde erfolgreich aktualisiert!');
        return redirect()->route('page.index');
    }

    public function delete($id)
    {
        //Delete Page

        page::find($id)->delete();
        Session::flash('success_msg','Seite wurde erfolgreich gelöscht!');
        return redirect()->route('page.index');
    }
    
}