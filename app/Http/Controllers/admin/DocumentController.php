<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\models\Document as document;
use App\models\masters\DocumentAudit as document_audit;
use App\models\masters\Category as category;
use App\models\masters\Keyword as keyword;
use App\models\masters\Timerange as timerange;
use Spatie\PdfToText\Pdf as PdfToText;
use Spatie\PdfToImage\Pdf as PdfToImage;
use thiagoalessio\TesseractOCR\TesseractOCR as TesseractOCR;
use App\Http\Controllers\utilities\FunctionsController as fun;
use App\User;

use Session;
use Storage;
use File;
class DocumentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

/**/
    public function index()
    {
        $documents = document::select('id','title','search_count','description','categories','keywords')->where('status_flag','0')->orderBy('title')->get();
        $categorydata = category::all();
        return view('admin.document.index',compact('documents','categorydata'));
    }

/*Document Add*/
    public function add()
    {
        $keywords = keyword::all();
        //return view('admin.document.add',compact('keywords'));
    	$categories = category::all();
        $timerange = timerange::select('id','yearrange')->get();
        return view('admin.document.add',compact('categories', 'keywords', 'timerange'));
    }

/*Document View*/
    public function showaudit($id)
    {
        $documentdata = document::all()->find($id);
        if(!$documentdata){
            abort(404);
        }
        $categories = category::all();
        $keywords = keyword::all();
        $userdocs = User::select('id','name')->get();
        $docaudit = document_audit::where('doc_id',$id)->orderBy('id','desc')->limit(10)->get();
        return view('admin.document.audit', compact('documentdata','userdocs', 'docaudit', 'categories', 'keywords'));
    }

/*Document Edit*/
    public function edit($id)
    {
        $document = document::find($id);
        if(!$document){
            abort(404);
        }
        $keywords = keyword::all();
        //return view('admin.document.edit',compact('document','keywords'));
        $categories = category::all();
        $timerange = timerange::select('id','yearrange')->get();
        return view('admin.document.edit',compact('document', 'categories', 'keywords', 'timerange'));
    }

/*Document Store in Database*/
    public function store(Request $request)
    {
        //dd($request->get('docaddress'));
       /* $categories = $request->get('categories');
        $cat = category::find($categories)->first();
        $root = $cat->root_category();
        dd($root);*/
          //dd($request->get('description'));
        $latlngarray = $request['lat'].",".$request['lng'];
        $validatedData = $request->validate([
        /*'title' => 'required|regex:/^[a-z A-Z 0-9]+$/u|max:30',*/
        'title' => 'required|max:30',
        'keywords' => 'max:15',
        'pdf' => 'file|max:10000' //max limit 12MB
        ]);
        
        $title_slug = fun::getSlug($request->get('title'),'D');
        $pdf_name="";
        $audio_name="";
        $public_url="";
        $data="";
        $audio_url="";
        // dd($_FILES['pdf']);
        // dd($request['pdf']);
        //dd("No checking mimetype");
        if($request['pdf']!= "") {
            //dd("yes pdf");
            $path = storage_path()."/app/public/documents/";
            
            $pdf_name = $title_slug.".".$request['pdf']->getClientOriginalExtension();
            $request['pdf']->move($path , $pdf_name);
            $local_pdf_path = $path.$pdf_name;
            //dd($local_pdf_path);

            //pdf to text data
            $data = PdfToText::getText($local_pdf_path);
            // dd($data);

            //image to text data 
            /*$imgtext = "";
            $pdf = new PdfToImage($local_pdf_path);
            $pdf->setResolution(100);
            $pdf->setCompressionQuality(100);
            $img_dir_path = storage_path()."/app/public/documents/".$title_slug;
            File::makeDirectory($img_dir_path, $mode = 0777, 0777, 0777);
            
            foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) 
            {
               $pdf->setPage($pageNumber)
                   ->saveImage($img_dir_path.'/'.$pageNumber.'.jpg');
                $img = new TesseractOCR($img_dir_path.'/'.$pageNumber.'.jpg');
                $imgtext .= $img->lang('deu','eng')->txt()->run();
            }
            File::deleteDirectory($img_dir_path);
            $data = $imgtext;*/
            /*
            if no text is found 
            if($data=="")
            {
                $pdf = new PdfToImage($local_pdf_path);
                $img_dir_path = storage_path()."/app/public/documents/".$title_slug;
                File::makeDirectory($img_dir_path, $mode = 0777, 0777, 0777);
                //$pdfimage = $pdf->saveImage($img_dir_path);
                foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {
                   $pdf->setPage($pageNumber)
                       ->saveImage($img_dir_path.'/'.$pageNumber.'.jpg');
                    $img = new TesseractOCR($img_dir_path.'/'.$pageNumber.'.jpg');
                    $imgtext = $img->txt()->run();
                }
                $data = $imgtext;
                dd($data);
            }*/
            //dd("yes data");
            Storage::disk('s3')->put($pdf_name, file_get_contents($local_pdf_path));
            $public_url = Storage::disk('s3')->url($pdf_name);
            File::Delete($local_pdf_path);
        }
        else {$public_url=$request['pdf'];}
        if($request['audio'] != ""){
            $audio_name = $title_slug.".".$request['audio']->getClientOriginalExtension();
            Storage::disk('s3')->put($audio_name,file_get_contents($request['audio']),'public');
            $audio_url = Storage::disk('s3')->url($audio_name);
        }
        else {$audio_url=$request['audio'];}
        if($request['video_url'] != "") {}

        $keywords = "";
        if ($request->get('keywords')) 
        {
            
            foreach ($request->get('keywords') as $key => $keyword) {
                if (!keyword::where('name', '=', $keyword)->first()) 
                {
                    $slug = fun::getSlug($keyword,'K');
                    keyword::create(['name'=>$keyword,'slug'=>$slug,'created_by'=>auth()->user()->id]);
                }
            }
            
            $keywords = implode(', ', $request->get('keywords'));
            
        }
        
        /*For updating Category*/
            $categories = $request->get('categories');
            $cat = category::select('id','parent_id','docs_count','audio_count','video_count')->where('id',$categories)->first();
            //dd($cat->parent_id);
            if($cat->parent_id=='0'){$parent_cat=$request->get('categories');}
            else{$parent_cat = $cat->parent_id;}
            
            $cat->update(['docs_count',($cat->docs_count++)]);
            if($request->get('video_url')!="") {
                $cat->update(['video_count',($cat->video_count++)]);
            }
            if($audio_url!="") {
                $cat->update(['audio_count',($cat->audio_count++)]);
            }

        /*For updating Parent Category*/
            if($cat->parent_id!=0){
                $root = $cat->root_category();
                $root->update(['docs_count',($root->docs_count++)]);
                if($request->get('video_url')!="") {
                    $root->update(['video_count',($root->video_count++)]);
                }
                if($audio_url!="") {
                    $root->update(['audio_count',($root->audio_count++)]);
                }
            }
            //dd($root);

    	$document = document::create([
            'title'=>$request->get('title'),
    		'slug'=>$title_slug,
    		'description'=>$request->get('description'),
            'categories'=>$categories,
            'parent_cat'=>$parent_cat,
    		'keywords'=>$keywords,
            'url'=>$pdf_name,
            'video_url'=>$request->get('video_url'),
            'audio_name'=>$audio_name,
            'audio_url'=>$audio_url,
            'public_url'=>$public_url,
    		'data'=>$data,
            'timerange_id'=>$request->get('timerange'),
            'address'=>$request->get('docaddress'),
            'lat_long'=>$latlngarray,
            'city'=>$request->get('city'),
            'state'=>$request->get('state'),
            'country'=>$request->get('country'),
            'created_by'=>auth()->user()->id,
        ]);



        $docid = document::select('id','created_by')->orderBy('id', 'desc')->first();
        $docaudit = document_audit::create([
            'doc_id' => $docid->id,
            'user_id' => $docid->created_by,
            'action' => 'C',
            'user_ipaddress' => $request->ip(),
        ]);

    	Session::flash('success_msg',"Dokument erfolgreich hinzugefügt!");
        return redirect()->route('document.index');
    }

/*Document Update in Database*/
    public function update($id,Request $request)
    {
     //   dd($request->pdfdata);
        $latlngarray = $request['lat'].",".$request['lng'];
        $validatedData = $request->validate([
        'keywords' => 'max:15',
        'pdf' => 'file|max:10000' //max limit 12MB
        ]);

        $document = document::find($id);
        $title_slug = fun::getSlug($request->get('title'),'D',$id);

        //$data = $document->data;
        $data = $request->pdfdata;
        //dd($data);
        $pdf_name = $document->url;
        $audio_name = $document->audio_name;
        $public_url = $document->public_url;
        $audio_url=$document->audio_url;
        
        if ($request['pdf']) 
        {
            //delete old file
            Storage::disk('s3')->delete($pdf_name);

            $path = storage_path()."/app/public/documents/";
            $pdf_name = $title_slug.".".$request['pdf']->getClientOriginalExtension();
            //dd($request['pdf']);

            //upload new file
            $request['pdf']->move($path , $pdf_name);
            $local_pdf_path = $path.$pdf_name;

            //get new data
            $data = PdfToText::getText($local_pdf_path);

            /*if($data=="")
            {
                //dd("scanned pdf");
                //image to text data 
                $imgtext = "";
                $pdf = new PdfToImage($local_pdf_path);
                $pdf->setResolution(100);
                $pdf->setCompressionQuality(100);
                $img_dir_path = storage_path()."/app/public/documents/".$title_slug;
                File::makeDirectory($img_dir_path, $mode = 0777, 0777, 0777);
                
                foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) 
                {
                    $pdf->setPage($pageNumber)
                       ->saveImage($img_dir_path.'/'.$pageNumber.'.jpg');
                    $img = new TesseractOCR($img_dir_path.'/'.$pageNumber.'.jpg');
                    $imgtext .= $img
                                ->lang('deu','eng')
                                ->txt()
                                ->run();
                   
                }
                File::deleteDirectory($img_dir_path);
                $data = $imgtext;
            }*/
            Storage::disk('s3')->put($pdf_name, file_get_contents($local_pdf_path));
            $public_url = Storage::disk('s3')->url($pdf_name);
            File::Delete($local_pdf_path);
        }
        else
        {
            if($pdf_name!=""){
            if ($document->title != $request->get('title')) 
            {
                $old_pdf_name = $pdf_name;
                // dd($old_pdf_name);
                $array = explode('.', $old_pdf_name);
                $extension = end($array);
                $pdf_name = $title_slug.'.'.$extension;
                //dd($pdf_name);
                Storage::disk('s3')->move($old_pdf_name, $pdf_name);
                $public_url = Storage::disk('s3')->url($pdf_name);
            }
            }
        }

        if ($request['audio']) 
        {
            //delete old file
            Storage::disk('s3')->delete($audio_name);
            
            $audio_name = $title_slug.".".$request['audio']->getClientOriginalExtension();

            //get new data
            Storage::disk('s3')->put($audio_name, file_get_contents($request['audio']));
            $audio_url = Storage::disk('s3')->url($audio_name);
        }
        else
        {
            if($audio_name!=""){
            if ($document->title != $request->get('title')) 
            {
                $old_audio_name = $audio_name;
                $array = explode('.', $old_audio_name);
                $extension = end($array);
                $audio_name = $title_slug.'.'.$extension;
                //dd($audio_name);
                Storage::disk('s3')->move($old_audio_name, $audio_name);
                $audio_url = Storage::disk('s3')->url($audio_name);
            }
            }
        }

        $keywords = "";
        if ($request->get('keywords')) 
        {
            foreach ($request->get('keywords') as $key => $keyword) {
                if (!keyword::where('name', '=', $keyword)->first()) 
                {
                    $slug = fun::getSlug($keyword,'K');
                    keyword::create(['name'=>$keyword,'slug'=>$slug,'created_by'=>auth()->user()->id]);
                }
            }
            $keywords = implode(', ', $request->get('keywords'));
        }

        $catparent = category::select('id','parent_id')->where('id',$request->get('categories'))->first();
        if($catparent->parent_id=='0'){$catpar = $request->get('categories');}
        else{$catpar = $catparent->parent_id;}
        
/*Document Count Update in Category*/
            $doccatid = $document->select('categories','video_url','audio_url')->where('id',$id)->first();
            //dd($catid->categories);
            if($doccatid->categories == $request->get('categories')) {
                //$videoaudio = $document->select('video_url','audio_url')->where('id',$id)->first();
                $cat = category::select('id','parent_id','video_count','audio_count')->where('id',$request->get('categories'))->first();
                //dd($videoaudio);
                if($doccatid->video_url=="" && $request->get('video_url')!=""){
                    $cat->update(['video_count',($cat->video_count++)]);
                }
                elseif($doccatid->video_url!="" && $request->get('video_url')=="" && $cat->video_count!=0){
                    $cat->update(['video_count',($cat->video_count--)]);
                }
                
                if($doccatid->audio_url=="" && $audio_url!=""){
                    $cat->update(['audio_count',($cat->audio_count++)]);
                }

                if($cat->parent_id!=0)
                {
                    $root = $cat->root_category();
                    if($doccatid->video_url=="" && $request->get('video_url')!=""){
                        $root->update(['video_count',($root->video_count++)]);
                    }
                    elseif($doccatid->video_url!="" && $request->get('video_url')=="" && $root->video_count!=0){
                        $root->update(['video_count',($root->video_count--)]);
                    }
                    
                    if($doccatid->audio_url=="" && $audio_url!=""){
                        $root->update(['audio_count',($root->audio_count++)]);
                    }
                }
            }
            else 
            {
                $oldcat = $doccatid->categories;
                if($oldcat=='0'){}
                else 
                {
                    $catold = category::select('id','parent_id','docs_count','audio_count','video_count')->where('id',$oldcat)->first();
                    //dd($catold);
                    if($catold->audio_count==0){$audio_count=0;}
                    else {$audio_count = ($catold->audio_count) - 1;}
                    
                    if($catold->video_count==0){$video_count=0;}
                    else {$video_count = ($catold->video_count) - 1;}
                    
                    if($catold->docs_count==0){$docs_count=0;}
                    else{$docs_count = ($catold->docs_count) - 1;}
                    
                    $catold->update([
                        'docs_count'=>$docs_count, 
                        'audio_count'=>$audio_count,
                        'video_count'=>$video_count,
                    ]);
                
                /*Update Parent category*/
                    if($catold->parent_id!=0)
                    {
                        $rootold = $catold->root_category();
                        if($rootold->audio_count==0){$audio_count=0;}
                        else {$audio_count = ($rootold->audio_count) - 1;}
                        
                        if($rootold->video_count==0){$video_count=0;}
                        else {$video_count = ($rootold->video_count) - 1;}
                        
                        if($rootold->docs_count==0){$docs_count=0;}
                        else{$docs_count = ($rootold->docs_count) - 1;}
                        
                        $rootold->update([
                            'docs_count'=>$docs_count, 
                            'audio_count'=>$audio_count,
                            'video_count'=>$video_count,
                        ]);
                    }
                }
                
                
                /*New category*/
                $categories = $request->get('categories');
                $catnew = category::where('id',$categories)->first();
                $catnew->update(['docs_count',($catnew->docs_count++)]);
                if($request->get('video_url')!="") {
                    $catnew->update(['video_count',($catnew->video_count++)]);
                }
                if($audio_url!="") {
                    $catnew->update(['audio_count',($catnew->audio_count++)]);
                }
            /*Update Parent Category*/    
                if($catnew->parent_id!=0)
                {
                    $rootnew = $catnew->root_category();
                    $rootnew->update(['docs_count',($rootnew->docs_count++)]);
                    if($request->get('video_url')!="") {
                        $rootnew->update(['video_count',($rootnew->video_count++)]);
                    }
                    if($audio_url!="") {
                        $rootnew->update(['audio_count',($rootnew->audio_count++)]);
                    }
                }
            }    

        $document->update([
            'title'=>$request->get('title'),
            'slug'=>$title_slug,
            'description'=>$request->get('description'),
            'keywords'=>$keywords,
            'categories'=>$request->get('categories'),
            'parent_cat'=>$catpar,
            'data'=>$data,
            'url'=>$pdf_name,
            'video_url'=>$request->get('video_url'),
            'public_url'=>$public_url,
            'audio_name'=>$audio_name,
            'audio_url'=>$audio_url,
            'timerange_id'=>$request->get('timerange'),
            'address'=>$request->get('docaddress'),
            'lat_long'=>$latlngarray,
            'city'=>$request->get('city'),
            'state'=>$request->get('state'),
            'country'=>$request->get('country'),
            'updated_by'=>auth()->user()->id,
        ]);

        $docupid = document::select('id','updated_by')->find($id);
        $docauditup = document_audit::create([
            'doc_id' => $docupid->id,
            'user_id' => $docupid->updated_by,
            'action' => 'U',
            'user_ipaddress' => $request->ip(),
        ]);
        
        Session::flash('success_msg',"Dokument erfolgreich aktualisiert!");
        return redirect()->route('document.index');
    }

/*Document Soft Delete*/
    public function delete($id,Request $request)
    {
        $document = document::find($id);

        $docauditdel = document_audit::create([
            'doc_id' => $document->id,
            'user_id' => auth()->user()->id,
            'action' => 'D',
            'user_ipaddress' => $request->ip(),
        ]);

        Storage::disk('s3')->delete($document->url);

        //$document->delete();
        $catid = $document->select('categories')->where('id',$id)->first();
        if($catid->categories==0){}
        else 
        {            
            $cat = category::select('id','parent_id','docs_count','audio_count','video_count')->where('id',$catid->categories)->first();

            if($cat->audio_count==0){$audio_count=0;}
            else {$audio_count = ($cat->audio_count) - 1;}
            
            if($cat->video_count==0){$video_count=0;}
            else {$video_count = ($cat->video_count) - 1;}
            
            if($cat->docs_count==0){$docs_count=0;}
            else{$docs_count = $cat->docs_count - 1;}
            
            $cat->update([
                'docs_count'=>$docs_count, 
                'video_count'=>$video_count, 
                'audio_count'=>$audio_count,
            ]);

            /*For updating Parent category*/
            if($cat->parent_id!=0)
            {
                $root = $cat->root_category();
                if($root->audio_count==0){$audio_count=0;}
                else {$audio_count = ($root->audio_count) - 1;}
                
                if($root->video_count==0){$video_count=0;}
                else {$video_count = ($root->video_count) - 1;}
                
                if($root->docs_count==0){$docs_count=0;}
                else{$docs_count = $root->docs_count - 1;}
                
                $root->update([
                    'docs_count'=>$docs_count, 
                    'video_count'=>$video_count, 
                    'audio_count'=>$audio_count,
                ]);
            }
        }


        $document->update(['status_flag'=>'1']);
        Session::flash('success_msg','Dokument erfolgreich gelöscht!');
        return redirect()->route('document.index');
    }
}