<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\masters\Keyword as keyword;

use App\User;
use Session;
use App\Http\Controllers\utilities\FunctionsController as fun;

class KeywordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $keywords = keyword::orderBy('created_at','desc')->get();
        return view('admin.keyword.index',compact('keywords'));
    }

    public function add()
    {
        Session::flash('open_add','true');
        return redirect()->route('keyword.index');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $validatedData = $request->validate([
        'name' => 'required|regex:/^[a-z A-Z 0-9]+$/u|max:15|unique:keyword'
        ]);

        $slug = fun::getSlug($request->get('name'),'K');
        $keyword = keyword::create(['name'=>$request->get('name'),'slug'=>$slug,'created_by'=>auth()->user()->id]);
        Session::flash('success_msg','Keywort erfolgreich hinzugefügt!');
        if ($request->routename && $request->docid) 
        {
            //dd($request->routename,$request->docid);
            return redirect()->route($request->routename,[$request->docid]);
        }
        elseif($request->routename)
        {
            return redirect()->route($request->routename);
        }
        else
        {
            return redirect()->route('keyword.index');
        }
        
    }

    public function update($id,Request $request)
    {

        $validatedData = $request->validate([
        'name' => 'required|regex:/^[a-z A-Z 0-9]+$/u|max:15'
        ]);

        $keyword = keyword::find($id);
        
        /*$validatedData = $request->validate([
        'name' => 'unique:keyword'
        ]);*/

        $slug = fun::getSlug($request->get('name'),'K',$id);
        //print_r($slug);
        $keyword->update(['name'=>$request->get('name'),'slug'=>$slug,'updated_by'=>auth()->user()->id]);
        
        Session::flash('success_msg','Keyword erfolgreich aktualisiert!');
        
        return redirect()->route('keyword.index');
    }

    public function delete($id)
    {
        keyword::find($id)->delete();
        Session::flash('success_msg','Keyword erfolgreich gelöscht!');
        return redirect()->route('keyword.index');
    }
}
