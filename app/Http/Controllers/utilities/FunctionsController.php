<?php

namespace App\Http\Controllers\utilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\models\masters\Category as category;
use App\models\masters\Keyword as keyword;
use App\models\Document as document;
use App\models\masters\Page as page;

class FunctionsController extends Controller
{   
    public static function getSlug($title,$type,$id = 0) //$type:{C->Category, K->Keyword}
    {
        $slug = str_slug($title);

        $allSlugs = FunctionsController::getRelatedSlugs($slug,$type,$id);

        if (! $allSlugs->contains($slug))
        {
            return $slug;
        }

        for ($i = 1; $i <= 1000; $i++) 
        {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains($newSlug)) 
            {
                return $newSlug;
            }
        }
    } 

    protected static function getRelatedSlugs($slug,$type,$id = 0)
    {
        if ($type == 'C') //For Category
        {
            return category::where('slug', 'like', $slug.'%')
                ->where('id', '<>', $id)
                ->get()->pluck('slug');
        }
        else if ($type == 'K') //For Keyword
        {
            return keyword::where('slug', 'like', $slug.'%')
                ->where('id', '<>', $id)
                ->get()->pluck('slug');
        }
        else if ($type == 'D') //For Products
        {
            return document::where('slug', 'like', $slug.'%')
                ->where('id', '<>', $id)
                ->get()->pluck('slug');
        }
        else if ($type == 'P') //For Pages
        {
            return page::where('slug', 'like', $slug.'%')
                ->where('id', '<>', $id)
                ->get()->pluck('slug');
        }
        /*
        else if ($type == 'PT') //For Page Tag
        {
            return page_tags_trans::where('slug', 'like', $slug.'%')
                ->where('locale',$locale)
                ->where('page_tag_id', '<>', $id)
                ->get()->pluck('slug');
        }
        else if ($type == 'PG') //For Pages
        {
            return pages_trans::where('slug', 'like', $slug.'%')
                ->where('locale',$locale)
                ->where('page_id', '<>', $id)
                ->get()->pluck('slug');
        }*/
    }
}