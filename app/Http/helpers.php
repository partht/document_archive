<?php

  function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->role);

    foreach ($permissions as $key => $value) {
      if($value == $userAccess){
        return true;
      }
    }
    return false;
  }

  function getMyPermission($id)
  {
    switch ($id) {
      case 'A':
        return 'admin';
        break;
      case 'S':
        return 'sub admin';
        break;
      default:
        return 'house_owner';
        break;
    }
  }

  function get_notification($user_type,$user_id)
  {
    if($user_type == 'a')
    {
     return  \App\models\Notification::where('created_by','!=',$user_id)->orderBy('id','desc')->get();

    }

    if($user_type == 'c')
    {
     return \App\models\Notification::where('user_id',$user_id)->where('created_by','!=',$user_id)->orderBy('id','desc')->get();
    }
  }

  function agreements($user_id)
  {
    $agreements=\App\models\AgreementDtl::where('user_id',$user_id)->get();
    if(count($agreements) > 0)
    {
      foreach($agreements as $agreement)
      {
        if($agreement->is_accepted == 'N')
        {
          return 0;
        }
      }
      return 1;
    }
    else
    {
      return 0;
    }
  }

?>